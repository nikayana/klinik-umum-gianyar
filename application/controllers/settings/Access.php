<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Access extends HC_Controller {
    private $response;
    private $modul;
    private $priv;
	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->response['status'] = 404;
        $this->response['messages'] = "Unknown Error, Please contact your administrator";
        $this->response['datas'] = array();
        $this->modul = "settings/privilege";
        $this->priv = $this->checkpriv($this->modul);

    }

    public function main($key){
        $data = array();
        $data['title'] = "Akses Pengguna";
        $data['description'] = "Mengelola akses pengguna";
        $data['privilege'] = $this->privilege->fetch("md5(".$this->privilege->FIELD_PRIMARY.")='".$key."'");
        $data['priv'] = $this->priv;
        if(isset($this->priv->{$this->access->FIELD_ACCESS_VIEW}) && $this->priv->{$this->access->FIELD_ACCESS_VIEW} == $this->access->ACCESS){
            $this->theme("admin/access/main-access", $data);
        }else{
            redirect("notfound");
        }
        
    }

    public function datatables(){
        $result = array();
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $aColumns = array("access_id","menu_name", "access_view", "access_add", "access_update", "access_delete", "access_datemodified");

        $sLimit = 20;
        $sOffset = 0;
        $sOrder = "";
        $sWhere = "";
        $sGroup = "";
        $sEcho = 0;
        if(isset($param["sEcho"])){
            $sEcho = 0;
        }
        if(isset($param['start']) && $param['start'] > 0){
            $sOffset = $param['start'];
        }
        
        if(isset($param['order'])){
            if(empty($sOrder)){
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }else{
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }
        }

        $sWhere = "1=1";
        $sWhere .= " AND a.".$this->access->FIELD_PRIVILEGE_ID."='".$param[$this->access->FIELD_PRIVILEGE_ID]."'";
        if(isset($param[$this->access->FIELD_MENU_ID]) && strlen($param[$this->access->FIELD_MENU_ID]) > 0){
            $sWhere .=" AND a.".$this->access->FIELD_MENU_ID."=".$param[$this->access->FIELD_MENU_ID]."";
        }
        $searchWhere = "";
        if(!empty($param['search']['value'])){

            for($i=0;$i<count($aColumns);$i++){
                if(empty($searchWhere)){
                    $searchWhere .=$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }else{
                    $searchWhere .=" OR ".$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }

            }

            if(!empty($sWhere)){
                $sWhere .=" AND (".$searchWhere.")";
            }else{
                $sWhere .= "(".$searchWhere.")";
            }

        }
        $sTotal = $this->access->getCountJoin($sWhere);
        $data = $this->access->getListJoin($sOffset,$sLimit,$sWhere,$sOrder,$sGroup);
        $output = array(
            "sEcho" => intval($sEcho),
            "iTotalRecords" => $sTotal,
            "iTotalDisplayRecords" => $sTotal,
            "aaData" => array()
        );

        foreach ($data AS $key => $value){
            $row = array();
            $button = "";

            if(isset($this->priv->{$this->access->FIELD_ACCESS_UPDATE}) && $this->priv->{$this->access->FIELD_ACCESS_UPDATE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->access->FIELD_PRIMARY}."' data-url='". site_url("settings/access/form")."' class='addedit btn btn-warning btn-xs m-t-10' data-toggle='tooltip' data-original-title='Edit' data-placement='top' style='margin-right:5px;' data-modal='#editor' data-title='Ubah Data Akses'><i class='fas fa-edit'></i></button>";
            }
            if(isset($this->priv->{$this->access->FIELD_ACCESS_DELETE}) && $this->priv->{$this->access->FIELD_ACCESS_DELETE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->access->FIELD_PRIMARY}."' data-url='". site_url("settings/access/deleteExc")."' class='delete btn btn-danger btn-xs m-t-10' data-toggle='tooltip' data-original-title='Hapus' data-placement='top' style='margin-right:5px;'><i class='fa fa-trash'></i></button>";
           }

            for($i = 0; $i < count($aColumns); $i++){
                if($aColumns[$i] == $this->access->FIELD_PRIMARY){
                        $row[] = $sOffset+$key+1;
                }else if($aColumns[$i] == $this->access->FIELD_ACCESS_DATEMODIFIED){
                        $row[] = $button;
                }else if($aColumns[$i] == $this->access->FIELD_ACCESS_ADD || $aColumns[$i] == $this->access->FIELD_ACCESS_VIEW || $aColumns[$i] == $this->access->FIELD_ACCESS_UPDATE || $aColumns[$i] == $this->access->FIELD_ACCESS_DELETE){
                    $icon = $value->{$aColumns[$i]} == 0 ? "<i class='fa fa-times' style='color:#e46a76;'></i>" : "<i class='fa fa-check' style='color:#00c292'></i>";
                    $row[] = $icon;
                }else{
                        $row[] = $value->{$aColumns[$i]};
                }
            }

            $row[] = $button;
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function form(){
		$data = array();
		$param = $this->input->post(NULL, TRUE);
		$param = $this->antiinjection->antiinject($param);
		$where = "";
		//GET Data
        $data['data'] = $this->access->fetch($this->access->FIELD_PRIMARY."='".$param['id']."'");
        $data['menu'] = $this->menu->getList(0,0,$this->menu->FIELD_MENU_STATUS."='".$this->menu->ACTIVE_STATUS."'",$this->menu->FIELD_MENU_NAME." ASC","");
        $data['access'] = $this->access->ACCESSES;
        $data['profile'] = $this->getprofile();
        
		$this->response['html'] = $this->load->view("admin/access/access-form", $data, TRUE);
		$this->response['status'] = 200;
		$this->response['messages'] = '';
        $this->response['datas'] = array();
        

		echo json_encode($this->response);
    }
    
    public function save(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);

        $filter = $this->checkparam($this->access->getrules(), $param);
        if(count($filter) == 0){
            $data = $this->access->fetch($this->access->FIELD_PRIMARY."='".$param['id']."'");
            $param[$this->access->FIELD_ACCESS_DATEMODIFIED] = date("Y-m-d H:i:s");
            if(isset($data->{$this->access->FIELD_PRIMARY})){
                $this->updateExc($param);
            }else{
                $param[$this->access->FIELD_ACCESS_DATECREATED] = date("Y-m-d H:i:s");
                $this->insertExc($param);
            }
        }else{
            $this->response = $filter;
        }
        
        echo json_encode($this->response);
    }

    public function updateExc($param){
        $id = $param['id'];

        unset($param['id']);
        $result = $this->access->update($param, $this->access->FIELD_PRIMARY."='".$id."'");

        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data menu akses berhasil diperbaharui';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function insertExc($param){

        unset($param['id']);
        $exist = $this->access->getCount($this->access->FIELD_MENU_ID."=".$param[$this->access->FIELD_MENU_ID]." AND ".$this->access->FIELD_PRIVILEGE_ID."=".$param[$this->access->FIELD_PRIVILEGE_ID]);
        if($exist > 0){
            $this->response['status'] = 404;
            $this->response['messages'] = 'Data akses yang anda masukkan sudah ada';
        }else{
            $result = $this->access->insert($param);
            if($result){
                $this->response['status'] = 200;
                $this->response['messages'] = 'Data jenis akses berhasil diperbaharui';
            }else{
                $this->response['status'] = 500;
                $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
            }
        }
    }

    public function deleteExc(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $result = $this->access->delete($this->access->FIELD_PRIMARY."='".$param['id']."'");
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data akses berhasil dihapus';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menghapus data, silahkan ulangi kembali';
        }

        echo json_encode($this->response);
    }

    public function selectdata(){
        $response = array();
        $result = array();
        $raw = array();
        $raw['id'] = "";
        $raw['text'] = "Semua Akses";
        $result[] = $raw;
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $where = "";
        $order = $this->access->FIELD_ACCESS_NAME." ASC";
        if(isset($param['term']) && !empty($param['term'])){
            $where .= "LOWER(".$this->privilege->FIELD_ACCESS_NAME.") LIKE '%".$param['term']."%'";
            $data = $this->access->getList(0,0,$where, $order, "");
            foreach($data AS $key => $value){
                $raw = array();
                $raw['id'] = $value->{$this->access->FIELD_PRIMARY};
                $raw['text'] = $value->{$this->access->FIELD_ACCESS_NAME};
                $result[] = $raw;
            }
        }
        
        $response['results'] = $result;
        echo json_encode($response);
    }
}