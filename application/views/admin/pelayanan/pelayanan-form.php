<input type="hidden" name="id" value="<?php echo isset($data->pelayanan_id) ? $data->pelayanan_id : 0 ?>">
<input type="hidden" name="pelayanan_modifiedby" value="<?php echo isset($profile->user_id) ? $profile->user_id : "" ?>">
<div class="form-material">
    <div class="row">
        <div class="col-md-12 b-r"> 
            <div class="form-group">
                <label>Nama Pelayanan <font color='red'>*</font></label>
                <input type="text" name="pelayanan_name" class="form-control form-control-line" value="<?php echo isset($data->pelayanan_name) ? $data->pelayanan_name : "" ?>" placeholder='Isikan nama pelayanan rumah sakit' required="true"> 
            </div>
            <div class="form-group">
                <label>Deskripsi Pelayanan <font color='red'>*</font></label>
                <textarea name="pelayanan_desc" class="form-control form-control-line" placeholder='Isikan deskripsi pelayanan rumah sakit' required="true"><?php echo isset($data->pelayanan_desc) ? $data->pelayanan_desc : "" ?></textarea>
            </div>
            <div class="form-group">
                <label>Status <font color='red'>*</font></label>
                <select class='form-control form-control-line' name="pelayanan_status" required="true">
                    <option value=""> Pilih Status Pelayanan </option>
                    <?php
                        if(isset($status)){
                            foreach($status AS $key => $value){
                                $selected = "";
                                if(isset($data->pelayanan_status) && $data->pelayanan_status == $key){
                                    $selected = "selected='selected'";
                                }
                                ?>
                                <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                                <?php
                                
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
    </div>
</div>
