<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">
                    <b>Dashboard</b>
                </h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Beranda</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Info box -->
        <!-- ============================================================== -->
        
        <!-- ============================================================== -->
        <!-- End Info box -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Sales Chart and browser state-->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-lg-12">
                <div class='card' id="searchform">
                    <div class="card-body">
                        
                    <h4 class="card-title">Selamat Datang</h4>
                    <p>Halo <b><?php echo $this->session->has_userdata("name") ? $this->session->userdata("name") : "" ?></b> ,Selamat datang di Sistem  <?php echo isset($comprof->profile_name) ? $comprof->profile_name  : "" ?>, silakan mengakses modul pada sistem melalui menu yang berada diatas halaman ini</p>
                        
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</div>