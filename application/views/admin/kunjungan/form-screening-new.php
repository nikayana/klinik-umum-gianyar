<?php  
    $diff = "";
    if(isset($data->pasien_tgllahir)){
        $date1 = $data->pasien_tgllahir;
        $date2 = date("Y-m-d");
        
        $diff = abs(strtotime($date2) - strtotime($date1));
        
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
    
        $diff = $years." tahun ".$months." bulan ".$days." hari";
    }
?>
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">
                    <b>Form Pemeriksaan Pasien</b>
                </h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard") ?>"><i class='fa fa-home'></i> Beranda</a></li>
                        <li class="breadcrumb-item">Kunjungan</li>
                        <li class="breadcrumb-item active"> Pemeriksaan Pasien</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        
        <!-- ============================================================== -->
        <!-- Sales Chart and browser state-->
        <!-- ============================================================== -->
        <div class="row">
            <!-- Column -->
            <form id="formdatareg" data-url="<?php echo site_url("operasional/kunjungan/save") ?>" data-modal="#editor">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Informasi Pasien</h4>
                        <input type="hidden" name="id" value="<?php echo isset($data->kunjungan_id) ? $data->kunjungan_id : 0 ?>">
                        <input type="hidden" name="kunjungan_modifiedby" value="<?php echo isset($profile->user_id) ? $profile->user_id : "" ?>">
                        <input type="hidden" name="kunjungan_date" value="<?php echo isset($data->kunjungan_date) ? $data->kunjungan_date : "" ?>">
                        <input type="hidden" name="kunjungan_time" value="<?php echo isset($data->kunjungan_time) ? $data->kunjungan_time : "" ?>">
                        <input type="hidden" name="dokter_id" value="<?php echo isset($data->dokter_id) ? $data->dokter_id : "" ?>">
                        <input type="hidden" name="registrasi_id" value="<?php echo isset($data->registrasi_id) ? $data->registrasi_id : "" ?>">
                        <div class="form-material">
                            <div class="row"> 
                                <div class="col-md-3 b-r">
                                    <div class="form-group">
                                        <label>Nomor Rekam Medis <font color='red'>*</font></label>
                                        <input type="text" name="registrasi_norekam" class="form-control form-control-line" value="<?php echo isset($data->registrasi_norekam) ? $data->registrasi_norekam : "" ?>" disabled > 
                                    </div>
                                </div>
                                <div class="col-md-3 b-r">
                                    <div class="form-group">
                                        <label>Nama Pasien <font color='red'>*</font></label>
                                        <input type="text" name="pasien_name" class="form-control form-control-line" value="<?php echo isset($data->pasien_name) ? $data->pasien_name : "" ?>" disabled > 
                                    </div>
                                </div>
                                <div class="col-md-3 b-r">
                                    <div class="form-group">
                                        <label>TTL / Umur <font color='red'>*</font></label>
                                        <input type="text" name="pasien_tgllahir" class="form-control form-control-line" value="<?php echo isset($data->pasien_tgllahir) ? date("d F Y", strtotime($data->pasien_tgllahir))." / ".$diff : "" ?>" disabled> 
                                    </div>
                                </div>
                                <div class="col-md-3 b-r">
                                    <div class="form-group">
                                        <label>Jenis Kelamin <font color='red'>*</font></label>
                                        <input type="text" name="pasien_jk" class="form-control form-control-line" value="<?php echo isset($data->pasien_jk) ? $jk[$data->pasien_jk] : "" ?>" disabled> 
                                    </div>
                                </div>
                                <div class="col-md-6 b-r">
                                    <div class="form-group">
                                        <label>Alamat <font color='red'>*</font></label>
                                        <input type="text" name="pasien_alamat" class="form-control form-control-line" value="<?php echo isset($data->pasien_alamat) ? $data->pasien_alamat : "" ?>" disabled> 
                                    </div>
                                </div>
                                <div class="col-md-3 b-r">
                                    <div class="form-group">
                                        <label>No HP <font color='red'>*</font></label>
                                        <input type="text" name="pasien_phone" class="form-control form-control-line" value="<?php echo isset($data->pasien_phone) ? $data->pasien_phone : "" ?>" disabled> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <div>
                                <h5 class="card-title">Hasil Pemeriksaan Pasien</h5>
                            </div>
                        </div>
                        <div class="form-material">
                            <div class="row">
                                <div class="col-md-12 b-r">
                                    <div class="form-group">
                                        <label>Anamnesa <font color='red'>*</font></label>
                                        <textarea name="amnesa_keluhan" class="form-control form-control-line" placeholder='Isikan informasi Anamnesa pasien' required="true"><?php echo isset($data->amnesa_keluhan) ? $data->amnesa_keluhan : "" ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12 b-r">
                                    <div class="form-group">
                                        <label>Therapy/Tindakan Dokter <font color='red'>*</font></label>
                                        <textarea name="tindakan_dokter" class="form-control form-control-line" placeholder='Isikan informasi Therapy/tindakan dokter' required="true"><?php echo isset($data->tindakan_dokter) ? $data->tindakan_dokter : "" ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12 b-r">
                                    <div class="form-group">
                                        <label>Diagnosa Penyakit <font color='red'>*</font></label>
                                         
                                        <select class="form-control select2" name="icd10">
                                            <option value="">-pilih diagnosa penyakit-</option>
                                            <?php foreach($icd10 as $key => $value){ ?>
                                            <option value="<?php echo $value->icd10_id; ?>" <?php echo ($value->icd10_id == $data->icd10)? 'selected="selected"': ''; ?> >(<?php echo $value->icd10_kode.') '.$value->icd10_name; ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12 b-r">
                                    <div class="form-group">
                                        <label>Dokter <font color='red'>*</font></label>
                                         
                                        <select class="form-control select2" name="dokter_id" required>
                                            <option value="">-pilih dokter pemeriksa-</option>
                                            <?php foreach($dokter as $key => $value){ ?>
                                            <option value="<?php echo $value->dokter_id; ?>" <?php echo ($value->dokter_id == $data->dokter_id)? 'selected="selected"': ''; ?>><?php echo $value->dokter_name; ?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12 b-r">
                                    <div class="form-group">
                                        <label>Resep Obat Dokter <font color='red'>*</font></label>
                                        <textarea name="resep_obat" class="form-control form-control-line" placeholder='Isikan informasi resep dokter' required="true"><?php echo isset($data->resep_obat) ? $data->resep_obat : "" ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12 b-r">
                                    <div class="form-group">
                                        <label>Perlu Pemeriksaan Lab ? <font color='red'>*</font></label>
                                         
                                        <select class="form-control select2" name="status_pemeriksaan_lab" required>
                                            <option value="">-pilih -</option>
                                            <option value="1" <?php echo ($data->status_pemeriksaan_lab == 1)? 'selected="selected"': ''; ?>>Ya</option>
                                            <option value="0" <?php echo ($data->status_pemeriksaan_lab == 0)? 'selected="selected"': ''; ?>>Tidak</option>
                                            
                                        </select>
                                    </div>

                                    <?php if(!empty($data->file_pemeriksaan_lab)){ ?>
                                        <p>Tgl Pemeriksaan Lab :  <?php echo date('Y-m-d', strtotime($data->tgl_pemeriksaan_lab));  ?></p>
                                        <a href="/upload/lab/<?php echo $data->file_pemeriksaan_lab;  ?>" target="_blank">Download File Hasil Lab</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 b-r text-right">
                                <div class="alert" id="notif" style="display:none;"></div>
                                <button type="submit" class="btn btn-success" id="savebtn"><i class='fa fa-check'></i> Simpan</button>
                                <a href="<?php echo site_url("operasional/kunjungan") ?>" class="btn btn-danger" id="resetbtn"><i class='fa fa-crose'></i> Keluar</a> 
                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
            <form id="search">
                <input type="hidden" name="kunjungan_id" value="<?php echo isset($data->kunjungan_id) ? $data->kunjungan_id : 0 ?>">
            </form>
            <!-- Column -->
        </div>
        
        <!-- ============================================================== -->
        <!-- Comment - chats -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- End Comment - chats -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</div>

<div class="modal fade" id="editor" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	   <div class="modal-dialog modal-xl" role="document">
		     <div class="modal-content">

              <form id="formdata" data-url="<?php echo site_url("operasional/kunjunganpelayanan/save") ?>" data-modal="#editor">
			        <div class="modal-header">
			            <h5 class="modal-title">Modal title</h5>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
		                </button>
                    </div>
	                <div class="modal-body">
				            
        			</div>
        			<div class="modal-footer">
                        
                        <div class="col-md-12 mb-3">
                            <div class="alert" id="notif" style="display:none;"></div>
                        </div>
        				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Tutup</button>
        				<button type="submit" class="btn btn-success" id="savebtn"><i class='fa fa-check'></i> Simpan</button>
        			</div>
              </form>
          </div>
     </div>
</div>