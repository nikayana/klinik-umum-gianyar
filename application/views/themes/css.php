<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url("assets/images/favicon.png") ?>">
<title>Home Care - <?php echo isset($comprof->profile_id) ? $comprof->profile_name : "" ?></title>
<!-- This page CSS -->
<!-- This page CSS -->
<link href="<?php echo site_url("assets/node_modules/morrisjs/morris.css") ?>" rel="stylesheet">
<!-- Select2 CSS -->
<link href="<?php echo site_url("assets/node_modules/select2/dist/css/select2.min.css") ?>" rel="stylesheet" type="text/css" />
<!-- Toast CSS -->
<link href="<?php echo site_url("assets/node_modules/toast-master/css/jquery.toast.css") ?>" rel="stylesheet">
<!-- Sweet Alert CSS -->
<link href="<?php echo site_url("assets/node_modules/sweetalert2/dist/sweetalert2.min.css")?>" rel="stylesheet">
<link href="<?php echo site_url("assets/node_modules/croppie/croppie.css")?>" rel="stylesheet">
<link href="<?php echo site_url("assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css") ?>" rel="stylesheet">
<!-- Page plugins css -->
<link href="<?php echo site_url("assets/node_modules/clockpicker/dist/jquery-clockpicker.min.css") ?>" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo site_url("assets/dist/css/style.min.css") ?>" rel="stylesheet">
<style type="text/css">
    .datepicker{z-index:1151 !important;}
    .dp-selected[style] {
        background-color: #fb9678 !important; 
    }
    .datepaginator-sm .pagination li a,
    .datepaginator-lg .pagination li a,
    .datepaginator .pagination li a {
        padding: 0 5px;
        height: 60px;
        border: 1px solid #e9ecef;
        float: left;
        position: relative; 
    }
</style>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->