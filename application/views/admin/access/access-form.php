<input type="hidden" name="id" value="<?php echo isset($data->access_id) ? $data->access_id : 0 ?>">
<input type="hidden" name="access_modifiedby" value="<?php echo isset($profile->user_id) ? $profile->user_id : "" ?>">

<div class="form-row">
  <div class="col-md-12 mb-3">
      <label for="menu_id">Menu <font color='red'>*</font></label>
      <select name="menu_id" class="selects form-control custom-select" style="width:100%">
        <option value="">Pilih Menu</option>
        <?php 
            if(isset($menu)){
                foreach($menu AS $key => $value){
                    $selected = "";
                    if(isset($data->menu_id) && $data->menu_id == $value->menu_id){
                        $selected = "selected='selected'";
                    }
                    ?>
                    <option value='<?php echo $value->menu_id ?>' <?php echo $selected ?>><?php echo $value->menu_name ?></option>
                    <?php
                }
            }
        
        ?>
      </select>
  </div>
  <div class="col-md-12 mb-3">
      <label for="access_view">Melihat Data <font color='red'>*</font></label>
      <select name="access_view" class="selects form-control custom-select" style="width:100%">
        <?php 
            if(isset($access)){
                foreach($access AS $key => $value){
                    $selected = "";
                    if(isset($data->access_view) && $data->access_view == $key){
                        $selected = "selected='selected'";
                    }
                    ?>
                    <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                    <?php
                }
            }
        
        ?>
      </select>
    </div>
    <div class="col-md-12 mb-3">
      <label for="access_add">Menambahkan Data <font color='red'>*</font></label>
      <select name="access_add" class="selects form-control custom-select" style="width:100%">
        <?php 
            if(isset($access)){
                foreach($access AS $key => $value){
                    $selected = "";
                    if(isset($data->access_add) && $data->access_add == $key){
                        $selected = "selected='selected'";
                    }
                    ?>
                    <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                    <?php
                }
            }
        
        ?>
      </select>
    </div>
    <div class="col-md-12 mb-3">
      <label for="access_update">Mengubah Data <font color='red'>*</font></label>
      <select name="access_update" class="selects form-control custom-select" style="width:100%">
        <?php 
            if(isset($access)){
                foreach($access AS $key => $value){
                    $selected = "";
                    if(isset($data->access_update) && $data->access_update == $key){
                        $selected = "selected='selected'";
                    }
                    ?>
                    <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                    <?php
                }
            }
        
        ?>
      </select>
    </div>
    <div class="col-md-12 mb-3">
      <label for="access_delete">Menghapus Data <font color='red'>*</font></label>
      <select name="access_delete" class="selects form-control custom-select" style="width:100%">
        <?php 
            if(isset($access)){
                foreach($access AS $key => $value){
                    $selected = "";
                    if(isset($data->access_delete) && $data->access_delete == $key){
                        $selected = "selected='selected'";
                    }
                    ?>
                    <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                    <?php
                }
            }
        
        ?>
      </select>
    </div>
</div>
