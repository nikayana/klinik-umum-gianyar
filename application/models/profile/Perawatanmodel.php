<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perawatanmodel extends CI_Model{
    //TABLE PROPERTIES
    private $table = "homecare_perawatan";
    private $table_pelayanan = "homecare_pelayanan";
    public $FIELD_PRIMARY = "perawatan_id";
    public $FIELD_PERAWATAN_NAME = "perawatan_name";
    public $FIELD_PELAYANAN_ID = "pelayanan_id";
    public $FIELD_PERAWATAN_STATUS = "perawatan_status";
    public $FIELD_PERAWATAN_DATECREATED = "perawatan_datecreated";
    public $FIELD_PERAWATAN_DATEMODIFIED = "perawatan_datemodified";
    public $FIELD_PERAWATAN_MODIFIEDBY = "perawatan_modifiedby";
    public $FIELD_PERAWATAN_HARGA = "perawatan_harga";

    public $ACTIVE_STATUS = 1;
    public $NONACTIVE_STATUS = 0;
    public $STATUS = array(
        "Non Aktif",
        "Aktif"
    );

    //RULES
    private $rules;

    function __construct(){
        parent::__construct();

        $this->rules  = array(
            array(
                "name" => "nama perawatan",
                "field" =>$this->FIELD_PERAWATAN_NAME,
                "required" => true,
            ),
            array(
                "name" => "pelayanan",
                "field" =>$this->FIELD_PELAYANAN_ID,
                "required" => true,
            ),
            array(
                "name" => "status",
                "field" => $this->FIELD_PERAWATAN_STATUS,
                "required" => false,
            ),
            array(
                "name" => "harga",
                "field" => $this->FIELD_PERAWATAN_HARGA
            )
        );
    }

    public function getrules(){
        return $this->rules;
    }
    public function fetch($where = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    public function getAll(){
        $result = array();
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

    public function getList($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getListJoin($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        $this->db->select("pl.pelayanan_name, p.*");
        $this->db->from($this->table." AS p");
        $this->db->join($this->table_pelayanan." AS pl", "p.".$this->FIELD_PELAYANAN_ID."=pl.pelayanan_id","LEFT");

        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get();
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getCountJoin($where= ""){
        $result = 0;
        $this->db->from($this->table." AS p");
        $this->db->join($this->table_pelayanan." AS pl", "p.".$this->FIELD_PELAYANAN_ID."=pl.pelayanan_id","LEFT");
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get();
        $result = $query->num_rows();

        return $result;
    }

    public function getCount($where= ""){
        $result = 0;

        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->num_rows();

        return $result;
    }

    public function insert($param){
        $result = false;
        $this->db->set($param);

        if($this->db->insert($this->table)){
            $result = true;
        }

        return $result;
    }

    public function insertBatch($param){
        $result = false;
        if($this->db->insert_batch($this->table, $param)){
            $data = true;
        }

        return $result;
    }

    public function update($param, $where){
        $result = false;
        $this->db->set($param);
        $this->db->where($where);

        if($this->db->update($this->table)){
            $result = true;
        }

        return $result;
    }

    public function delete($where){
        $result = false;
        $this->db->where($where);
        if($this->db->delete($this->table)){
            $result = true;
        }
        return $result;
    }
    
}
?>
