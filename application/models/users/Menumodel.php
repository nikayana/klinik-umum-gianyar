<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menumodel extends CI_Model{
    //TABLE PROPERTIES
    private $table = "homecare_menu";
    private $table_header = "homecare_menu_header"; 
    public $FIELD_PRIMARY = "menu_id";
    public $FIELD_MENU_NAME = "menu_name";
    public $FIELD_MENU_LINK = "menu_link";
    public $FIELD_HEADER_ID = "header_id";
    public $FIELD_MENU_STATUS = "menu_status";
    public $FIELD_MENU_DATECREATED = "menu_datecreated";
    public $FIELD_MENU_DATEMODIFIED = "menu_datemodified";
    public $FIELD_MENU_MODIFIEDBY = "menu_modifiedby";

    public $ACTIVE_STATUS = 1;
    public $NONACTIVE_STATUS = 0;

    public $STATUS = array(
        "Tidak Aktif",
        "Aktif"
    );

    //RULES
    private $rules;

    function __construct(){
        parent::__construct();

        $this->rules  = array(
            array(
                "name" => "nama",
                "field" =>$this->FIELD_MENU_NAME,
                "required" => true,
            ),
            array(
                "name" => "link",
                "field" => $this->FIELD_MENU_LINK,
                "required" => true,
            ),
            array(
                "name" => "header",
                "field" => $this->FIELD_HEADER_ID,
                "required" => true,
            ),
            array(
                "name" => "status",
                "field" => $this->FIELD_MENU_STATUS,
                "required" => false,
            )
        );
    }

    public function getrules(){
        return $this->rules;
    }

    public function fetch($where = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    public function getAll(){
        $result = array();
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

    public function getList($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getListJoin($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        $this->db->select("h.header_name, m.*");
        $this->db->from($this->table." AS m");
        $this->db->join($this->table_header." AS h", "m.".$this->FIELD_HEADER_ID."=h.header_id", "LEFT");
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get();
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getCount($where= ""){
        $result = 0;

        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($goup)){
            $this->db->group_by($group);
        }

        $query = $this->db->get($this->table);
        $result = $query->num_rows();

        return $result;
    }

    public function getCountJoin($where= ""){
        $result = 0;
        
        $this->db->from($this->table." AS m");
        $this->db->join($this->table_header." AS h", "m.".$this->FIELD_HEADER_ID."=h.header_id", "LEFT");

        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($goup)){
            $this->db->group_by($group);
        }

        $query = $this->db->get();
        $result = $query->num_rows();

        return $result;
    }

    public function insert($param){
        $result = false;
        $this->db->set($param);

        if($this->db->insert($this->table)){
            $result = true;
        }

        return $result;
    }

    public function insertBatch($param){
        $result = false;
        if($this->db->insert_batch($this->table, $param)){
            $data = true;
        }

        return $result;
    }

    public function update($param, $where){
        $result = false;
        $this->db->set($param);
        $this->db->where($where);

        if($this->db->update($this->table)){
            $result = true;
        }

        return $result;
    }

    public function delete($where){
        $result = false;
        $this->db->where($where);
        if($this->db->delete($this->table)){
            $result = true;
        }
        return $result;
    }
    
}
?>
