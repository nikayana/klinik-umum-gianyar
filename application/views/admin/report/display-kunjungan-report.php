<html>
    <head>
        <title>Laporan Kunjungan </title>
    </head>
    <body>
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <thead>
                <tr>
                    <th style='padding:5px;' align="center">
                        <center>
                            <img src="<?php echo isset($profile->profile_logo) ? site_url("assets/images/profile/".$profile->profile_logo) : "" ?>" width="100">
                            <h3>
                                Laporan Kunjungan Home Care<br>
                                <?php echo isset($profile->profile_name) ? $profile->profile_name : "" ?>
                            </h3>
                            <?php echo isset($profile->profile_address) ? $profile->profile_address : "" ?><br>
                            Telepon: <?php echo isset($profile->profile_phone) ? $profile->profile_phone : "" ?>. Email: <?php echo isset($profile->profile_email) ? $profile->profile_email : "" ?>.<br>
                            
                        </center>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="padding:5px;">
                        <table border="1" cellpadding="1" cellspacing="1" width="100%">
                            <thead>
                                <tr>
                                    <th style="padding:5px;">No</th>
                                    <th style="padding:5px;">Tgl Kunjungan</th>
                                    <th style="padding:5px;">Jam Kunjungan</th>
                                    <th style="padding:5px;">No Rekam Medis</th>
                                    <th style="padding:5px;">Pasien</th>
                                    <th style="padding:5px;">Jenis Kelamin</th>
                                    <th style="padding:5px;">Alamat</th>
                                    <th style="padding:5px;">Pedaftar</th>
                                    <th style="padding:5px;">No HP Pendaftar</th>
                                    <th style="padding:5px;">Dokter</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if(isset($data)){
                                        foreach($data AS $key => $value){
                                            ?>
                                            <tr>
                                                <td style="padding:5px;"><?php echo $key+1 ?></td>
                                                <td style="padding:5px;"><?php echo date("d F Y", strtotime($value->kunjungan_date)) ?></td>
                                                <td style="padding:5px;"><?php echo date("H:i", strtotime($value->kunjungan_time)) ?></td>
                                                <td style="padding:5px;"><?php echo $value->registrasi_norekam ?></td>
                                                <td style="padding:5px;"><?php echo $value->pasien_name ?></td>
                                                <td style="padding:5px;"><?php echo $jk[$value->pasien_jk] ?></td>
                                                <td style="padding:5px;"><?php echo $value->pasien_alamat ?></td>
                                                <td style="padding:5px;"><?php echo $value->registrasi_name ?></td>
                                                <td style="padding:5px;"><?php echo $value->registrasi_phone ?></td>
                                                <td style="padding:5px;"><?php echo $value->dokter_name ?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                ?>
                                
                            </tbody>
                        </table>
                    </td>
            </tbody>
        </table>
    </body>
</html>