<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">
                    <b>Pengguna</b>
                </h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard") ?>">Dashboard</a></li>
                    <li class="breadcrumb-item active">Pengguna</li>
                </ol>
                <?php 
                    if(isset($priv->access_add) && $priv->access_add == 1){
                        ?>
                        <button type="button" class="btn btn-success addedit m-l-15" data-modal="#editor" data-url="<?php echo site_url("settings/users/form") ?>" data-id="" data-title="Tambah Data Pengguna"><i class='fa fa-plus-circle'></i> Tambah Data</button>
                        <?php
                    }
                ?>
                <button type="button" class="btn btn-info m-l-15" id="searchformbtn"><i class='fa fa-search'></i> Pencarian</button>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        
        <!-- ============================================================== -->
        <!-- Sales Chart and browser state-->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-lg-12">
                <div class='card' id="searchform" style="display:none;">
                    <div class="card-header">
                        <h4 class="card-title">Form Pencarian</h4>
                    </div>
                    <div class="card-body">
                        <form id="search">
                            <div class='row'>
                                <div class="col-md-4">
                                    <label for="user_name">Nama Pengguna</label>
                                    <input type="text" name="user_name" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label for="user_email">Email</label>
                                    <input type="email" name="user_email" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <label for="privilege_id">Hak Akses </label>
                                    <select name="privilege_id" class="select2ajax form-control custom-select" style="width:100%" data-url="<?php echo site_url("settings/privilege/selectdata") ?>">
                                        <option value="">Semua Hak Akses</option>
                                        <?php 
                                            if(isset($privilege)){
                                                foreach($privilege AS $key => $value){
                                                    ?>
                                                    <option value='<?php echo $value->privilege_id ?>'><?php echo $value->privilege_name ?></option>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="user_status">Status </label>
                                    <select name="user_status" class="selectmain form-control custom-select" style="width:100%">
                                        <option value="">Semua Status</option>
                                        <?php 
                                            if(isset($status)){
                                                foreach($status AS $key => $value){
                                                    ?>
                                                    <option value='<?php echo $key ?>'><?php echo $value ?></option>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="searchbtn"> &nbsp; </label>
                                    <button name="searchbtn" type="submit" class="btn btn-block btn-success" id="searchbtn"><i class='fa fa-search'></i> Cari</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Column -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><?php echo isset($title) ? $title : "" ?></h4>
                        <h6 class="card-subtitle"> <?php echo isset($description) ? $description : "" ?> </h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="tables" class="table display table-bordered table-striped no-wrap" data-url="<?php echo site_url("settings/users/datatables") ?>">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th>Hak Akses</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        
        <!-- ============================================================== -->
        <!-- Comment - chats -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- End Comment - chats -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</div>
<div class="modal fade" id="editor" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	   <div class="modal-dialog modal-lg" role="document">
		     <div class="modal-content">
              <form id="formdata" data-url="<?php echo site_url("settings/users/save") ?>" data-modal="#editor" class="needs-validation" novalidate>
			        <div class="modal-header">
			            <h5 class="modal-title">Modal title</h5>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
		                </button>
                    </div>
	                <div class="modal-body">
				            
        			</div>
        			<div class="modal-footer">
                        
                        <div class="col-md-12 mb-3">
                            <div class="alert" id="notif" style="display:none;"></div>
                        </div>
        				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Tutup</button>
        				<button type="submit" class="btn btn-success" id="savebtn"><i class='fa fa-check'></i> Simpan</button>
        			</div>
              </form>
          </div>
     </div>
</div>