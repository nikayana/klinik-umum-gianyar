<input type="hidden" name="id" value="<?php echo isset($data->jadwal_id) ? $data->jadwal_id : 0 ?>">
<input type="hidden" name="jadwal_modifiedby" value="<?php echo isset($profile->user_id) ? $profile->user_id : "" ?>">
<div class="form-material">
    <div class="row">
        <div class="col-md-12 b-r">
            <div class="form-group">
                <label>Dokter <font color='red'>*</font></label>
                <select class='form-control form-control-line selects' name="dokter_id" required="true" style="width:100%">
                    <option value=""> Pilih Dokter </option>
                    <?php
                        if(isset($dokter)){
                            foreach($dokter AS $key => $value){
                                $selected = "";
                                if(isset($data->dokter_id) && $data->dokter_id == $value->dokter_id){
                                    $selected = "selected='selected'";
                                }
                                ?>
                                <option value='<?php echo $value->dokter_id ?>' <?php echo $selected ?>><?php echo $value->dokter_name ?></option>
                                <?php
                                
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-md-12 b-r">
            <div class="form-group">
                <label>Status <font color='red'>*</font></label>
                <select class='form-control form-control-line' name="jadwal_hari" required="true">
                    <option value=""> Pilih Hari </option>
                    <?php
                        if(isset($hari)){
                            foreach($hari AS $key => $value){
                                $selected = "";
                                if(isset($data->jadwal_hari) && $data->jadwal_hari == $key){
                                    $selected = "selected='selected'";
                                }
                                if($key > 0){
                                    ?>
                                    <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                                    <?php
                                }
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-md-6 b-r">
            <div class="form-group">
                <label>Jam Mulai <font color='red'>*</font></label>
                <input type="text" name="jadwal_start" class="form-control form-control-line clockpicker" value="<?php echo isset($data->jadwal_start) ? $data->jadwal_start : "" ?>" placeholder='Isikan jam mulai' required="true"> 
            </div>
        </div>
        <div class="col-md-6 b-r">
            <div class="form-group">
                <label>Jam Selesai <font color='red'>*</font></label>
                <input type="text" name="jadwal_end" class="form-control form-control-line clockpicker" value="<?php echo isset($data->jadwal_end) ? $data->jadwal_end : "" ?>" placeholder='Isikan jam selesai' required="true"> 
            </div>
        </div>
    </div>
</div>
