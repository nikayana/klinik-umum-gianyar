<?php
require_once 'application/libraries/vendor/autoload.php';
defined('BASEPATH') OR exit('No direct script access allowed');
class Kunjungan extends HC_Controller {
    private $response;
    private $modul;
    private $priv;

	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->response['status'] = 404;
        $this->response['messages'] = "Unknown Error, Please contact your administrator";
        $this->response['datas'] = array();
        $this->load->model("operasional/registrasimodel", "registrasi");
        $this->load->model("profile/pelayananmodel", "pelayanan");
        $this->load->model("profile/perawatanmodel", "perawatan");
        $this->load->model("master/doktermodel", "dokter");
        $this->load->model("master/Icd10model", "Icd10model");
        $this->load->model("operasional/pasienmodel", "pasien");
        $this->load->model("operasional/kunjunganmodel", "kunjungan");
        $this->load->model("operasional/kunjunganitemmodel", "kunjunganpelayanan");

        $this->modul = "operasional/kunjungan";
        $this->priv = $this->checkpriv($this->modul);

    }

    public function index(){
        $data = array();
        $data['title'] = "Pemeriksaan";
        $data['description'] = "Pelayanan Pemeriksaan Dokter";
        $data['dokter'] = $this->dokter->getList(0,0,$this->dokter->FIELD_DOKTER_STATUS."=".$this->dokter->ACTIVE_STATUS,"","");
        $data['priv'] = $this->priv;
        if(isset($this->priv->{$this->access->FIELD_ACCESS_VIEW}) && $this->priv->{$this->access->FIELD_ACCESS_VIEW} == $this->access->ACCESS){
            $this->theme("admin/kunjungan/main-kunjungan", $data);
        }else{
            redirect("notfound");
        }
        
    }

    

    public function datatables(){
        $result = array();
        
        $param[$this->kunjungan->FIELD_KUNJUNGAN_DATE] = date("Y-m-d");
        $param[$this->kunjungan->FIELD_DOKTER_ID] = "";
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $aColumns = array("kunjungan_id","registrasi_norekam",   "kunjungan_date", "pasien_name", "dokter_name", "kunjungan_datemodified");

        $sLimit = 20;
        $sOffset = 0;
        $sOrder = "";
        $sWhere = "";
        $sGroup = "";
        $sEcho = 0;
        if(isset($param["sEcho"])){
            $sEcho = 0;
        }
        if(isset($param['start']) && $param['start'] > 0){
            $sOffset = $param['start'];
        }
        
        if(isset($param['order'])){
            if(empty($sOrder)){
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }else{
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }
        }

        $sWhere = "1=1";
        if(isset($param[$this->pasien->FIELD_PASIEN_NAME]) && !empty($param[$this->pasien->FIELD_PASIEN_NAME])){
            $sWhere .=" AND LOWER(p.".$this->pasien->FIELD_PASIEN_NAME.") LIKE '%".strtolower($param[$this->pasien->FIELD_PASIEN_NAME])."%'";
        }

        if(isset($param[$this->registrasi->FIELD_REGISTRASI_NOREKAM]) && !empty($param[$this->registrasi->FIELD_REGISTRASI_NOREKAM])){
            $sWhere .=" AND LOWER(r.".$this->registrasi->FIELD_REGISTRASI_NOREKAM.") LIKE '%".strtolower($param[$this->registrasi->FIELD_REGISTRASI_NOREKAM])."%'";
        }

        if(isset($param[$this->kunjungan->FIELD_KUNJUNGAN_DATE]) && !empty($param[$this->kunjungan->FIELD_KUNJUNGAN_DATE])){
            $sWhere .=" AND k.".$this->kunjungan->FIELD_KUNJUNGAN_DATE."='".$param[$this->kunjungan->FIELD_KUNJUNGAN_DATE]."'";
        }

        if(isset($param[$this->kunjungan->FIELD_DOKTER_ID]) && !empty($param[$this->kunjungan->FIELD_DOKTER_ID])){
            $sWhere .=" AND k.".$this->kunjungan->FIELD_DOKTER_ID."='".$param[$this->kunjungan->FIELD_DOKTER_ID]."'";
        }

        if($this->priv->{$this->privilege->FIELD_PRIVILEGE_SHOW_DATA} == $this->privilege->SELF_DATA){
            $sWhere .=" AND k.".$this->kunjungan->FIELD_DOKTER_ID."=".$this->session->userdata("dokter");
        }

        $searchWhere = "";
        if(!empty($param['search']['value'])){

            for($i=0;$i<count($aColumns);$i++){
                if(empty($searchWhere)){
                    $searchWhere .=$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }else{
                    $searchWhere .=" OR ".$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }

            }

            if(!empty($sWhere)){
                $sWhere .=" AND (".$searchWhere.")";
            }else{
                $sWhere .= "(".$searchWhere.")";
            }

        }
        $sGroup = "k.kunjungan_id";
        $sTotal = $this->kunjungan->getCountJoin($sWhere);
        $data = $this->kunjungan->getListJoin($sOffset,$sLimit,$sWhere,$sOrder,$sGroup);
        $output = array(
            "sEcho" => intval($sEcho),
            "iTotalRecords" => $sTotal,
            "iTotalDisplayRecords" => $sTotal,
            "aaData" => array()
        );

        foreach ($data AS $key => $value){
            $row = array();
            $button = "";

            if(isset($this->priv->{$this->access->FIELD_ACCESS_UPDATE}) && $this->priv->{$this->access->FIELD_ACCESS_UPDATE} == $this->access->ACCESS){
                $button .="<button type='button' data-url='". site_url("operasional/kunjungan/form")."' data-id='".$value->{$this->kunjungan->FIELD_PRIMARY}."' class='btn btn-warning btn-xs m-t-10 addedit' data-toggle='tooltip' data-original-title='Edit' data-placement='top' style='margin-right:5px;' data-modal='#editor' data-title='Ubah Data Kunjungan'><i class='fas fa-edit'></i></button>";
                $button .="<a href='". site_url("operasional/kunjungan/screening_new/".$value->{$this->kunjungan->FIELD_PRIMARY})."' class='btn btn-success btn-xs m-t-10' data-toggle='tooltip' data-original-title='Pemeriksaan Dokter' data-placement='top' style='margin-right:5px;' data-modal='#editor'><i class='fas fa-medkit'></i></a>";
                /* $button .="<a href='". site_url("operasional/kunjungan/screening/".$value->{$this->kunjungan->FIELD_PRIMARY})."' class='btn btn-success btn-xs m-t-10' data-toggle='tooltip' data-original-title='Edit' data-placement='top' style='margin-right:5px;' data-modal='#editor'><i class='fas fa-upload'></i></a>"; */
                /* $button .="<a href='". site_url("operasional/kunjungan/print/".$value->{$this->kunjungan->FIELD_PRIMARY})."' class='btn btn-default btn-xs m-t-10' data-toggle='tooltip' data-original-title='Edit' data-placement='top' style='margin-right:5px;' data-modal='#editor'><i class='fas fa-print'></i></a>"; */
            }
            if(isset($this->priv->{$this->access->FIELD_ACCESS_DELETE}) && $this->priv->{$this->access->FIELD_ACCESS_DELETE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->kunjungan->FIELD_PRIMARY}."' data-url='". site_url("operasional/kunjungan/deleteExc")."' class='delete btn btn-danger btn-xs m-t-10' data-toggle='tooltip' data-original-title='Hapus' data-placement='top' style='margin-right:5px;'><i class='fa fa-trash'></i></button>";
            }

            for($i = 0; $i < count($aColumns); $i++){
                    $reasstype = "";
                    if($aColumns[$i] == $this->kunjungan->FIELD_PRIMARY){
                            $row[] = $sOffset+$key+1;
                    }else if($aColumns[$i] == $this->kunjungan->FIELD_KUNJUNGAN_DATEMODIFIED){
                            $row[] = $button;
                    }else{
                            $row[] = $value->{$aColumns[$i]};
                    }


            }

            $row[] = $button;
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function form(){
		$data = array();
		$param = $this->input->post(NULL, TRUE);
		$param = $this->antiinjection->antiinject($param);
		$where = "";
        //GET Data
        $subwhere = "(SELECT ".$this->kunjungan->FIELD_REGISTRASI_ID." FROM homecare_kunjungan)";
        $data['data'] = $this->kunjungan->fetch($this->kunjungan->FIELD_PRIMARY."='".$param['id']."'");
        $data['dokter'] = $this->dokter->getList(0,0,$this->dokter->FIELD_DOKTER_STATUS."=".$this->dokter->ACTIVE_STATUS,"","");
        $data['registrasi'] = $this->registrasi->getListJoin(0,0,"r.".$this->registrasi->FIELD_PRIMARY." NOT IN ".$subwhere,"","");
        $data['profile'] = $this->getprofile();

		$this->response['html'] = $this->load->view("admin/kunjungan/kunjungan-form", $data, TRUE);
		$this->response['status'] = 200;
		$this->response['messages'] = '';
        $this->response['datas'] = array();

		echo json_encode($this->response);
    }

    /* public function screening($id = NULL){
        $data = array();
        $param = array();
        $param['id'] = 0;
        if(!empty($id)){
            $param['id'] = $id;
        }
		$param = $this->antiinjection->antiinject($param);
		$where = "";
        //GET Data
        $data['data'] = $this->kunjungan->fetchJoin($this->kunjungan->FIELD_PRIMARY."='".$param['id']."'");
        $data['profile'] = $this->getprofile();
        $data['pasien'] = $this->kunjungan->RSUD;
        $data['jk'] = $this->pasien->JK;
        $data['atm'] = $this->kunjungan->ATM;
        $data['internet'] = $this->kunjungan->INTERNET;
        $data['kendaraan'] = $this->kunjungan->KENDARAAN;
        $data['title'] = "Kunjungan";
        $data['description'] = "Kunjungan Pelayanan";
        $data['priv'] = $this->priv;
        if(isset($this->priv->{$this->access->FIELD_ACCESS_UPDATE}) && $this->priv->{$this->access->FIELD_ACCESS_UPDATE} == $this->access->ACCESS){
            $this->theme("admin/kunjungan/form-screening", $data);
        }else{
            redirect("notfound");
        }
    } */

    public function screening_new($id = NULL){
        $data = array();
        $param = array();
        $param['id'] = 0;
        if(!empty($id)){
            $param['id'] = $id;
        }
		$param = $this->antiinjection->antiinject($param);
		$where = "";
        //GET Data
        $data['data'] = $this->kunjungan->fetchJoin($this->kunjungan->FIELD_PRIMARY."='".$param['id']."'");
        //echo "<pre>"; print_r($data['data']); die;
        $data['profile'] = $this->getprofile();
        $data['pasien'] = $this->kunjungan->RSUD;
        $data['dokter'] = $this->dokter->getAll();
        $data['jk'] = $this->pasien->JK; 
        $data['icd10'] = $this->Icd10model->getAll();
        $data['title'] = "Kunjungan";
        $data['description'] = "Kunjungan Pelayanan";
        $data['priv'] = $this->priv;
        
        if(isset($this->priv->{$this->access->FIELD_ACCESS_UPDATE}) && $this->priv->{$this->access->FIELD_ACCESS_UPDATE} == $this->access->ACCESS){
            $this->theme("admin/kunjungan/form-screening-new", $data);
        }else{
            redirect("notfound");
        }
    }
    
    public function save(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        
        $filter = $this->checkparam($this->kunjungan->getrules(), $param);
        if(count($filter) == 0){
            $data = $this->kunjungan->fetch($this->kunjungan->FIELD_PRIMARY."='".$param['id']."'");
            $param[$this->kunjungan->FIELD_KUNJUNGAN_DATEMODIFIED] = date("Y-m-d H:i:s");
            if(isset($data->{$this->kunjungan->FIELD_PRIMARY})){
                 $this->updateExc($param);
            }else{
                $param[$this->kunjungan->FIELD_KUNJUNGAN_DATECREATED] = date("Y-m-d H:i:s");
                 $this->insertExc($param);
            }
        }else{
            $this->response = $filter;
        }
        
        echo json_encode($this->response);
    }

    public function updateExc($param){
        $id = $param['id'];

        unset($param['id']);
        $result = $this->kunjungan->update($param, $this->kunjungan->FIELD_PRIMARY."='".$id."'");

        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data Pemeriksaan berhasil diperbaharui';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function insertExc($param){

        unset($param['id']);
        $result = $this->kunjungan->insert($param);
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data Pemeriksaan berhasil diperbaharui';
        }else{
            $this->response['status'] = 500;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function deleteExc(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $result = $this->kunjungan->delete($this->kunjungan->FIELD_PRIMARY."='".$param['id']."'");
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data Pemeriksaan berhasil dihapus';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menghapus data, silahkan ulangi kembali';
        }

        echo json_encode($this->response);
    }

    public function print($id){

        $mpdf = new \Mpdf\Mpdf();
        $data = array();
        $data['profile'] = $this->getcomprof();
        $data['data'] = $this->kunjungan->fetchJoin("k.".$this->kunjungan->FIELD_PRIMARY."=".$id);
        $data['jk'] = $this->pasien->JK;
        $data['status'] = $this->pasien->STATUS;
        $data['gol'] = $this->pasien->GOL;
        $data['pasientype'] = $this->kunjungan->RSUD;
        $data['atm'] = $this->kunjungan->ATM;
        $data['internet'] = $this->kunjungan->INTERNET;
        $data['kendaraan'] = $this->kunjungan->KENDARAAN;

        $listpelayanan = array();
        $datapelayanan = $this->pelayanan->getList(0,0,$this->pelayanan->FIELD_PELAYANAN_STATUS."=".$this->pelayanan->ACTIVE_STATUS,"","");
        foreach($datapelayanan AS $key => $value){
            $checked = 0;
            $itemtpelayanan['name'] = $value->{$this->pelayanan->FIELD_PELAYANAN_NAME};
            $itemtpelayanan['perawatan'] = array();
            $dataperawatan = $this->perawatan->getList(0,0,$this->perawatan->FIELD_PELAYANAN_ID."=".$value->{$this->pelayanan->FIELD_PRIMARY}." AND ".$this->perawatan->FIELD_PERAWATAN_STATUS."=".$this->perawatan->ACTIVE_STATUS,"","");
            foreach($dataperawatan AS $keys => $values){
                $isbooked = $this->kunjunganpelayanan->getCount($this->kunjunganpelayanan->FIELD_PERAWATAN_ID."=".$values->{$this->perawatan->FIELD_PRIMARY}." AND ".$this->kunjunganpelayanan->FIELD_KUNJUNGAN_ID."=".$id);
                $listperawatan = array();
                $listperawatan['name'] = $values->{$this->perawatan->FIELD_PERAWATAN_NAME};
                $listperawatan['booked'] = $isbooked;
                $itemtpelayanan['perawatan'][] = $listperawatan;
                $checked+=$isbooked;
            }

            $itemtpelayanan['booked'] = $checked;
            $listpelayanan[] = $itemtpelayanan;
        }
        $data['pelayanan'] = $listpelayanan;
        $mpdf->WriteHTML($this->load->view("admin/kunjungan/print-kunjungan", $data, TRUE));
        $mpdf->Output("Form Screening.pdf", 'I');

    }

}