<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menuheadermodel extends CI_Model{
    //TABLE PROPERTIES
    private $table = "homecare_menu_header";
    public $FIELD_PRIMARY = "header_id";
    public $FIELD_HEADER_NAME = "header_name";
    public $FIELD_HEADER_STATUS = "header_status";
    public $FIELD_HEADER_DATECREATED = "header_datecreated";
    public $FIELD_HEADER_DATEMODIFIED = "header_datemodified";
    public $FIELD_HEADER_MODIFIEDBY = "header_modifiedby";
    public $FIELD_HEADER_POSITION = "header_position";
    public $FIELD_HEADER_ICON = "header_icon";

    public $ACTIVE_STATUS = 1;
    public $NONACTIVE_STATUS = 0;
    public $STATUS = array(
        "Non Aktif",
        "Aktif"
    );

    //RULES
    private $rules;

    function __construct(){
        parent::__construct();

        $this->rules  = array(
            array(
                "name" => "nama header menu",
                "field" =>$this->FIELD_HEADER_NAME,
                "required" => true,
            ),
            array(
                "name" => "status",
                "field" => $this->FIELD_HEADER_STATUS,
                "required" => false,
            ),
            array(
                "name" => "posisi",
                "field" => $this->FIELD_HEADER_POSITION,
                "required" => true,
            ),
            array(
                "name" => "icon",
                "field" => $this->FIELD_HEADER_ICON,
                "required" => true,
            )
        );
    }

    public function getrules(){
        return $this->rules;
    }
    public function fetch($where = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    public function getAll(){
        $result = array();
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

    public function getMaxPosition(){
        $result = 0;
        $this->db->select_max($this->FIELD_HEADER_POSITION);
        $query = $this->db->get($this->table);
        $rows = $query->num_rows();
        if($rows > 0){
            $result = $query->row();
            $result = $result->{$this->FIELD_HEADER_POSITION};
        }

        return $result;
    }

    public function getList($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getCount($where= ""){
        $result = 0;

        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->num_rows();

        return $result;
    }

    public function insert($param){
        $result = false;
        $this->db->set($param);

        if($this->db->insert($this->table)){
            $result = true;
        }

        return $result;
    }

    public function insertBatch($param){
        $result = false;
        if($this->db->insert_batch($this->table, $param)){
            $data = true;
        }

        return $result;
    }

    public function update($param, $where){
        $result = false;
        $this->db->set($param);
        $this->db->where($where);

        if($this->db->update($this->table)){
            $result = true;
        }

        return $result;
    }

    public function delete($where){
        $result = false;
        $this->db->where($where);
        if($this->db->delete($this->table)){
            $result = true;
        }
        return $result;
    }
    
}
?>
