<input type="hidden" name="id" value="<?php echo isset($data->privilege_id) ? $data->privilege_id : 0 ?>">
<input type="hidden" name="privilege_modifiedby" value="<?php echo isset($profile->user_id) ? $profile->user_id : "" ?>">

<div class="form-row">
  <div class="col-md-12 mb-3">
      <label for="privilege_name">Nama Hak Akses <font color='red'>*</font></label>
      <input type="text" name="privilege_name" class="form-control" placeholder="Isikan nama hak akses" value="<?php echo isset($data->privilege_name) ? $data->privilege_name : ""  ?>" required>
  </div>
  <div class="col-md-12 mb-3">
      <label for="privilege_status">Status <font color='red'>*</font></label>
      <select name="privilege_status" class="selects form-control custom-select" style="width:100%">
        <?php 
            if(isset($status)){
                foreach($status AS $key => $value){
                    $selected = "";
                    if(isset($data->privilege_status) && $data->privilege_status == $key){
                        $selected = "selected='selected'";
                    }
                    ?>
                    <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                    <?php
                }
            }
        
        ?>
      </select>
    </div>
    <div class="col-md-12 mb-3">
      <label for="privilege_show_data">Status <font color='red'>*</font></label>
      <select name="privilege_show_data" class="selects form-control custom-select" style="width:100%">
        <?php 
            if(isset($show)){
                foreach($show AS $key => $value){
                    $selected = "";
                    if(isset($data->privilege_show_data) && $data->privilege_show_data == $key){
                        $selected = "selected='selected'";
                    }
                    ?>
                    <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                    <?php
                }
            }
        
        ?>
      </select>
    </div>
</div>
