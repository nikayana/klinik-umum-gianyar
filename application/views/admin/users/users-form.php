<input type="hidden" name="id" value="<?php echo isset($data->user_id) ? $data->user_id : 0 ?>">
<input type="hidden" name="user_modifiedby" value="<?php echo isset($profile->user_id) ? $profile->user_id : "" ?>">
<div class="form-row">
  <div class="col-md-6">
    <div class="col-md-12 mb-3">
        <label for="user_name">Nama Pengguna <font color='red'>*</font></label>
        <input type="text" name="user_name" class="form-control" placeholder="Isikan nama lengkap pengguna" value="<?php echo isset($data->user_name) ? $data->user_name : ""  ?>" required>
    </div>
    <div class="col-md-12 mb-3">
        <label for="user_address">Alamat</label>
        <input type="text" name="user_address" class="form-control" placeholder="Isikan alamat pengguna" value="<?php echo isset($data->user_address) ? $data->user_address : ""  ?>">
    </div>
    <div class="col-md-12 mb-3">
        <label for="user_phone">Telepon</label>
        <input type="text" name="user_phone" class="form-control" placeholder="Isikan no telepon pengguna" value="<?php echo isset($data->user_phone) ? $data->user_phone : ""  ?>">
    </div>
    <div class="col-md-12 mb-3">
        <label for="user_email">Email <font color='red'>*</font></label>
        <input type="email" name="user_email" class="form-control" placeholder="Isikan alamat pengguna" value="<?php echo isset($data->user_email) ? $data->user_email : ""  ?>" required>
    </div>
    <div class="col-md-12 mb-3">
        <label for="dokter_id">Sebagai Dokter <small>* pilih bila user sebagai dokter</small></label>
        <select name="dokter_id" class="selects form-control custom-select" style="width:100%">
            <option value="">Pilih Dokter</option>
            <?php 
                if(isset($dokter)){
                    foreach($dokter AS $key => $value){
                        $selected = "";
                        if(isset($data->dokter_id) && $data->dokter_id == $value->dokter_id){
                            $selected = "selected='selected'";
                        }
                        ?>
                        <option value='<?php echo $value->dokter_id ?>' <?php echo $selected ?>><?php echo $value->dokter_name ?></option>
                        <?php
                    }
                }
            
            ?>
        </select>
    </div>
  </div>
  <div class="col-md-6">
    <div class='col-md-12 mb-3'>
      <label for="privilege_id">Hak Akses <font color='red'>*</font></label>
      <select name="privilege_id" class="selects form-control custom-select" style="width:100%" required>
        <option value="">Pilih Hak Akses</option>
        <?php 
            if(isset($privilege)){
                foreach($privilege AS $key => $value){
                    $selected = "";
                    if(isset($data->privilege_id) && $data->privilege_id == $value->privilege_id){
                        $selected = "selected='selected'";
                    }
                    ?>
                    <option value='<?php echo $value->privilege_id ?>' <?php echo $selected ?>><?php echo $value->privilege_name ?></option>
                    <?php
                }
            }
        
        ?>
      </select>
    </div>
    <div class="col-md-12 mb-3">
      <label for="user_username">Username <font color='red'>*</font></label>
      <input type="text" name="user_username" class="form-control" placeholder="Isikan username pengguna" value="<?php echo isset($data->user_username) ? $data->user_username : ""  ?>" <?php echo isset($data->user_username) ? "readonly" : "required" ?>>
    </div>
    <div class="col-md-12 mb-3">
        <label for="user_password">Password <small style='color:red'>Isikan bila ingin mengubah kata sandi</small></label>
        <input type="password" name="user_password" class="form-control" placeholder="Isikan kata sandi" >
    </div>
    <div class="col-md-12 mb-3">
        <label for="user_status">Status <font color='red'>*</font></label>
        <select name="user_status" class="selects form-control custom-select" style="width:100%">
            <?php 
                if(isset($status)){
                    foreach($status AS $key => $value){
                        $selected = "";
                        if(isset($data->user_status) && $data->user_status == $key){
                            $selected = "selected='selected'";
                        }
                        ?>
                        <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                        <?php
                    }
                }
            
            ?>
        </select>
    </div>
  </div>
  
</div>
