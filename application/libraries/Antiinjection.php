<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AntiInjection
 *
 * @author STAF IT
 */
class AntiInjection{
    
    public function antiinject($param){
        foreach ($param AS $key => $value){
            if(is_array($value)){
                $param[$key] = $this->antiinject($value);
            }else{
                $param[$key] = preg_replace($this->my_Sql_regcase("/(from|select|insert|delete|where|drop table|like|show tables|\’|'\| |=|;|,|\|’|<|>|#|\*|–|\\\\)/"), "" ,$param[$key]);

				$param[$key] = trim($param[$key]);
				$param[$key] = strip_tags($param[$key]);
				$param[$key] = addslashes($param[$key]);
            }
        }
        return $param;
    }
	
	private function my_Sql_regcase($str){

		$res = "";

		$chars = str_split($str);
		foreach($chars as $char){
			if(preg_match("/[A-Za-z]/", $char))
				$res .= "[".mb_strtoupper($char, 'UTF-8').mb_strtolower($char, 'UTF-8')."]";
			else
				$res .= $char;
		}

		return $res;
	}
}
