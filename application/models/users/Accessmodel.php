<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accessmodel extends CI_Model{
    //TABLE PROPERTIES
    private $table = "homecare_access";
    private $table_menu = "homecare_menu";
    private $table_menu_header = "homecare_menu_header";
    private $table_privilege = "homecare_privilege";
    public $FIELD_PRIMARY = "access_id";
    public $FIELD_MENU_ID = "menu_id";
    public $FIELD_ACCESS_ADD = "access_add";
    public $FIELD_ACCESS_VIEW = "access_view";
    public $FIELD_ACCESS_UPDATE = "access_update";
    public $FIELD_ACCESS_DELETE = "access_delete";
    public $FIELD_ACCESS_DATECREATED = "access_datecreated";
    public $FIELD_ACCESS_DATEMODIFIED = "access_datemodified";
    public $FIELD_ACCESS_MODIFIEDBY = "access_modifiedby";
    public $FIELD_PRIVILEGE_ID = "privilege_id";

    public $ACCESS = 1;
    public $NONACCCESS = 0;
    public $ACCESSES = array(
        "Tidak",
        "Ya"
    );

    //RULES
    private $rules;

    function __construct(){
        parent::__construct();

        $this->rules  = array(
            array(
                "name" => "menu",
                "field" =>$this->FIELD_MENU_ID,
                "required" => true,
            ),
            array(
                "name" => "add",
                "field" => $this->FIELD_ACCESS_ADD,
                "required" => false,
            ),
            array(
                "name" => "view",
                "field" => $this->FIELD_ACCESS_VIEW,
                "required" => false,
            ),
            array(
                "name" => "update",
                "field" => $this->FIELD_ACCESS_UPDATE,
                "required" => false,
            ),
            array(
                "name" => "delete",
                "field" => $this->FIELD_ACCESS_DELETE,
                "required" => false,
            ),
            array(
                "name" => "hakakses",
                "field" => $this->FIELD_PRIVILEGE_ID,
                "required" => true,
            )
        );
    }

    public function getrules(){
        return $this->rules;
    }

    public function fetch($where = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    public function fetchJoin($where = ""){
        $result = array();
        $this->db->select("a.*, p.privilege_show_data");
        $this->db->from($this->table." AS a");
        $this->db->join($this->table_menu." AS m", "a.".$this->FIELD_MENU_ID."=m.menu_id AND m.menu_status='1'", "INNER");
        $this->db->join($this->table_privilege." AS p", "a.".$this->FIELD_PRIVILEGE_ID."=p.privilege_id", "INNER");
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get();
        if($query->num_rows() > 0){
            $result = $query->row();
        } 
        return $result;
    }

    public function getAll(){
        $result = array();
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

    public function getList($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getListJoin($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        $this->db->select("mh.header_id, mh.header_icon, mh.header_name, m.menu_name, m.menu_link, a.*");
        $this->db->from($this->table." AS a");
        $this->db->join($this->table_menu." AS m","a.".$this->FIELD_MENU_ID."=m.menu_id", "LEFT");
        $this->db->join($this->table_menu_header." AS mh","m.header_id"."=mh.header_id", "LEFT");
        $this->db->join($this->table_privilege." AS p","a.".$this->FIELD_PRIVILEGE_ID."=p.privilege_id", "LEFT");
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get();
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        
        return $result;
    }

    public function getCount($where= ""){
        $result = 0;

        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($goup)){
            $this->db->group_by($group);
        }

        $query = $this->db->get($this->table);
        $result = $query->num_rows();

        return $result;
    }

    public function getCountJoin($where= ""){
        $result = 0;
        $this->db->from($this->table." AS a");
        $this->db->join($this->table_menu." AS m","a.".$this->FIELD_MENU_ID."=m.menu_id", "LEFT");
        $this->db->join($this->table_menu_header." AS mh","m.header_id"."=mh.header_id", "LEFT");
        $this->db->join($this->table_privilege." AS p","a.".$this->FIELD_PRIVILEGE_ID."=p.privilege_id", "LEFT");
        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($goup)){
            $this->db->group_by($group);
        }

        $query = $this->db->get();
        $result = $query->num_rows();

        return $result;
    }

    public function insert($param){
        $result = false;
        $this->db->set($param);

        if($this->db->insert($this->table)){
            $result = true;
        }

        return $result;
    }

    public function insertBatch($param){
        $result = false;
        if($this->db->insert_batch($this->table, $param)){
            $data = true;
        }

        return $result;
    }

    public function update($param, $where){
        $result = false;
        $this->db->set($param);
        $this->db->where($where);

        if($this->db->update($this->table)){
            $result = true;
        }

        return $result;
    }

    public function delete($where){
        $result = false;
        $this->db->where($where);
        if($this->db->delete($this->table)){
            $result = true;
        }
        return $result;
    }
    
}
?>
