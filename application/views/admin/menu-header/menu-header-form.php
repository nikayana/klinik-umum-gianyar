<input type="hidden" name="id" value="<?php echo isset($data->header_id) ? $data->header_id : 0 ?>">
<input type="hidden" name="header_modifiedby" value="<?php echo isset($profile->user_id) ? $profile->user_id : "" ?>">

<div class="form-row">
  <div class="col-md-12 mb-3">
      <label for="header_name">Nama Header Menu <font color='red'>*</font></label>
      <input type="text" name="header_name" class="form-control" placeholder="Isikan nama header menu" value="<?php echo isset($data->header_name) ? $data->header_name : ""  ?>" required>
  </div>
  <div class="col-md-12 mb-3">
      <label for="header_icon">Icon <font color='red'>*</font></label>
      <input type="text" name="header_icon" class="form-control" placeholder="Isikan nama icon" value="<?php echo isset($data->header_icon) ? $data->header_icon : ""  ?>" required>
  </div>
  <div class="col-md-12 mb-3">
      <label for="header_position">Posisi <font color='red'>*</font></label>
      <input type="number" name="header_position" class="form-control" placeholder="Isikan posisi menu header" value="<?php echo isset($data->header_position) ? $data->header_position : $position  ?>" required>
  </div>
  <div class="col-md-12 mb-3">
      <label for="header_status">Status <font color='red'>*</font></label>
      <select name="header_status" class="selects form-control custom-select" style="width:100%">
        <?php 
            if(isset($status)){
                foreach($status AS $key => $value){
                    $selected = "";
                    if(isset($data->header_status) && $data->header_status == $key){
                        $selected = "selected='selected'";
                    }
                    ?>
                    <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                    <?php
                }
            }
        
        ?>
      </select>
  </div>
</div>
