<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Welcome extends HC_Controller {

	private $response;

	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->response['status'] = 200;
        $this->response['messages'] = "";
        $this->response['datas'] = array();

    }

    public function index(){
        $this->load->view("public/welcome");
    }

    public function signup(){
        $this->load->view("public/pendaftar");
    }

    public function activation(){
        $this->load->view("public/activation");
    }

    public function authentication(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        
        //checkrules
        $filter = $this->checkparam($this->users->getloginrules(), $param);
        
        if(count($filter) == 0){
            //CHECK USERNAME & PASSWORD
            $password = $this->ruangencrypt->hash($param[$this->users->FIELD_USER_PASSWORD]);
            $where = $this->users->FIELD_USER_USERNAME."='".$param[$this->users->FIELD_USER_USERNAME]."' AND ".$this->users->FIELD_USER_STATUS."='".$this->users->ACTIVE_STATUS."'";
            $data = $this->users->fetch($where);
            if(!is_null($data)){
                if($this->ruangencrypt->verify($param[$this->users->FIELD_USER_PASSWORD], $data->{$this->users->FIELD_USER_PASSWORD}) == 'true'){

                    $login = array(
                        "username" 	=> $data->{$this->users->FIELD_USER_USERNAME},
                        "name" 		=> $data->{$this->users->FIELD_USER_NAME},
                        "id"		=> $data->{$this->users->FIELD_PRIMARY},
                        "priv"		=> $data->{$this->users->FIELD_PRIVILEGE_ID},
                        "dokter"    => $data->{$this->users->FIELD_DOKTER_ID}
                    );

                    $this->session->set_userdata($login);
                    $this->response['status'] = 200;
                    $this->response['messages'] = 'Anda akan diarahkan kehalaman utama';
                    $this->response['datas'] = array('url' => site_url('dashboard'));
                }else{
                    $this->response['status'] = 404;
                    $this->response['messages'] = '<i class="fa fa-exclamation-circle"></i> Username dan password yang anda masukkan tidak ditemukan';
                    $this->response['datas'] = array();
                }
            }else{
                $this->response['status'] = 404;
                $this->response['messages'] = '<i class="fa fa-exclamation-circle"></i> Username dan password yang anda masukkan tidak ditemukan';
                $this->response['datas'] = array();
            }
        }else{
            $this->response = $filter;
        }

        
		echo json_encode($this->response);
    }

    public function dosignup(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        
        //checkrules
        $filter = $this->checkparam($this->users->getsignuprules(), $param);
        
        if(count($filter) == 0){

            //CHECK EXIST
            $emailexist = $this->users->getCount($this->users->FIELD_USER_EMAIL."='".$param[$this->users->FIELD_USER_EMAIL]."'");
            $usernameexist = $this->users->getCount($this->users->FIELD_USER_USERNAME."='".$param[$this->users->FIELD_USER_USERNAME]."'");
            if($emailexist > 0){
                $this->response['status'] = 404;
                $this->response['messages'] = "<i class='fa fa-exclamation-circle'></i> Email ".$param[$this->users->FIELD_USER_EMAIL]." sudah digunakan";
            }else if($usernameexist > 0){
                $this->response['status'] = 404;
                $this->response['messages'] = "<i class='fa fa-exclamation-circle'></i> Username ".$param[$this->users->FIELD_USER_USERNAME]." sudah digunakan";
            }else{
                $param[$this->users->FIELD_USER_DATECREATED] = date("Y-m-d H:i:s");
                $param[$this->users->FIELD_USER_DATEMODIFIED] = date("Y-m-d H:i:s");
                $param[$this->users->FIELD_USER_MODIFIEDBY] = 0;

                try{
                    $param[$this->users->FIELD_USER_PASSWORD] = $this->ruangencrypt->hash($param[$this->users->FIELD_USER_PASSWORD]);
                    $result = $this->users->insert($param);
                    
                    $this->response['status'] = 200;
                    $this->response['messages'] = "Data berhasil disimpan";
                    $this->response['datas'] = array(
                        "redirect" => site_url("welcome/activation")
                    );
                }catch(Exception $e){
                    $this->response['status'] = 404;
                    $this->response['messages'] = "<i class='fa fa-exclamation-circle'></i> ".$e->getMessage();
                    $this->response['datas'] = array();
                }
            }
            
        }else{
            $this->response = $filter;
        }
        echo json_encode($this->response);
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect("");
    }
}
