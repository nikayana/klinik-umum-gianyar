<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Menu extends HC_Controller {
    private $response;
    private $modul;
    private $priv;
	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->response['status'] = 404;
        $this->response['messages'] = "Unknown Error, Please contact your administrator";
        $this->response['datas'] = array();

        $this->modul = "settings/menu";
        $this->priv = $this->checkpriv($this->modul);

    }

    public function index(){
        $data = array();
        $data['title'] = "Sub Menu";
        $data['description'] = "Mengelola data sub menu header pada sidebar menu";
        $data['status'] = $this->menuheader->STATUS;
        $data["priv"] = $this->priv;
        
        if(isset($this->priv->{$this->access->FIELD_ACCESS_VIEW}) && $this->priv->{$this->access->FIELD_ACCESS_VIEW} == $this->access->ACCESS){
            $this->theme("admin/menu/main-menu", $data);
        }else{
            redirect("notfound");
        }
        
    }

    public function datatables(){
        $result = array();
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $aColumns = array("menu_id","header_name", "menu_name", "menu_status", "menu_datemodified");

        $sLimit = 20;
        $sOffset = 0;
        $sOrder = "";
        $sWhere = "";
        $sGroup = "";
        $sEcho = 0;
        if(isset($param["sEcho"])){
            $sEcho = 0;
        }
        if(isset($param['start']) && $param['start'] > 0){
            $sOffset = $param['start'];
        }
        
        if(isset($param['order'])){
            if(empty($sOrder)){
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }else{
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }
        }

        $sWhere = "1=1";
        if(isset($param[$this->menu->FIELD_HEADER_ID]) && !empty($param[$this->menu->FIELD_HEADER_ID])){
            $sWhere .=" AND m.".$this->menu->FIELD_HEADER_ID."=".$param[$this->menu->FIELD_HEADER_ID];
        }

        if(isset($param[$this->menu->FIELD_MENU_NAME]) && strlen($param[$this->menu->FIELD_MENU_NAME]) > 0){
            $sWhere .=" AND LOWER(m.".$this->menu->FIELD_MENU_NAME.") LIKE '%".$param[$this->menu->FIELD_MENU_NAME]."%'";
        }

        if(isset($param[$this->menu->FIELD_MENU_STATUS]) && strlen($param[$this->menu->FIELD_MENU_STATUS]) > 0){
            $sWhere .=" AND m.".$this->menu->FIELD_MENU_STATUS."=".$param[$this->menu->FIELD_MENU_STATUS];
        }
        $searchWhere = "";
        if(!empty($param['search']['value'])){

            for($i=0;$i<count($aColumns);$i++){
                if(empty($searchWhere)){
                    $searchWhere .=$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }else{
                    $searchWhere .=" OR ".$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }

            }

            if(!empty($sWhere)){
                $sWhere .=" AND (".$searchWhere.")";
            }else{
                $sWhere .= "(".$searchWhere.")";
            }

        }
        $sTotal = $this->menu->getCountJoin($sWhere);
        $data = $this->menu->getListJoin($sOffset,$sLimit,$sWhere,$sOrder,$sGroup);
        $output = array(
            "sEcho" => intval($sEcho),
            "iTotalRecords" => $sTotal,
            "iTotalDisplayRecords" => $sTotal,
            "aaData" => array()
        );

        foreach ($data AS $key => $value){
            $row = array();
            $button = "";

            if(isset($this->priv->{$this->access->FIELD_ACCESS_UPDATE}) && $this->priv->{$this->access->FIELD_ACCESS_UPDATE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->menu->FIELD_PRIMARY}."' data-url='". site_url("settings/menu/form")."' class='addedit btn btn-warning btn-xs m-t-10' data-toggle='tooltip' data-original-title='Edit' data-placement='top' style='margin-right:5px;' data-modal='#editor' data-title='Ubah Data Sub Menu'><i class='fas fa-edit'></i></button>";
            }
            if(isset($this->priv->{$this->access->FIELD_ACCESS_DELETE}) && $this->priv->{$this->access->FIELD_ACCESS_DELETE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->menu->FIELD_PRIMARY}."' data-url='". site_url("settings/menu/deleteExc")."' class='delete btn btn-danger btn-xs m-t-10' data-toggle='tooltip' data-original-title='Hapus' data-placement='top' style='margin-right:5px;'><i class='fa fa-trash'></i></button>";
            }

            for($i = 0; $i < count($aColumns); $i++){
                    $reasstype = "";
                    if($aColumns[$i] == $this->menu->FIELD_PRIMARY){
                            $row[] = $sOffset+$key+1;
                    }else if($aColumns[$i] == $this->menu->FIELD_MENU_DATEMODIFIED){
                            $row[] = $button;
                    }else if($aColumns[$i] == $this->menu->FIELD_MENU_STATUS){
                        $row[] = $this->menuheader->STATUS[$value->{$aColumns[$i]}];
                    }else{
                            $row[] = $value->{$aColumns[$i]};
                    }


            }

            $row[] = $button;
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function form(){
		$data = array();
		$param = $this->input->post(NULL, TRUE);
		$param = $this->antiinjection->antiinject($param);
		$where = "";
		//GET Data
        $data['data'] = $this->menu->fetch($this->menu->FIELD_PRIMARY."='".$param['id']."'");
        $data['status'] = $this->menu->STATUS;
        $data['header'] = $this->menuheader->getAll();
        $data['profile'] = $this->getprofile();

		$this->response['html'] = $this->load->view("admin/menu/menu-form", $data, TRUE);
		$this->response['status'] = 200;
		$this->response['messages'] = '';
        $this->response['datas'] = array();

		echo json_encode($this->response);
    }
    
    public function save(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);

        $filter = $this->checkparam($this->menu->getrules(), $param);
        if(count($filter) == 0){
            $data = $this->menu->fetch($this->menu->FIELD_PRIMARY."='".$param['id']."'");
            $param[$this->menu->FIELD_MENU_DATEMODIFIED] = date("Y-m-d H:i:s");
            if(isset($data->{$this->menu->FIELD_PRIMARY})){
                $this->updateExc($param);
            }else{
                $param[$this->menu->FIELD_MENU_DATECREATED] = date("Y-m-d H:i:s");
                $this->insertExc($param);
            }
        }else{
            $this->response = $filter;
        }
        
        echo json_encode($this->response);
    }

    public function updateExc($param){
        $id = $param['id'];

        unset($param['id']);
        $result = $this->menu->update($param, $this->menu->FIELD_PRIMARY."='".$id."'");

        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data menu berhasil diperbaharui';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function insertExc($param){

        unset($param['id']);
        $result = $this->menu->insert($param);
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data jenis menu berhasil diperbaharui';
        }else{
            $this->response['status'] = 500;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function deleteExc(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $result = $this->menu->delete($this->menu->FIELD_PRIMARY."='".$param['id']."'");
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data menu berhasil dihapus';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menghapus data, silahkan ulangi kembali';
        }

        echo json_encode($this->response);
    }

    
    public function menuaccess(){
        $response = array();
        $result = array();
        $raw = array();
        $raw['id'] = "";
        $raw['text'] = "Semua Menu";
        $result[] = $raw;
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $where = $this->menu->FIELD_MENU_STATUS."=".$this->menu->ACTIVE_STATUS;
        $order = $this->menu->FIELD_MENU_NAME." ASC";
        if(isset($param['term']) && !empty($param['term'])){
            $where .= " AND LOWER(".$this->menu->FIELD_MENU_NAME.") LIKE '%".$param['term']."%'";
            
            $data = $this->menu->getList(0,0,$where, $order, "");
            foreach($data AS $key => $value){
                $raw = array();
                $raw['id'] = $value->{$this->menu->FIELD_PRIMARY};
                $raw['text'] = $value->{$this->menu->FIELD_MENU_NAME};
                $result[] = $raw;
            }
        }
        
        $response['results'] = $result;
        echo json_encode($response);
    }
}