<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doktermodel extends CI_Model{
    //TABLE PROPERTIES
    private $table = "homecare_dokter";
    public $FIELD_PRIMARY = "dokter_id";
    public $FIELD_DOKTER_NAME = "dokter_name";
    public $FIELD_DOKTER_PHONE = "dokter_phone";
    public $FIELD_DOKTER_ALAMAT = "dokter_alamat";
    public $FIELD_DOKTER_EMAIL = "dokter_email";
    public $FIELD_DOKTER_STATUS = "dokter_status";
    public $FIELD_DOKTER_JK = "dokter_jk";
    public $FIELD_DOKTER_PICT = "dokter_pict";
    public $FIELD_DOKTER_DATECREATED = "dokter_datecreated";
    public $FIELD_DOKTER_DATEMODIFIED = "dokter_datemodified";
    public $FIELD_DOKTER_MODIFIEDBY = "dokter_modifiedby";

    public $ACTIVE_STATUS = 1;
    public $NONACTIVE_STATUS = 0;
    public $STATUS = array(
        "Non Aktif",
        "Aktif"
    );

    public $LAKI = 1;
    public $PEREMPUAN = 0;
    public $JK = array(
        "Perempuan",
        "Laki-laki"
    );

    //RULES
    private $rules;

    function __construct(){
        parent::__construct();

        $this->rules  = array(
            array(
                "name" => "nama dokter",
                "field" =>$this->FIELD_DOKTER_NAME,
                "required" => true,
            ),
            array(
                "name" => "status",
                "field" => $this->FIELD_DOKTER_STATUS,
                "required" => false,
            ),
            array(
                "name" => "jenis kelamin dokter",
                "field" => $this->FIELD_DOKTER_JK,
                "required" => false,
            )
        );
    }

    public function getrules(){
        return $this->rules;
    }
    public function fetch($where = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    public function getAll(){
        $result = array();
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

    public function getList($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getCount($where= ""){
        $result = 0;

        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->num_rows();

        return $result;
    }

    public function insert($param){
        $result = false;
        $this->db->set($param);

        if($this->db->insert($this->table)){
            $result = true;
        }

        return $result;
    }

    public function insertBatch($param){
        $result = false;
        if($this->db->insert_batch($this->table, $param)){
            $data = true;
        }

        return $result;
    }

    public function update($param, $where){
        $result = false;
        $this->db->set($param);
        $this->db->where($where);

        if($this->db->update($this->table)){
            $result = true;
        }

        return $result;
    }

    public function delete($where){
        $result = false;
        $this->db->where($where);
        if($this->db->delete($this->table)){
            $result = true;
        }
        return $result;
    }
    
}
?>
