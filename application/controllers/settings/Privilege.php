<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Privilege extends HC_Controller {
    private $response;
    private $priv;
    private $modul;

	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->response['status'] = 404;
        $this->response['messages'] = "Unknown Error, Please contact your administrator";
        $this->response['datas'] = array();
        $this->modul = "settings/privilege";
        $this->priv = $this->checkpriv($this->modul);
    }

    public function index(){
        $data = array();
        $data['title'] = "Hak Akses";
        $data['description'] = "Mengelola data hak akses pengguna";
        $data['status'] = $this->menuheader->STATUS;
        $data['priv'] = $this->priv;
        if(isset($this->priv->{$this->access->FIELD_ACCESS_VIEW}) && $this->priv->{$this->access->FIELD_ACCESS_VIEW} == $this->access->ACCESS){
            $this->theme("admin/privilege/main-privilege", $data);
        }else{
            redirect("notfound");
        }
        
    }

    public function datatables(){
        $result = array();
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $aColumns = array("privilege_id","privilege_name", "privilege_status", "privilege_datemodified");

        $sLimit = 20;
        $sOffset = 0;
        $sOrder = "";
        $sWhere = "";
        $sGroup = "";
        $sEcho = 0;
        if(isset($param["sEcho"])){
            $sEcho = 0;
        }
        if(isset($param['start']) && $param['start'] > 0){
            $sOffset = $param['start'];
        }
        
        if(isset($param['order'])){
            if(empty($sOrder)){
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }else{
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }
        }

        $sWhere = "1=1";
        if(isset($param[$this->privilege->FIELD_PRIVILEGE_NAME]) && !empty($param[$this->privilege->FIELD_PRIVILEGE_NAME])){
            $sWhere .=" AND LOWER(".$this->privilege->FIELD_PRIVILEGE_NAME.") LIKE '%".strtolower($param[$this->privilege->FIELD_PRIVILEGE_NAME])."%'";
        }

        if(isset($param[$this->privilege->FIELD_PRIVILEGE_STATUS]) && strlen($param[$this->privilege->FIELD_PRIVILEGE_STATUS]) > 0){
            $sWhere .=" AND ".$this->privilege->FIELD_PRIVILEGE_STATUS."=".$param[$this->privilege->FIELD_PRIVILEGE_STATUS]."";
        }
        $searchWhere = "";
        if(!empty($param['search']['value'])){

            for($i=0;$i<count($aColumns);$i++){
                if(empty($searchWhere)){
                    $searchWhere .=$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }else{
                    $searchWhere .=" OR ".$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }

            }

            if(!empty($sWhere)){
                $sWhere .=" AND (".$searchWhere.")";
            }else{
                $sWhere .= "(".$searchWhere.")";
            }

        }
        $sTotal = $this->privilege->getCount($sWhere);
        $data = $this->privilege->getList($sOffset,$sLimit,$sWhere,$sOrder,$sGroup);
        $output = array(
            "sEcho" => intval($sEcho),
            "iTotalRecords" => $sTotal,
            "iTotalDisplayRecords" => $sTotal,
            "aaData" => array()
        );

        foreach ($data AS $key => $value){
            $row = array();
            $button = "";

            if(isset($this->priv->{$this->access->FIELD_ACCESS_UPDATE}) && $this->priv->{$this->access->FIELD_ACCESS_UPDATE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->privilege->FIELD_PRIMARY}."' data-url='". site_url("settings/privilege/form")."' class='addedit btn btn-warning btn-xs m-t-10' data-toggle='tooltip' data-original-title='Edit' data-placement='top' style='margin-right:5px;' data-modal='#editor' data-title='Ubah Data Hak Akses'><i class='fas fa-edit'></i></button>";
                $button .="<a href='".site_url("settings/access/main/".md5($value->{$this->privilege->FIELD_PRIMARY}))."' class='btn btn-info btn-xs m-t-10' style='margin-right:5px;'><i class='fa fa-key'></i></a>";
            }

            
            if(isset($this->priv->{$this->access->FIELD_ACCESS_DELETE}) && $this->priv->{$this->access->FIELD_ACCESS_DELETE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->privilege->FIELD_PRIMARY}."' data-url='". site_url("settings/privilege/deleteExc")."' class='delete btn btn-danger btn-xs m-t-10' data-toggle='tooltip' data-original-title='Hapus' data-placement='top' style='margin-right:5px;'><i class='fa fa-trash'></i></button>";
            }

            for($i = 0; $i < count($aColumns); $i++){
                if($aColumns[$i] == $this->privilege->FIELD_PRIMARY){
                        $row[] = $sOffset+$key+1;
                }else if($aColumns[$i] == $this->privilege->FIELD_PRIVILEGE_DATEMODIFIED){
                        $row[] = $button;
                }else if($aColumns[$i] == $this->privilege->FIELD_PRIVILEGE_STATUS){
                    $row[] = $this->privilege->STATUS[$value->{$aColumns[$i]}];
                }else{
                        $row[] = $value->{$aColumns[$i]};
                }
            }

            $row[] = $button;
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function form(){
		$data = array();
		$param = $this->input->post(NULL, TRUE);
		$param = $this->antiinjection->antiinject($param);
		$where = "";
		//GET Data
        $data['data'] = $this->privilege->fetch($this->privilege->FIELD_PRIMARY."='".$param['id']."'");
        $data['status'] = $this->privilege->STATUS;
        $data['profile'] = $this->getprofile();
        $data['show'] = $this->privilege->SHOW_DATA;

		$this->response['html'] = $this->load->view("admin/privilege/privilege-form", $data, TRUE);
		$this->response['status'] = 200;
		$this->response['messages'] = '';
        $this->response['datas'] = array();

		echo json_encode($this->response);
    }
    
    public function save(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);

        $filter = $this->checkparam($this->privilege->getrules(), $param);
        if(count($filter) == 0){
            $data = $this->privilege->fetch($this->privilege->FIELD_PRIMARY."='".$param['id']."'");
            $param[$this->privilege->FIELD_PRIVILEGE_DATEMODIFIED] = date("Y-m-d H:i:s");
            if(isset($data->{$this->privilege->FIELD_PRIMARY})){
                $this->updateExc($param);
            }else{
                $param[$this->privilege->FIELD_PRIVILEGE_DATECREATED] = date("Y-m-d H:i:s");
                $this->insertExc($param);
            }
        }else{
            $this->response = $filter;
        }
        
        echo json_encode($this->response);
    }

    public function updateExc($param){
        $id = $param['id'];

        unset($param['id']);
        $result = $this->privilege->update($param, $this->privilege->FIELD_PRIMARY."='".$id."'");

        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data menu hak akses berhasil diperbaharui';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function insertExc($param){

        unset($param['id']);
        $result = $this->privilege->insert($param);
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data jenis privilege berhasil diperbaharui';
        }else{
            $this->response['status'] = 500;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function deleteExc(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $result = $this->privilege->delete($this->privilege->FIELD_PRIMARY."='".$param['id']."'");
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data privilege berhasil dihapus';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menghapus data, silahkan ulangi kembali';
        }

        echo json_encode($this->response);
    }

    public function selectdata(){
        $response = array();
        $result = array();
        $raw = array();
        $raw['id'] = "";
        $raw['text'] = "Semua Menu Header";
        $result[] = $raw;
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $where = "";
        $order = $this->privilege->FIELD_PRIVILEGE_NAME." ASC";
        if(isset($param['term']) && !empty($param['term'])){
            $where .= "LOWER(".$this->privilege->FIELD_PRIVILEGE_NAME.") LIKE '%".$param['term']."%'";
            $data = $this->privilege->getList(0,0,$where, $order, "");
            foreach($data AS $key => $value){
                $raw = array();
                $raw['id'] = $value->{$this->privilege->FIELD_PRIMARY};
                $raw['text'] = $value->{$this->privilege->FIELD_PRIVILEGE_NAME};
                $result[] = $raw;
            }
        }
        
        $response['results'] = $result;
        echo json_encode($response);
    }
}