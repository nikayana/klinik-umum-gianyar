<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">
                    <b>Registrasi Pelayanan Pasien</b>
                </h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard") ?>"><i class='fa fa-home'></i> Beranda</a></li>
                        <li class="breadcrumb-item active">Data Pasien</li>
                    </ol>
                    <?php
                        if(isset($priv->access_add) && $priv->access_add == 1){
                            ?>
                                <a href="<?php echo site_url("operasional/pasien") ?>"  class="btn btn-success d-none d-lg-block m-l-15" data-modal="#editor"><i class='fa fa-file'></i> Data Pasien </a>
                                
                                <a href="<?php echo site_url("operasional/registrasi/form") ?>"  class="btn btn-success d-none d-lg-block m-l-15" data-modal="#editor"><i class='fa fa-plus-circle'></i> Registrasi Baru</a>
                            <?php
                        }
                    ?>
                    <button type="button" class="btn btn-info d-none d-lg-block m-l-15" id="searchformbtn"><i class='fa fa-search'></i> Pencarian</button>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        
        <!-- ============================================================== -->
        <!-- Sales Chart and browser state-->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-lg-12">
                <div class='card' id="searchform" style="display:none;">
                    <div class="card-body">
                        <h4 class="card-title">Form Pencarian</h4>
                        <form id="search" class="form-material">
                            <div class='row'>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="registrasi_norekam">Nomor Rekam Medis</label>
                                        <input type="text" name="registrasi_norekam" class="form-control form-control-line">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="pasien_name">Nama Pasien</label>
                                        <input type="text" name="pasien_name" class="form-control form-control-line">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="searchbtn"> &nbsp; </label>
                                    <button name="searchbtn" type="submit" class="btn btn-block btn-success" id="searchbtn"><i class='fa fa-search'></i> Cari</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Column -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"><?php echo isset($title) ? $title : "" ?></h4>
                        <h6 class="card-subtitle"> <?php echo isset($description) ? $description : "" ?> </h6>
                        <div id="datepaging"></div>
                        <div class="table-responsive">
                            <table id="tables" class="table display table-bordered table-striped no-wrap" data-url="<?php echo site_url("operasional/registrasi/datatables") ?>">
                                <thead>
                                    <tr>
                                        <th>No</th> 
                                        <th>No Rekam Medis</th>
                                        <th>Pasien</th>
                                        <th>Keluhan</th>
                                        <th>Jaminan</th>
                                        <th>Tgl Register</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        
        <!-- ============================================================== -->
        <!-- Comment - chats -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- End Comment - chats -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</div>