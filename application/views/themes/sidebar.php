
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="user-pro"> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><img src="../assets/images/users/1.jpg" alt="user-img" class="img-circle"><span class="hide-menu">Mark Jeckson</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="javascript:void(0)"><i class="ti-user"></i> My Profile</a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-power-off"></i> Logout</a></li>
                    </ul>
                </li>
                <li class="nav-small-cap">--- NAVIGATION</li>
                <?php  
                    if(isset($sb_menusheader)){
                        foreach($sb_menusheader AS $key => $value){
                            ?>
                                <li> 
                                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                                        <i class="<?php echo $value->header_icon ?>"></i> 
                                        <span class="hide-menu"><?php echo $value->header_name ?> </span>
                                    </a>
                                    <?php
                                        if(isset($sb_menus['header'.$value->header_id])){
                                            ?>
                                            <ul aria-expanded="false" class="collapse">
                                                <?php
                                                    foreach($sb_menus['header'.$value->header_id] AS $values){
                                                         
                                                        ?>
                                                            <li><a href="<?php echo site_url($values['link']) ?>"><?php echo $values['name'] ?></a></li>
                                                        <?php
                                                        
                                                    }
                                                ?>
                                            </ul>
                                        <?php
                                        }
                                    ?>
                                </li>
                            <?php
                        }
                    }
                ?>
            </ul>
        </nav>
    </div>
</aside>