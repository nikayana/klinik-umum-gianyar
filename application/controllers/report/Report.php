<?php
require_once 'application/libraries/vendor/autoload.php';
defined('BASEPATH') OR exit('No direct script access allowed');
class Report extends HC_Controller {
    private $response;
    private $modul;
    private $priv;

	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->response['status'] = 404;
        $this->response['messages'] = "Unknown Error, Please contact your administrator";
        $this->response['datas'] = array();
        $this->load->model("operasional/registrasimodel", "registrasi");
        $this->load->model("profile/pelayananmodel", "pelayanan");
        $this->load->model("profile/perawatanmodel", "perawatan");
        $this->load->model("master/doktermodel", "dokter");
        $this->load->model("operasional/pasienmodel", "pasien");
        $this->load->model("operasional/kunjunganmodel", "kunjungan");
        $this->load->model("operasional/kunjunganitemmodel", "kunjunganpelayanan");

        $this->modul = "report/report/reportkunjungan";
        $this->priv = $this->checkpriv($this->modul);

    }

    public function reportkunjungan(){
        $data = array();
        $data['title'] = "Laporan Kunjungan";
        $data['description'] = "Mengelola Laporan Kunjungan";
        $data['priv'] = $this->priv;

        if(isset($this->priv->{$this->access->FIELD_ACCESS_VIEW}) && $this->priv->{$this->access->FIELD_ACCESS_VIEW} == $this->access->ACCESS){
            $this->theme("admin/report/main-kunjungan-report", $data);
        }else{
            redirect("notfound");
        }
        
    }

    public function reporttopten(){
        $data = array();
        $data['title'] = "Laporan Tranding  Penyakit";
        $data['description'] = "Mengelola Laporan Tranding  Penyakit";
        $data['priv'] = $this->priv;

        if(isset($this->priv->{$this->access->FIELD_ACCESS_VIEW}) && $this->priv->{$this->access->FIELD_ACCESS_VIEW} == $this->access->ACCESS){
            $this->theme("admin/report/main-topten-report", $data);
        }else{
            redirect("notfound");
        }
        
    }

    public function displayreportkunjungan(){
		$data = array();
		$param = $this->input->post(NULL, TRUE);
		$param = $this->antiinjection->antiinject($param);
        $where = "1=1";
        
        if(isset($param['tgl_start']) && isset($param['tgl_end']) && !empty($param['tgl_start']) && !empty($param['tgl_end'])){
            $where .=" AND k.".$this->kunjungan->FIELD_KUNJUNGAN_DATE." BETWEEN '".$param['tgl_start']."' AND '".$param['tgl_end']."'";
        }
		//GET Data
        $data['data'] = $this->kunjungan->getListJoinReport(0,0,$where,"k.".$this->kunjungan->FIELD_KUNJUNGAN_DATE." DESC, k.".$this->kunjungan->FIELD_KUNJUNGAN_TIME." DESC","");
        $data['jk'] = $this->pasien->JK;
        $data['profile'] = $this->getcomprof();

		$this->response['html'] = $this->load->view("admin/report/display-kunjungan-report", $data, TRUE);
		$this->response['status'] = 200;
		$this->response['messages'] = '';
        $this->response['datas'] = array();

		echo json_encode($this->response);
    }

    public function printreportkunjungan(){
        $data = array();
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
		$param = $this->input->get(NULL, TRUE);
		$param = $this->antiinjection->antiinject($param);
        $where = "1=1";
        
        if(isset($param['tgl_start']) && isset($param['tgl_end']) && !empty($param['tgl_start']) && !empty($param['tgl_end'])){
            $where .=" AND k.".$this->kunjungan->FIELD_KUNJUNGAN_DATE." BETWEEN '".$param['tgl_start']."' AND '".$param['tgl_end']."'";
        }
        $data['data'] = $this->kunjungan->getListJoinReport(0,0,$where,"k.".$this->kunjungan->FIELD_KUNJUNGAN_DATE." DESC, k.".$this->kunjungan->FIELD_KUNJUNGAN_TIME." DESC","");
        $data['jk'] = $this->pasien->JK;
        $data['profile'] = $this->getcomprof();

        $mpdf->WriteHTML($this->load->view("admin/report/display-kunjungan-report", $data, TRUE));
        $mpdf->Output("Laporan Kunjungan Home Care.pdf", 'I');
    }

    public function displayreporttopten(){
		$data = array();
		$param = $this->input->post(NULL, TRUE);
		$param = $this->antiinjection->antiinject($param);
        $where = "1=1";
        
        if(isset($param['tgl_start']) && isset($param['tgl_end']) && !empty($param['tgl_start']) && !empty($param['tgl_end'])){
            $where .=" AND k.".$this->kunjungan->FIELD_KUNJUNGAN_DATE." BETWEEN '".$param['tgl_start']."' AND '".$param['tgl_end']."'";
        }
		//GET Data
        $data['data'] = $this->kunjungan->getListJoinReportAll($where);
        $data['jk'] = $this->pasien->JK;
        $data['profile'] = $this->getcomprof();

		$this->response['html'] = $this->load->view("admin/report/display-topten-report", $data, TRUE);
		$this->response['status'] = 200;
		$this->response['messages'] = '';
        $this->response['datas'] = array();

		echo json_encode($this->response);
    }

    public function printreporttopten(){
        $data = array();
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
		$param = $this->input->get(NULL, TRUE);
		$param = $this->antiinjection->antiinject($param);
        $where = "1=1";
        
        if(isset($param['tgl_start']) && isset($param['tgl_end']) && !empty($param['tgl_start']) && !empty($param['tgl_end'])){
            $where .=" AND k.".$this->kunjungan->FIELD_KUNJUNGAN_DATE." BETWEEN '".$param['tgl_start']."' AND '".$param['tgl_end']."'";
        }
		//GET Data
        $data['data'] = $this->kunjungan->getListJoinReportAll($where);
        $data['jk'] = $this->pasien->JK;
        $data['profile'] = $this->getcomprof();

        $mpdf->WriteHTML($this->load->view("admin/report/display-topten-report", $data, TRUE));
        $mpdf->Output("Laporan 10 Besar Perawatan.pdf", 'I');
    }
    
    public function save(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);

        $filter = $this->checkparam($this->agama->getrules(), $param);
        if(count($filter) == 0){
            $data = $this->agama->fetch($this->agama->FIELD_PRIMARY."='".$param['id']."'");
            $param[$this->agama->FIELD_AGAMA_DATEMODIFIED] = date("Y-m-d H:i:s");
            if(isset($data->{$this->agama->FIELD_PRIMARY})){
                $this->updateExc($param);
            }else{
                $param[$this->agama->FIELD_AGAMA_DATECREATED] = date("Y-m-d H:i:s");
                $this->insertExc($param);
            }
        }else{
            $this->response = $filter;
        }
        
        echo json_encode($this->response);
    }

    public function updateExc($param){
        $id = $param['id'];

        unset($param['id']);
        $result = $this->agama->update($param, $this->agama->FIELD_PRIMARY."='".$id."'");

        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data agama berhasil diperbaharui';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function insertExc($param){

        unset($param['id']);
        $result = $this->agama->insert($param);
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data agama berhasil diperbaharui';
        }else{
            $this->response['status'] = 500;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function deleteExc(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $result = $this->agama->delete($this->agama->FIELD_PRIMARY."='".$param['id']."'");
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data agama berhasil dihapus';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menghapus data, silahkan ulangi kembali';
        }

        echo json_encode($this->response);
    }

    public function selectdata(){
        $response = array();
        $result = array();
        $raw = array();
        $raw['id'] = "";
        $raw['text'] = "Semua Agama";
        $result[] = $raw;
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $where = "";
        $order = $this->agama->FIELD_AGAMA_NAME." ASC";
        if(isset($param['term']) && !empty($param['term'])){
            $where .= "LOWER(".$this->agama->FIELD_AGAMA_NAME.") LIKE '%".$param['term']."%'";
            $data = $this->agama->getList(0,0,$where, $order, "");
            foreach($data AS $key => $value){
                $raw = array();
                $raw['id'] = $value->{$this->agama->FIELD_PRIMARY};
                $raw['text'] = $value->{$this->agama->FIELD_AGAMA_NAME};
                $result[] = $raw;
            }
        }
        
        $response['results'] = $result;
        echo json_encode($response);
    }

}