$(document).ready(function(){
    $("form#login").submit(function(){
        $("#notif").hide();
        $.ajax({
            type    : "POST",
            url     : $(this).data("url"),
            data    : $(this).serialize(),
            cache   : false,
            dataType : 'json',
            beforeSend : function(){
                $("#loginbtn").attr("disabled", true);
            },
            success : function(response){
                if(response.status == 200){
                    window.location.href = response.datas.url;
                }else{
                    alertcustom(response.messages, 'danger');
                }
                $("#loginbtn").removeAttr("disabled");
            },
            error : function(xhr, err){
                alertcustom('Ups, Terjadi kesalahan '+formatErrorMessage(xhr, err), 'danger');
                $("#loginbtn").removeAttr("disabled");
            } 
        });

        return false;
    });

    $("form#signup").submit(function(){
        $("#notif").hide();
        let password = $("#password").val();
        let cfmpassword = $("#cfmpassword").val();

        if(password != cfmpassword){
            alertcustom("<i class='fa fa-exclamation-circle'></i> Konfirmasi password tidak sama dengan password anda", 'danger');
        }else{
            $.ajax({
                type    : "POST",
                url     : $(this).data("url"),
                data    : $(this).serialize(),
                dataType : 'json',
                beforeSend : function(){
                    $("#signupbtn").attr("disabled", true);
                },
                success : function(response){
                    console.log(response.status);
                    if(response.status == 200){
                        window.location.href = response.datas.redirect;
                    }else{
                        alertcustom(response.messages, 'danger');
                    }
                    $("#signupbtn").removeAttr("disabled");
                },
                error : function(xhr, err){
                    alertcustom(formatErrorMessage(xhr, err), 'danger');
                    $("#signupbtn").removeAttr("disabled");
                } 
            });
        }
        
        return false;
    });
})