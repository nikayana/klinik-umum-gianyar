<input type="hidden" name="id" value="<?php echo isset($data->menu_id) ? $data->menu_id : 0 ?>">
<input type="hidden" name="menu_modifiedby" value="<?php echo isset($profile->user_id) ? $profile->user_id : "" ?>">

<div class="form-row">
  <div class="col-md-12 mb-3">
      <label for="header_id">Menu Header <font color='red'>*</font></label>
      <select name="header_id" class="selects form-control custom-select" style="width:100%" required>
        <option value="">Pilih menu header</option>
        <?php 
            if(isset($header)){
                foreach($header AS $key => $value){
                    $selected = "";
                    if(isset($data->header_id) && $data->header_id == $value->header_id){
                        $selected = "selected='selected'";
                    }
                    ?>
                    <option value='<?php echo $value->header_id ?>' <?php echo $selected ?>><?php echo $value->header_name ?></option>
                    <?php
                }
            }
        
        ?>
      </select>
  </div>
  <div class="col-md-12 mb-3">
      <label for="menu_name">Nama Menu <font color='red'>*</font></label>
      <input type="text" name="menu_name" class="form-control" placeholder="Isikan nama menu" value="<?php echo isset($data->menu_name) ? $data->menu_name : ""  ?>" required>
  </div>
  <div class="col-md-12 mb-3">
      <label for="menu_link">Link <font color='red'>*</font></label>
      <input type="text" name="menu_link" class="form-control" placeholder="Isikan link menu" value="<?php echo isset($data->menu_link) ? $data->menu_link : ""  ?>" required>
  </div>
  <div class="col-md-12 mb-3">
      <label for="menu_status">Status <font color='red'>*</font></label>
      <select name="menu_status" class="selects form-control custom-select" style="width:100%">
        <?php 
            if(isset($status)){
                foreach($status AS $key => $value){
                    $selected = "";
                    if(isset($data->menu_status) && $data->menu_status == $key){
                        $selected = "selected='selected'";
                    }
                    ?>
                    <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                    <?php
                }
            }
        
        ?>
      </select>
  </div>
</div>
