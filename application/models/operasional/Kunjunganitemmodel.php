<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kunjunganitemmodel extends CI_Model{
    //TABLE PROPERTIES
    private $table = "homecare_kunjungan_pelayanan";
    private $table_pelayanan = "homecare_pelayanan";
    private $table_perawatan = "homecare_perawatan";
    private $table_kunjungan = "homecare_kunjungan";
    private $table_icd10 = "homecare_icd10";
    public $FIELD_PRIMARY = "kunjungan_pelayanan_id";
    public $FIELD_PERAWATAN_ID = "perawatan_id";
    public $FIELD_PELAYANAN_ID = "pelayanan_id";
    public $FIELD_PELAYANAN_LAINNYA= "pelayanan_lainnya";
    public $FIELD_PELAYANAN_HARGA = "pelayanan_harga";
    public $FIELD_KUNJUNGAN_ID = "kunjungan_id";
    public $FIELD_KUNJUNGAN_PELAYANAN_DATECREATED = "kunjungan_pelayanan_datecreated";
    public $FIELD_KUNJUNGAN_PELAYANAN_DATEMODIFIED = "kunjungan_pelayanan_datemodified";
    public $FIELD_KUNJUNGAN_PELAYANAN_MODIFIEDBY = "kunjungan_pelayanan_modifiedby";


    //RULES
    private $rules;

    function __construct(){
        parent::__construct();

        $this->rules  = array(
           
        );
    }

    public function getrules(){
        return $this->rules;
    }
    public function fetch($where = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    public function fetchJoin($where = ""){
        $result = array();
        
        $this->db->select("kp.*, p.pelayanan_name, pr.perawatan_name");
        $this->db->from($this->table." AS kp");
        $this->db->join($this->table_pelayanan." AS p", "kp.".$this->FIELD_PELAYANAN_ID."=p.pelayanan_id","LEFT");
        $this->db->join($this->table_perawatan." AS pr", "kp.".$this->FIELD_PERAWATAN_ID."=pr.perawatan_id","LEFT");
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    public function getAll(){
        $result = array();
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

    public function getList($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getListJoin($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        $this->db->select("kp.*, CASE WHEN kp.pelayanan_id='0' THEN 'Lainnya' ELSE p.pelayanan_name END AS pelayanan_name, CASE WHEN kp.perawatan_id='0' THEN 'Lainnya' ELSE pr.perawatan_name END AS perawatan_name");
        $this->db->from($this->table." AS kp");
        $this->db->join($this->table_pelayanan." AS p", "kp.".$this->FIELD_PELAYANAN_ID."=p.pelayanan_id","LEFT");
        $this->db->join($this->table_perawatan." AS pr", "kp.".$this->FIELD_PERAWATAN_ID."=pr.perawatan_id","LEFT");
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get();
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getListJoinReport($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        $this->db->select("COUNT(*) AS jumlah, 
            CASE WHEN kp.pelayanan_id='0' THEN 'Lainnya' ELSE p.pelayanan_name END AS pelayanan_name, 
            CASE WHEN kp.perawatan_id='0' THEN 'Lainnya' ELSE pr.perawatan_name END AS perawatan_name
        ");
        $this->db->from($this->table." AS kp");
        $this->db->join($this->table_kunjungan." AS k", "kp.".$this->FIELD_KUNJUNGAN_ID."=k.kunjungan_id","LEFT");
        $this->db->join($this->table_pelayanan." AS p", "kp.".$this->FIELD_PELAYANAN_ID."=p.pelayanan_id","LEFT");
        $this->db->join($this->table_perawatan." AS pr", "kp.".$this->FIELD_PERAWATAN_ID."=pr.perawatan_id","LEFT");
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get();
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getCount($where= ""){
        $result = 0;

        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->num_rows();

        return $result;
    }

    public function getCountJoin($where= ""){
        $result = 0;
        $this->db->from($this->table." AS kp");
        $this->db->join($this->table_pelayanan." AS p", "kp.".$this->FIELD_PELAYANAN_ID."=p.pelayanan_id","LEFT");
        $this->db->join($this->table_perawatan." AS pr", "kp.".$this->FIELD_PERAWATAN_ID."=pr.perawatan_id","LEFT");
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get();
        $result = $query->num_rows();

        return $result;
    }

    public function insert($param){
        $result = false;
        $this->db->set($param);

        if($this->db->insert($this->table)){
            $result = true;
        }

        return $result;
    }

    public function insertBatch($param){
        $result = false;
        if($this->db->insert_batch($this->table, $param)){
            $data = true;
        }

        return $result;
    }

    public function update($param, $where){
        $result = false;
        $this->db->set($param);
        $this->db->where($where);

        if($this->db->update($this->table)){
            $result = true;
        }

        return $result;
    }

    public function delete($where){
        $result = false;
        $this->db->where($where);
        if($this->db->delete($this->table)){
            $result = true;
        }
        return $result;
    }

    public function getListJoinReportAll($where = ""){
        $result = array();
        
        $this->db->select("b.icd10_kode, b.icd10_name, count(a.icd10) as total");
        $this->db->from($this->table_kunjungan." AS k");
        $this->db->join($this->table_icd10." AS i", "k.".$this->FIELD_ICD10."=i.icd10_id","INNER"); 
        
        if(!empty($where)){
            $this->db->where($where);
        }
  
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        var_dump($result); die;
        return $result;
    }

    
}
?>
