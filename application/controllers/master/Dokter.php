<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dokter extends HC_Controller {
    private $response;
    private $modul;
    private $priv;

	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->response['status'] = 404;
        $this->response['messages'] = "Unknown Error, Please contact your administrator";
        $this->response['datas'] = array();
        $this->load->model("master/doktermodel", "dokter");

        $this->modul = "master/dokter";
        $this->priv = $this->checkpriv($this->modul);

    }

    public function index(){
        $data = array();
        $data['title'] = "Dokter";
        $data['description'] = "Mengelola data dokter";
        $data['status'] = $this->dokter->STATUS;
        $data['priv'] = $this->priv;
        $data['jk'] = $this->dokter->JK;

        if(isset($this->priv->{$this->access->FIELD_ACCESS_VIEW}) && $this->priv->{$this->access->FIELD_ACCESS_VIEW} == $this->access->ACCESS){
            $this->theme("admin/dokter/main-dokter", $data);
        }else{
            redirect("notfound");
        }
        
    }

    public function datatables(){
        $result = array();
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $aColumns = array("dokter_id","dokter_name", "dokter_jk", "dokter_alamat", "dokter_phone", "dokter_status", "dokter_datemodified");

        $sLimit = 20;
        $sOffset = 0;
        $sOrder = "";
        $sWhere = "";
        $sGroup = "";
        $sEcho = 0;
        if(isset($param["sEcho"])){
            $sEcho = 0;
        }
        if(isset($param['start']) && $param['start'] > 0){
            $sOffset = $param['start'];
        }
        
        if(isset($param['order'])){
            if(empty($sOrder)){
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }else{
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }
        }

        $sWhere = "1=1";
        if(isset($param[$this->dokter->FIELD_DOKTER_NAME]) && !empty($param[$this->dokter->FIELD_DOKTER_NAME])){
            $sWhere .=" AND LOWER(".$this->dokter->FIELD_DOKTER_NAME.") LIKE '%".strtolower($param[$this->dokter->FIELD_DOKTER_NAME])."%'";
        }

        if(isset($param[$this->dokter->FIELD_DOKTER_STATUS]) && strlen($param[$this->dokter->FIELD_DOKTER_STATUS]) > 0){
            $sWhere .=" AND ".$this->dokter->FIELD_DOKTER_STATUS."=".$param[$this->dokter->FIELD_DOKTER_STATUS]."";
        }

        if(isset($param[$this->dokter->FIELD_DOKTER_JK]) && strlen($param[$this->dokter->FIELD_DOKTER_JK]) > 0){
            $sWhere .=" AND ".$this->dokter->FIELD_DOKTER_JK."=".$param[$this->dokter->FIELD_DOKTER_JK]."";
        }
        $searchWhere = "";
        if(!empty($param['search']['value'])){

            for($i=0;$i<count($aColumns);$i++){
                if(empty($searchWhere)){
                    $searchWhere .=$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }else{
                    $searchWhere .=" OR ".$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }

            }

            if(!empty($sWhere)){
                $sWhere .=" AND (".$searchWhere.")";
            }else{
                $sWhere .= "(".$searchWhere.")";
            }

        }
        $sTotal = $this->dokter->getCount($sWhere);
        $data = $this->dokter->getList($sOffset,$sLimit,$sWhere,$sOrder,$sGroup);
        $output = array(
            "sEcho" => intval($sEcho),
            "iTotalRecords" => $sTotal,
            "iTotalDisplayRecords" => $sTotal,
            "aaData" => array()
        );

        foreach ($data AS $key => $value){
            $row = array();
            $button = "";

            if(isset($this->priv->{$this->access->FIELD_ACCESS_UPDATE}) && $this->priv->{$this->access->FIELD_ACCESS_UPDATE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->dokter->FIELD_PRIMARY}."' data-url='". site_url("master/dokter/form")."' class='addedit btn btn-warning btn-xs m-t-10' data-toggle='tooltip' data-original-title='Edit' data-placement='top' style='margin-right:5px;' data-modal='#editor' data-title='Ubah Data Dokter'><i class='fas fa-edit'></i></button>";
            }
            if(isset($this->priv->{$this->access->FIELD_ACCESS_DELETE}) && $this->priv->{$this->access->FIELD_ACCESS_DELETE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->dokter->FIELD_PRIMARY}."' data-url='". site_url("master/dokter/deleteExc")."' class='delete btn btn-danger btn-xs m-t-10' data-toggle='tooltip' data-original-title='Hapus' data-placement='top' style='margin-right:5px;'><i class='fa fa-trash'></i></button>";
            }

            for($i = 0; $i < count($aColumns); $i++){
                    $reasstype = "";
                    if($aColumns[$i] == $this->dokter->FIELD_PRIMARY){
                            $row[] = $sOffset+$key+1;
                    }else if($aColumns[$i] == $this->dokter->FIELD_DOKTER_DATEMODIFIED){
                            $row[] = $button;
                    }else if($aColumns[$i] == $this->dokter->FIELD_DOKTER_STATUS){
                        $displaystatus = "danger";
                        if($value->{$aColumns[$i]} == $this->dokter->ACTIVE_STATUS){
                            $displaystatus = "success";
                        }
                        $row[] = "<div class='label label-table label-".$displaystatus."'>".$this->dokter->STATUS[$value->{$aColumns[$i]}]."</div>";
                    }else if($aColumns[$i] == $this->dokter->FIELD_DOKTER_JK){
                        $displaystatus = "danger";
                        if($value->{$aColumns[$i]} == $this->dokter->LAKI){
                            $displaystatus = "success";
                        }
                        $row[] = "<div class='label label-table label-".$displaystatus."'>".$this->dokter->JK[$value->{$aColumns[$i]}]."</div>";
                    }else{
                            $row[] = $value->{$aColumns[$i]};
                    }


            }

            $row[] = $button;
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function form(){
		$data = array();
		$param = $this->input->post(NULL, TRUE);
		$param = $this->antiinjection->antiinject($param);
		$where = "";
		//GET Data
        $data['data'] = $this->dokter->fetch($this->dokter->FIELD_PRIMARY."='".$param['id']."'");
        $data['status'] = $this->dokter->STATUS;
        $data['jk'] = $this->dokter->JK;
        $data['profile'] = $this->getprofile();

		$this->response['html'] = $this->load->view("admin/dokter/dokter-form", $data, TRUE);
		$this->response['status'] = 200;
		$this->response['messages'] = '';
        $this->response['datas'] = array();

		echo json_encode($this->response);
    }

    private function convertimage($param){
        $image_array_1 = explode(";", $param);
        $image_array_2 = explode(",", $image_array_1[1]);
        $data = base64_decode($image_array_2[1]);
        $path = "./assets/images/profile/";
        $imageName = "dokter_".date("YmdHis"). '.png';
        file_put_contents($path.$imageName, $data);

        return $imageName;
    }
    
    public function save(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $rawpict = $this->input->post($this->dokter->FIELD_DOKTER_PICT);

        $filter = $this->checkparam($this->dokter->getrules(), $param);
        if(count($filter) == 0){

            if(!empty($rawpict)){
                $param[$this->dokter->FIELD_DOKTER_PICT] = $this->convertimage($rawpict);
            }else{
                unset($param[$this->dokter->FIELD_DOKTER_PICT]);
            }

            $data = $this->dokter->fetch($this->dokter->FIELD_PRIMARY."='".$param['id']."'");
            $param[$this->dokter->FIELD_DOKTER_DATEMODIFIED] = date("Y-m-d H:i:s");
            if(isset($data->{$this->dokter->FIELD_PRIMARY})){
                $this->updateExc($param);
            }else{
                $param[$this->dokter->FIELD_DOKTER_DATECREATED] = date("Y-m-d H:i:s");
                $this->insertExc($param);
            }
        }else{
            $this->response = $filter;
        }
        
        echo json_encode($this->response);
    }

    public function updateExc($param){
        $id = $param['id'];
        if(file_exists("./assets/images/profile/".$param['dokter_oldphoto']) && !empty($param['dokter_oldphoto']) && !empty($param[$this->dokter->FIELD_DOKTER_PICT])){
            @unlink("./assets/images/profile/".$param['dokter_oldphoto']);
        }
        unset($param['dokter_oldphoto']);
        unset($param['id']);
        $result = $this->dokter->update($param, $this->dokter->FIELD_PRIMARY."='".$id."'");

        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data dokter berhasil diperbaharui';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function insertExc($param){
        unset($param['dokter_oldphoto']);
        unset($param['id']);
        $result = $this->dokter->insert($param);
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data dokter berhasil diperbaharui';
        }else{
            $this->response['status'] = 500;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function deleteExc(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);

        $data = $this->dokter->fetch($this->dokter->FIELD_PRIMARY."=".$param['id']);

        if(isset($data->{$this->dokter->FIELD_PRIMARY})){
            if(file_exists("./assets/images/profile/".$data->{$this->dokter->FIELD_DOKTER_PICT}) && !empty($data->{$this->dokter->FIELD_DOKTER_PICT})){
                @unlink("./assets/images/profile/".$data->{$this->dokter->FIELD_DOKTER_PICT});
            }
        }
        
        
        $result = $this->dokter->delete($this->dokter->FIELD_PRIMARY."='".$param['id']."'");
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data dokter berhasil dihapus';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menghapus data, silahkan ulangi kembali';
        }

        echo json_encode($this->response);
    }

    public function selectdata(){
        $response = array();
        $result = array();
        $raw = array();
        $raw['id'] = "";
        $raw['text'] = "Semua Dokter";
        $result[] = $raw;
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $where = "";
        $order = $this->dokter->FIELD_DOKTER_NAME." ASC";
        if(isset($param['term']) && !empty($param['term'])){
            $where .= "LOWER(".$this->dokter->FIELD_DOKTER_NAME.") LIKE '%".$param['term']."%' AND ".$this->dokter->FIELD_DOKTER_STATUS."=".$this->dokter->ACTIVE_STATUS;
            $data = $this->dokter->getList(0,0,$where, $order, "");
            foreach($data AS $key => $value){
                $raw = array();
                $raw['id'] = $value->{$this->dokter->FIELD_PRIMARY};
                $raw['text'] = $value->{$this->dokter->FIELD_DOKTER_NAME};
                $result[] = $raw;
            }
        }
        
        $response['results'] = $result;
        echo json_encode($response);
    }

}