"use strict";
function toastcustom(headtext, message, icon){
  $.toast({
  	heading: headtext,
    text: message,
    position: 'top-right',
    loaderBg:'#ff6849',
    icon: icon,
    hideAfter: 4000, 
    stack: 1
  });
}


function alertcustom(message, type){
  $("#notif").removeClass("alert-danger")
  $("#notif").html(message).addClass("alert-"+type).fadeIn("medium");
}

function formatErrorMessage(jqXHR, exception) {

    if (jqXHR.status === 0) {
        return ('Not connected.\nPlease verify your network connection.');
    } else if (jqXHR.status == 404) {
        return ('The requested page not found. [404]');
    } else if (jqXHR.status == 500) {
        return ('Internal Server Error [500].');
    } else if (exception === 'parsererror') {
        return ('Requested JSON parse failed.');
    } else if (exception === 'timeout') {
        return ('Time out error.');
    } else if (exception === 'abort') {
        return ('Ajax request aborted.');
    } else {
        return ('Uncaught Error.\n' + jqXHR.responseText);
    }
}
