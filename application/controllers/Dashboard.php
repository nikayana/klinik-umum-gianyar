<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends HC_Controller {
    private $response;
    private $modul;
    private $priv;
	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->response['status'] = 200;
        $this->response['messages'] = "";
        $this->response['datas'] = array();
        $this->modul = "dashboard";
        $this->priv = $this->checkpriv($this->modul);


    }

    public function index(){

        if(isset($this->priv->{$this->access->FIELD_ACCESS_VIEW}) && $this->priv->{$this->access->FIELD_ACCESS_VIEW} == $this->access->ACCESS){
			$data = array();
            $this->theme("admin/dashboard/main-dashboard", $data);
        }else{
            redirect("notfound");
        }
        
    }

    
}