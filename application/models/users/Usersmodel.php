<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usersmodel extends CI_Model{
    //TABLE PROPERTIES
    private $table = "homecare_users";
    private $table_privilege = "homecare_privilege";
    public $FIELD_PRIMARY = "user_id";
    public $FIELD_USER_NAME = "user_name";
    public $FIELD_USER_ADDRESS = "user_address";
    public $FIELD_USER_PHONE = "user_phone";
    public $FIELD_USER_EMAIL = "user_email";
    public $FIELD_PRIVILEGE_ID = "privilege_id";
    public $FIELD_USER_USERNAME = "user_username";
    public $FIELD_USER_PASSWORD = "user_password";
    public $FIELD_USER_STATUS = "user_status";
    public $FIELD_USER_DATECREATED = "user_datecreated";
    public $FIELD_USER_DATEMODIFIED = "user_datemodified";
    public $FIELD_USER_MODIFIEDBY = "user_modifiedby";
    public $FIELD_DOKTER_ID = "dokter_id";

    public $ACTIVE_STATUS = 1;
    public $NONACTIVE_STATUS = 0;
    public $STATUS = array(
        "Blokir",
        "Aktif"
    );

    //RULES
    private $rules;
    private $loginrules;
    private $signuprules;

    function __construct(){
        parent::__construct();
        $this->rules = array(
            array(
                "name" => "Nama Pengguna",
                "field" => $this->FIELD_USER_NAME,
                "required" => true,
            ),
            array(
                "name" => "Alamat Pengguna",
                "field" => $this->FIELD_USER_ADDRESS,
                "required" => false,
            ),
            array(
                "name" => "No Telepon Pengguna",
                "field" => $this->FIELD_USER_PHONE,
                "required" => false,
            ),
            array(
                "name" => "Email Pengguna",
                "field" => $this->FIELD_USER_EMAIL,
                "required" => true,
            ),
            array(
                "name" => "Hak Akses Pengguna",
                "field" => $this->FIELD_PRIVILEGE_ID,
                "required" => true,
            ),
            array(
                "name" => "Username Pengguna",
                "field" => $this->FIELD_USER_USERNAME,
                "required" => true,
            ),
            array(
                "name" => "Password Pengguna",
                "field" => $this->FIELD_USER_PASSWORD,
                "required" => false,
            )
        );

        $this->loginrules = array(
            array(
                "name" => "Username",
                "field" => $this->FIELD_USER_USERNAME,
                "required" => true,
            ),
            array(
                "name" => "Password",
                "field" => $this->FIELD_USER_PASSWORD,
                "required" => true,
            )
        );

        $this->signuprules = array(
            array(
                "name" => "Nama",
                "field" => $this->FIELD_USER_NAME,
                "required" => true,
            ),
            array(
                "name" => "Email",
                "field" => $this->FIELD_USER_EMAIL,
                "required" => true,
            ),
            array(
                "name" => "Username",
                "field" => $this->FIELD_USER_PASSWORD,
                "required" => true,
            ),
            array(
                "name" => "Password",
                "field" => $this->FIELD_USER_PASSWORD,
                "required" => true,
            )
        );
    }

    public function getrules(){
        return $this->rules;
    }

    public function getloginrules(){
        return $this->loginrules;
    }

    public function getsignuprules(){
        return $this->signuprules;
    }

    public function fetch($where = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    public function getAll(){
        $result = array();
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

    public function getList($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getListJoin($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();

        $this->db->select("p.privilege_name, u.*");
        $this->db->from($this->table." AS u");
        $this->db->join($this->table_privilege." AS p", "u.".$this->FIELD_PRIVILEGE_ID."=p.privilege_id", "LEFT");
        
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get();
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getCount($where= ""){
        $result = 0;

        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($goup)){
            $this->db->group_by($group);
        }

        $query = $this->db->get($this->table);
        $result = $query->num_rows();

        return $result;
    }

    public function getCountJoin($where= ""){
        $result = 0;

        $this->db->from($this->table." AS u");
        $this->db->join($this->table_privilege." AS p", "u.".$this->FIELD_PRIVILEGE_ID."=p.privilege_id", "LEFT");
        
        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($goup)){
            $this->db->group_by($group);
        }

        $query = $this->db->get();
        $result = $query->num_rows();

        return $result;
    }

    public function insert($param){
        $result = false;
        $this->db->set($param);

        if($this->db->insert($this->table)){
            $result = true;
        }

        return $result;
    }

    public function insertBatch($param){
        $result = false;
        if($this->db->insert_batch($this->table, $param)){
            $data = true;
        }

        return $result;
    }

    public function update($param, $where){
        $result = false;
        $this->db->set($param);
        $this->db->where($where);

        if($this->db->update($this->table)){
            $result = true;
        }

        return $result;
    }

    public function delete($where){
        $result = false;
        $this->db->where($where);
        if($this->db->delete($this->table)){
            $result = true;
        }
        return $result;
    }
    
}
?>
