<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profilemodel extends CI_Model{
    //TABLE PROPERTIES
    private $table = "homecare_profile";
    public $FIELD_PRIMARY = "profile_id";
    public $FIELD_PROFILE_NAME = "profile_name";
    public $FIELD_PROFILE_EMAIL = "profile_email";
    public $FIELD_PROFILE_PHONE = "profile_phone";
    public $FIELD_PROFILE_FAX = "profile_fax";
    public $FIELD_PROFILE_ADDRESS = "profile_address";
    public $FIELD_PROFILE_VISI = "profile_visi";
    public $FIELD_PROFILE_MISI = "profile_misi";
    public $FIELD_PROFILE_DESCRIPTION = "profile_description";
    public $FIELD_PROFILE_DATECREATED = "profile_datecreated";
    public $FIELD_PROFILE_DATEMODIFIED = "profile_datemodified";
    public $FIELD_PROFILE_MODIFIEDBY = "profile_modifiedby";
    public $FIELD_PROFILE_LOGO = "profile_logo";

    //RULES
    private $rules;

    function __construct(){
        parent::__construct();

        $this->rules  = array(
            array(
                "name" => "nama rumah sakit",
                "field" =>$this->FIELD_PROFILE_NAME,
                "required" => true,
            ),
            array(
                "name" => "alamat email rumah sakit",
                "field" => $this->FIELD_PROFILE_EMAIL,
                "required" => true,
            ),
            array(
                "name" => "nomor telepon rumah sakit",
                "field" => $this->FIELD_PROFILE_PHONE,
                "required" => true,
            ),
            array(
                "name" => "alamat rumah sakit",
                "field" => $this->FIELD_PROFILE_ADDRESS,
                "required" => true,
            ),
            array(
                "name" => "visi rumah sakit",
                "field" => $this->FIELD_PROFILE_VISI,
                "required" => true,
            ),
            array(
                "name" => "misi rumah sakit",
                "field" => $this->FIELD_PROFILE_MISI,
                "required" => true,
            )
        );
    }

    public function getrules(){
        return $this->rules;
    }

    public function fetch($where = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    public function getAll(){
        $result = array();
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

    public function getList($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }


    public function getCount($where= ""){
        $result = 0;

        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($goup)){
            $this->db->group_by($group);
        }

        $query = $this->db->get($this->table);
        $result = $query->num_rows();

        return $result;
    }


    public function insert($param){
        $result = false;
        $this->db->set($param);

        if($this->db->insert($this->table)){
            $result = true;
        }

        return $result;
    }

    public function insertBatch($param){
        $result = false;
        if($this->db->insert_batch($this->table, $param)){
            $data = true;
        }

        return $result;
    }

    public function update($param, $where){
        $result = false;
        $this->db->set($param);
        $this->db->where($where);

        if($this->db->update($this->table)){
            $result = true;
        }

        return $result;
    }

    public function delete($where){
        $result = false;
        $this->db->where($where);
        if($this->db->delete($this->table)){
            $result = true;
        }
        return $result;
    }
    
}
?>
