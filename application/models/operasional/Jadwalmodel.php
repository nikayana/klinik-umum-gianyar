<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwalmodel extends CI_Model{
    //TABLE PROPERTIES
    private $table = "homecare_jadwal";
    private $table_dokter = "homecare_dokter";
    public $FIELD_PRIMARY = "jadwal_id";
    public $FIELD_JADWAL_HARI = "jadwal_hari";
    public $FIELD_JADWAL_START = "jadwal_start";
    public $FIELD_JADWAL_END = "jadwal_end";
    public $FIELD_DOKTER_ID = "dokter_id";
    public $FIELD_JADWAL_DATECREATED = "jadwal_datecreated";
    public $FIELD_JADWAL_DATEMODIFIED = "jadwal_datemodified";
    public $FIELD_JADWAL_MODIFIEDBY = "jadwal_modifiedby";

    public $SENIN = 1;
    public $SELASA = 2;
    public $RABU = 3;
    public $KAMIS = 4;
    public $JUMAT = 5;
    public $SABTU = 6;
    public $MINGGU = 7;
    public $HARI = array(
        "",
        "Senin",
        "Selasa",
        "Rabu",
        "Kamis",
        "Jumat",
        "Sabtu",
        "Minggu"
    );

    //RULES
    private $rules;

    function __construct(){
        parent::__construct();

        $this->rules  = array(
            array(
                "name" => "hari",
                "field" =>$this->FIELD_JADWAL_HARI,
                "required" => true,
            ),
            array(
                "name" => "waktu mulai",
                "field" => $this->FIELD_JADWAL_START,
                "required" => true,
            ),
            array(
                "name" => "waktu selesai",
                "field" => $this->FIELD_JADWAL_END,
                "required" => true,
            ),
            array(
                "name" => "dokter",
                "field" => $this->FIELD_DOKTER_ID,
                "required" => true,
            )
        );
    }

    public function getrules(){
        return $this->rules;
    }
    public function fetch($where = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    public function getAll(){
        $result = array();
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

    public function getList($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getListJoin($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        $this->db->select("d.dokter_name,j.*");
        $this->db->from($this->table." AS j");
        $this->db->join($this->table_dokter." AS d", "j.".$this->FIELD_DOKTER_ID."=d.dokter_id", "LEFT");
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get();
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getCountJoin($where= ""){
        $result = 0;
        $this->db->from($this->table." AS j");
        $this->db->join($this->table_dokter." AS d", "j.".$this->FIELD_DOKTER_ID."=d.dokter_id", "LEFT");
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get();
        $result = $query->num_rows();

        return $result;
    }

    public function getCount($where= ""){
        $result = 0;

        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->num_rows();

        return $result;
    }

    public function insert($param){
        $result = false;
        $this->db->set($param);

        if($this->db->insert($this->table)){
            $result = true;
        }

        return $result;
    }

    public function insertBatch($param){
        $result = false;
        if($this->db->insert_batch($this->table, $param)){
            $data = true;
        }

        return $result;
    }

    public function update($param, $where){
        $result = false;
        $this->db->set($param);
        $this->db->where($where);

        if($this->db->update($this->table)){
            $result = true;
        }

        return $result;
    }

    public function delete($where){
        $result = false;
        $this->db->where($where);
        if($this->db->delete($this->table)){
            $result = true;
        }
        return $result;
    }
    
}
?>
