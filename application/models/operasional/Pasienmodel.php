<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasienmodel extends CI_Model{
    //TABLE PROPERTIES
    private $table = "homecare_pasien";
    private $table_agama = "homecare_agama";
    private $table_pekerjaan = "homecare_pekerjaan";
    public $FIELD_PRIMARY = "pasien_id";
    public $FIELD_PASIEN_NAME = "pasien_name";
    public $FIELD_PASIEN_TGLLAHIR = "pasien_tgllahir";
    public $FIELD_PASIEN_JK = "pasien_jk";
    public $FIELD_PASIEN_PEKERJAAN = "pasien_pekerjaan";
    public $FIELD_PASIEN_AGAMA = "pasien_agama";
    public $FIELD_PASIEN_NOREKAM = "pasien_norekam";
    public $FIELD_PASIEN_NOJKN = "pasien_nojkn";
    public $FIELD_PASIEN_BERAT = "pasien_berat";
    public $FIELD_PASIEN_TINGGI = "pasien_tinggi";
    public $FIELD_PASIEN_PHONE = "pasien_phone";
    public $FIELD_PASIEN_DATECREATED = "pasien_datecreated";
    public $FIELD_PASIEN_DATEMODIFIED = "pasien_datemodified";
    public $FIELD_PASIEN_MODIFIEDBY = "pasien_modifiedby";
    public $FIELD_PASIEN_ALAMAT = "pasien_alamat";
    public $FIELD_PASIEN_EMAIL = "pasien_email";
    public $FIELD_PASIEN_TEMPATLAHIR = "pasien_tempatlahir";
    public $FIELD_PASIEN_KODE_POS = "pasien_kodepos";
    public $FIELD_PASIEN_WILAYAH = "pasien_wilayah";
    public $FIELD_PASIEN_HP = "pasien_hp";
    public $FIELD_PASIEN_GOL = "pasien_gol";


    public $LAKI = 1;
    public $PEREMPUAN = 0;
    public $JK = array(
        "Perempuan",
        "Laki - laki"
    );

    public $GOL_O = 0;
    public $GOL_A = 1;
    public $GOL_B = 2;
    public $GOL_AB = 3;
    public $GOL = array(
        "O",
        "A",
        "B",
        "AB"
    );

    public $STATUS_MENIKAH = 1;
    public $STATUS_BELUM_MENIKAH = 0;
    public $STATUS = array (
        "Belum Menikah",
        "Menikah"
    );

    //RULES
    private $rules;

    function __construct(){
        parent::__construct();

        $this->rules  = array(
            array(
                "name" => "nama pasien",
                "field" =>$this->FIELD_PASIEN_NAME,
                "required" => true,
            ),
            array(
                "name" => "alamat pasien",
                "field" => $this->FIELD_PASIEN_ALAMAT,
                "required" => true,
            ),
            array(
                "name" => "tanggal lahir pasien",
                "field" => $this->FIELD_PASIEN_TGLLAHIR,
                "required" => true,
            ),
            array(
                "name" => "jenis kelamin",
                "field" => $this->FIELD_PASIEN_JK,
                "required" => false,
            ),
            array(
                "name" => "pekerjaan pasien",
                "field" => $this->FIELD_PASIEN_PEKERJAAN,
                "required" => true,
            ),
            array(
                "name" => "agama pasien",
                "field" => $this->FIELD_PASIEN_AGAMA,
                "required" => true,
            ),
            array(
                "name" => "no rekam medis pasien",
                "field" => $this->FIELD_PASIEN_NOREKAM,
                "required" => true,
            )
        );
    }

    public function getrules(){
        return $this->rules;
    }
    public function fetch($where = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    public function getAll(){
        $result = array();
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

    public function getList($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getListJoin($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        $this->db->select("p.*, pk.pekerjaan_name, a.agama_name");
        $this->db->from($this->table." AS p");
        $this->db->join($this->table_pekerjaan." AS pk", "p.".$this->FIELD_PASIEN_PEKERJAAN."=pk.pekerjaan_id","LEFT");
        $this->db->join($this->table_agama." AS a", "p.".$this->FIELD_PASIEN_AGAMA."=a.agama_id","LEFT");
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get();
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getCount($where= ""){
        $result = 0;

        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->num_rows();

        return $result;
    }

    public function getCountJoin($where= ""){
        $result = 0;
        $this->db->from($this->table." AS p");
        $this->db->join($this->table_pekerjaan." AS pk", "p.".$this->FIELD_PASIEN_PEKERJAAN."=pk.pekerjaan_id","LEFT");
        $this->db->join($this->table_agama." AS a", "p.".$this->FIELD_PASIEN_AGAMA."=a.agama_id","LEFT");
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->num_rows();

        return $result;
    }

    public function insert($param){
        $result = false;
        $this->db->set($param);

        if($this->db->insert($this->table)){
            $result = true;
        }

        return $result;
    }

    public function insertBatch($param){
        $result = false;
        if($this->db->insert_batch($this->table, $param)){
            $data = true;
        }

        return $result;
    }

    public function update($param, $where){
        $result = false;
        $this->db->set($param);
        $this->db->where($where);

        if($this->db->update($this->table)){
            $result = true;
        }

        return $result;
    }

    public function delete($where){
        $result = false;
        $this->db->where($where);
        if($this->db->delete($this->table)){
            $result = true;
        }
        return $result;
    }
    
}
?>
