<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Icd10 extends HC_Controller {
    private $response;
    private $modul;
    private $priv;

	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->response['status'] = 404;
        $this->response['messages'] = "Unknown Error, Please contact your administrator";
        $this->response['datas'] = array();
        $this->load->model("master/Icd10model", "icd10");

        $this->modul = "master/icd10";
        $this->priv = $this->checkpriv($this->modul);

    }

    public function index(){
        $data = array();
        $data['title'] = "ICD10";
        $data['description'] = "Mengelola data ICD10";
        $data['status'] = $this->icd10->STATUS;
        $data['priv'] = $this->priv; 

        if(isset($this->priv->{$this->access->FIELD_ACCESS_VIEW}) && $this->priv->{$this->access->FIELD_ACCESS_VIEW} == $this->access->ACCESS){
            $this->theme("admin/icd10/main-icd10", $data);
        }else{
            redirect("notfound");
        }
        
    }

    public function datatables(){
        $result = array();
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $aColumns = array("icd10_id","icd10_kode","icd10_name", "icd10_status", "icd10_datemodified");

        $sLimit = 20;
        $sOffset = 0;
        $sOrder = "";
        $sWhere = "";
        $sGroup = "";
        $sEcho = 0;
        if(isset($param["sEcho"])){
            $sEcho = 0;
        }
        if(isset($param['start']) && $param['start'] > 0){
            $sOffset = $param['start'];
        }
        
        if(isset($param['order'])){
            if(empty($sOrder)){
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }else{
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }
        }

        $sWhere = "1=1";
        if(isset($param[$this->icd10->FIELD_ICD10_NAME]) && !empty($param[$this->icd10->FIELD_ICD10_NAME])){
            $sWhere .=" AND LOWER(".$this->icd10->FIELD_ICD10_NAME.") LIKE '%".strtolower($param[$this->icd10->FIELD_ICD10_NAME])."%'";
        }
        if(isset($param[$this->icd10->FIELD_ICD10_KODE]) && strlen($param[$this->icd10->FIELD_ICD10_KODE]) > 0){
            $sWhere .=" AND ".$this->icd10->FIELD_ICD10_KODE."=".$param[$this->icd10->FIELD_ICD10_KODE]."";
        }
        if(isset($param[$this->icd10->FIELD_ICD10_STATUS]) && strlen($param[$this->icd10->FIELD_ICD10_STATUS]) > 0){
            $sWhere .=" AND ".$this->icd10->FIELD_ICD10_STATUS."=".$param[$this->icd10->FIELD_ICD10_STATUS]."";
        }
 
        $searchWhere = "";
        if(!empty($param['search']['value'])){

            for($i=0;$i<count($aColumns);$i++){
                if(empty($searchWhere)){
                    $searchWhere .=$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }else{
                    $searchWhere .=" OR ".$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }

            }

            if(!empty($sWhere)){
                $sWhere .=" AND (".$searchWhere.")";
            }else{
                $sWhere .= "(".$searchWhere.")";
            }

        }
        $sTotal = $this->icd10->getCount($sWhere);
        $data = $this->icd10->getList($sOffset,$sLimit,$sWhere,$sOrder,$sGroup);
        $output = array(
            "sEcho" => intval($sEcho),
            "iTotalRecords" => $sTotal,
            "iTotalDisplayRecords" => $sTotal,
            "aaData" => array()
        );

        foreach ($data AS $key => $value){
            $row = array();
            $button = "";

            if(isset($this->priv->{$this->access->FIELD_ACCESS_UPDATE}) && $this->priv->{$this->access->FIELD_ACCESS_UPDATE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->icd10->FIELD_PRIMARY}."' data-url='". site_url("master/icd10/form")."' class='addedit btn btn-warning btn-xs m-t-10' data-toggle='tooltip' data-original-title='Edit' data-placement='top' style='margin-right:5px;' data-modal='#editor' data-title='Ubah Data icd10'><i class='fas fa-edit'></i></button>";
            }
            if(isset($this->priv->{$this->access->FIELD_ACCESS_DELETE}) && $this->priv->{$this->access->FIELD_ACCESS_DELETE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->icd10->FIELD_PRIMARY}."' data-url='". site_url("master/icd10/deleteExc")."' class='delete btn btn-danger btn-xs m-t-10' data-toggle='tooltip' data-original-title='Hapus' data-placement='top' style='margin-right:5px;'><i class='fa fa-trash'></i></button>";
            }

            for($i = 0; $i < count($aColumns); $i++){
                    $reasstype = "";
                    if($aColumns[$i] == $this->icd10->FIELD_PRIMARY){
                            $row[] = $sOffset+$key+1;
                    }else if($aColumns[$i] == $this->icd10->FIELD_ICD10_DATEMODIFIED){
                            $row[] = $button;
                    }else if($aColumns[$i] == $this->icd10->FIELD_ICD10_STATUS){
                        $displaystatus = "danger";
                        if($value->{$aColumns[$i]} == $this->icd10->ACTIVE_STATUS){
                            $displaystatus = "success";
                        }
                        $row[] = "<div class='label label-table label-".$displaystatus."'>".$this->icd10->STATUS[$value->{$aColumns[$i]}]."</div>";
                    }else{
                            $row[] = $value->{$aColumns[$i]};
                    }


            }

            $row[] = $button;
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function form(){
		$data = array();
		$param = $this->input->post(NULL, TRUE);
		$param = $this->antiinjection->antiinject($param);
		$where = "";
		//GET Data
        $data['data'] = $this->icd10->fetch($this->icd10->FIELD_PRIMARY."='".$param['id']."'");
        $data['status'] = $this->icd10->STATUS; 
        $data['kode'] = $this->icd10->FIELD_ICD10_KODE; 
        $data['profile'] = $this->getprofile();

		$this->response['html'] = $this->load->view("admin/icd10/icd10-form", $data, TRUE);
		$this->response['status'] = 200;
		$this->response['messages'] = '';
        $this->response['datas'] = array();

		echo json_encode($this->response);
    }

    private function convertimage($param){
        $image_array_1 = explode(";", $param);
        $image_array_2 = explode(",", $image_array_1[1]);
        $data = base64_decode($image_array_2[1]);
        $path = "./assets/images/profile/";
        $imageName = "icd10_".date("YmdHis"). '.png';
        file_put_contents($path.$imageName, $data);

        return $imageName;
    }
    
    public function save(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        //$rawpict = $this->input->post($this->icd10->FIELD_ICD10_PICT);

        $filter = $this->checkparam($this->icd10->getrules(), $param);
        if(count($filter) == 0){
            /* 
            if(!empty($rawpict)){
                $param[$this->icd10->FIELD_ICD10_PICT] = $this->convertimage($rawpict);
            }else{
                unset($param[$this->icd10->FIELD_ICD10_PICT]);
            } */

            $data = $this->icd10->fetch($this->icd10->FIELD_PRIMARY."='".$param['id']."'");
            $param[$this->icd10->FIELD_ICD10_DATEMODIFIED] = date("Y-m-d H:i:s");
            if(isset($data->{$this->icd10->FIELD_PRIMARY})){
                $this->updateExc($param);
            }else{
                $param[$this->icd10->FIELD_ICD10_DATECREATED] = date("Y-m-d H:i:s");
                $this->insertExc($param);
            }
        }else{
            $this->response = $filter;
        }
        
        echo json_encode($this->response);
    }

    public function updateExc($param){
        $id = $param['id'];
        /* if(file_exists("./assets/images/profile/".$param['icd10_oldphoto']) && !empty($param['icd10_oldphoto']) && !empty($param[$this->icd10->FIELD_ICD10_PICT])){
            @unlink("./assets/images/profile/".$param['icd10_oldphoto']);
        }
        unset($param['icd10_oldphoto']); */
        unset($param['id']);
        $result = $this->icd10->update($param, $this->icd10->FIELD_PRIMARY."='".$id."'");

        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data icd10 berhasil diperbaharui';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function insertExc($param){
        unset($param['icd10_oldphoto']);
        unset($param['id']);
        $result = $this->icd10->insert($param);
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data icd10 berhasil diperbaharui';
        }else{
            $this->response['status'] = 500;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function deleteExc(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);

        $data = $this->icd10->fetch($this->icd10->FIELD_PRIMARY."=".$param['id']);

        if(isset($data->{$this->icd10->FIELD_PRIMARY})){
            if(file_exists("./assets/images/profile/".$data->{$this->icd10->FIELD_ICD10_PICT}) && !empty($data->{$this->icd10->FIELD_ICD10_PICT})){
                @unlink("./assets/images/profile/".$data->{$this->icd10->FIELD_ICD10_PICT});
            }
        }
        
        
        $result = $this->icd10->delete($this->icd10->FIELD_PRIMARY."='".$param['id']."'");
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data icd10 berhasil dihapus';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menghapus data, silahkan ulangi kembali';
        }

        echo json_encode($this->response);
    }

    public function selectdata(){
        $response = array();
        $result = array();
        $raw = array();
        $raw['id'] = "";
        $raw['text'] = "Semua icd10";
        $result[] = $raw;
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $where = "";
        $order = $this->icd10->FIELD_ICD10_KODE." ASC";
        if(isset($param['term']) && !empty($param['term'])){
            $where .= "LOWER(".$this->icd10->FIELD_ICD10_NAME.") LIKE '%".$param['term']."%' AND ".$this->icd10->FIELD_ICD10_STATUS."=".$this->icd10->ACTIVE_STATUS;
            $data = $this->icd10->getList(0,0,$where, $order, "");
            foreach($data AS $key => $value){
                $raw = array();
                $raw['id'] = $value->{$this->icd10->FIELD_PRIMARY};
                $raw['text'] = $value->{$this->icd10->FIELD_ICD10_NAME};
                $result[] = $raw;
            }
        }
        
        $response['results'] = $result;
        echo json_encode($response);
    }

}