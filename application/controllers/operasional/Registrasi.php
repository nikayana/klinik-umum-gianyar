<?php
require_once 'application/libraries/vendor/autoload.php';
defined('BASEPATH') OR exit('No direct script access allowed');
class Registrasi extends HC_Controller {
    private $response;
    private $modul;
    private $priv;

	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->response['status'] = 404;
        $this->response['messages'] = "Unknown Error, Please contact your administrator";
        $this->response['datas'] = array();
        $this->load->model("operasional/registrasimodel", "registrasi");
        $this->load->model("profile/pelayananmodel", "pelayanan");
        $this->load->model("master/doktermodel", "dokter");
        $this->load->model("operasional/pasienmodel", "pasien");
        $this->load->model("master/agamamodel", "agama");
        $this->load->model("master/pekerjaanmodel", "pekerjaan");

        $this->modul = "operasional/registrasi";
        $this->priv = $this->checkpriv($this->modul);

    }

    public function index(){
        $data = array();
        $data['title'] = "Registrasi";
        $data['description'] = "Registrasi Pelayanan";
        $data['status'] = $this->registrasi->STATUS;
        $data['priv'] = $this->priv;
        $data['noreg'] = "HOMECARE-".date("m")."-".date("YmdHis");
        $data['profile'] = $this->getprofile();
        $data['dokter'] = $this->dokter->getList(0,0,$this->dokter->FIELD_DOKTER_STATUS."=".$this->dokter->ACTIVE_STATUS,"","");
        $data['pelayanan'] = $this->pelayanan->getList(0,0,$this->pelayanan->FIELD_PELAYANAN_STATUS."=".$this->pelayanan->ACTIVE_STATUS,"","");
        
        if(isset($this->priv->{$this->access->FIELD_ACCESS_VIEW}) && $this->priv->{$this->access->FIELD_ACCESS_VIEW} == $this->access->ACCESS){
            $this->theme("admin/registrasi/main-registrasi", $data);
        }else{
            redirect("notfound");
        }
        
    }

    public function print($id){

        $mpdf = new \Mpdf\Mpdf();
        $data = array();
        $data['comprof'] = $this->getcomprof();
        $data['data'] = $this->registrasi->fetchJoin("r.".$this->registrasi->FIELD_PRIMARY."=".$id);
         
        $data['jk'] = $this->pasien->JK;
        $data['status'] = $this->pasien->STATUS;
        $data['jaminan'] = $this->registrasi->BIAYA[$data['data']->registrasi_jaminan];
        $data['gol'] = $this->pasien->GOL;
    
        $mpdf->WriteHTML($this->load->view("admin/registrasi/print-registrasi", $data, TRUE));
        $mpdf->Output("Form Registrasi.pdf", 'I');

    }

    public function datatables(){
        $result = array(); 
        $param = $this->input->get(NULL, TRUE); 
        $param = $this->antiinjection->antiinject($param);
        $param[$this->registrasi->FIELD_REGISTRASI_DATE] = date("Y-m-d");
        $aColumns = array("registrasi_id","registrasi_norekam",  "pasien_name", "keluhan_saat_ini", "registrasi_jaminan", "registrasi_date");
         
        $sLimit = 20;
        $sOffset = 0;
        $sOrder = "";
        $sWhere = "";
        $sGroup = "";
        $sEcho = 0;
        if(isset($param["sEcho"])){
            $sEcho = 0;
        }
        if(isset($param['start']) && $param['start'] > 0){
            $sOffset = $param['start'];
        }
        
        if(isset($param['order'])){
            if(empty($sOrder)){
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }else{
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }
        }

        $sWhere = "1=1";
        if(isset($param[$this->pasien->FIELD_PASIEN_NAME]) && !empty($param[$this->pasien->FIELD_PASIEN_NAME])){
            $sWhere .=" AND LOWER(p.".$this->pasien->FIELD_PASIEN_NAME.") LIKE '%".strtolower($param[$this->pasien->FIELD_PASIEN_NAME])."%'";
        }

        if(isset($param[$this->registrasi->FIELD_REGISTRASI_NOREKAM]) && !empty($param[$this->registrasi->FIELD_REGISTRASI_NOREKAM])){
            $sWhere .=" AND LOWER(p.".$this->registrasi->FIELD_REGISTRASI_NOREKAM.") LIKE '%".strtolower($param[$this->registrasi->FIELD_REGISTRASI_NOREKAM])."%'";
        }
        
        if($this->priv->{$this->privilege->FIELD_PRIVILEGE_SHOW_DATA} == $this->privilege->SELF_DATA){
            $sWhere .=" AND r.".$this->registrasi->FIELD_REGISTRASI_MODIFIEDBY."=".$this->session->userdata("id");
        }

        if(isset($param[$this->registrasi->FIELD_REGISTRASI_DATE]) && !empty($param[$this->registrasi->FIELD_REGISTRASI_DATE])){
            $sWhere .=" AND r.".$this->registrasi->FIELD_REGISTRASI_DATE."='".$param[$this->registrasi->FIELD_REGISTRASI_DATE]."'";
            
        }

        $searchWhere = "";
        if(!empty($param['search']['value'])){

            for($i=0;$i<count($aColumns);$i++){
                if(empty($searchWhere)){
                    $searchWhere .=$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }else{
                    $searchWhere .=" OR ".$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }

            }

            if(!empty($sWhere)){
                $sWhere .=" AND (".$searchWhere.")";
            }else{
                $sWhere .= "(".$searchWhere.")";
            }

        }
        $sTotal = $this->registrasi->getCountJoin($sWhere);
        $data = $this->registrasi->getListJoin($sOffset,$sLimit,$sWhere,$sOrder,$sGroup);
         
        $output = array(
            "sEcho" => intval($sEcho),
            "iTotalRecords" => $sTotal,
            "iTotalDisplayRecords" => $sTotal,
            "aaData" => array()
        );

        foreach ($data AS $key => $value){
            $row = array();
            $button = "";

            if(isset($this->priv->{$this->access->FIELD_ACCESS_UPDATE}) && $this->priv->{$this->access->FIELD_ACCESS_UPDATE} == $this->access->ACCESS){
                $button .="<a href='". site_url("operasional/registrasi/form/".$value->{$this->registrasi->FIELD_PRIMARY})."' class='btn btn-warning btn-xs m-t-10' data-toggle='tooltip' data-original-title='Edit' data-placement='top' style='margin-right:5px;' data-modal='#editor' data-title='Ubah Data Registrasi'><i class='fas fa-edit'></i></a>";
                $button .="<a href='". site_url("operasional/registrasi/print/".$value->{$this->registrasi->FIELD_PRIMARY})."' class='btn btn-default btn-xs m-t-10' data-toggle='tooltip' data-original-title='Edit' data-placement='top' style='margin-right:5px;' data-modal='#editor'><i class='fas fa-print'></i></a>";
            }
            if(isset($this->priv->{$this->access->FIELD_ACCESS_DELETE}) && $this->priv->{$this->access->FIELD_ACCESS_DELETE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->registrasi->FIELD_PRIMARY}."' data-url='". site_url("operasional/registrasi/deleteExc")."' class='delete btn btn-danger btn-xs m-t-10' data-toggle='tooltip' data-original-title='Hapus' data-placement='top' style='margin-right:5px;'><i class='fa fa-trash'></i></button>";
            }

            for($i = 0; $i < count($aColumns); $i++){
                    $reasstype = "";
                    if($aColumns[$i] == $this->registrasi->FIELD_PRIMARY){
                            $row[] = $sOffset+$key+1;
                    }else if($aColumns[$i] == $this->registrasi->FIELD_REGISTRASI_DATEMODIFIED){
                            $row[] = $button;
                    }else if($aColumns[$i] == $this->registrasi->FIELD_REGISTRASI_JAMINAN){
                        $displaystatus = "danger";
                        if($value->{$aColumns[$i]} == $this->registrasi->JAMINAN){
                            $displaystatus = "success";
                        }
                        $row[] = "<div class='label label-table label-".$displaystatus."'>".$this->registrasi->BIAYA[$value->{$aColumns[$i]}]."</div>";
                    }else{
                            $row[] = $value->{$aColumns[$i]};
                    }


            }

            $row[] = $button;
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function form($id = NULL){
        $data = array();
        $param = array();
        $param['id'] = 0;
        if(!empty($id)){
            $param['id'] = $id;
        }
		$param = $this->antiinjection->antiinject($param);
		$where = "";
        //GET Data
        $data['pasien'] = $this->pasien->getAll();
        $data['status'] = $this->registrasi->STATUS;
        $data['profile'] = $this->getprofile();
        $data['title'] = "Registrasi";
        $data['description'] = "Registrasi Pelayanan";
        $data['status'] = $this->registrasi->STATUS;
        $data['priv'] = $this->priv;
        $data['noreg'] = date("His")."-".date("m")."-".date("YmdHis");
        $data['dokter'] = $this->dokter->getList(0,0,$this->dokter->FIELD_DOKTER_STATUS."=".$this->dokter->ACTIVE_STATUS,"","");
        $data['pelayanan'] = $this->pelayanan->getList(0,0,$this->pelayanan->FIELD_PELAYANAN_STATUS."=".$this->pelayanan->ACTIVE_STATUS,"","");
        $data['jk'] = $this->pasien->JK;
        $data['pasienstatus'] = $this->pasien->STATUS;
        $data['agama'] = $this->agama->getList(0,0,$this->agama->FIELD_AGAMA_STATUS."=".$this->agama->ACTIVE_STATUS, "", "");
        $data['pekerjaan'] = $this->pekerjaan->getList(0,0,$this->pekerjaan->FIELD_PEKERJAAN_STATUS."=".$this->pekerjaan->ACTIVE_STATUS, "", "");
        $data['pernah'] = array("Tidak", "Ya");
        $data['biaya'] = $this->registrasi->BIAYA;
        $data['gol'] = $this->pasien->GOL;
        //echo "<pre>"; print_r($data['data']);die;
        if(isset($this->priv->{$this->access->FIELD_ACCESS_UPDATE}) && $this->priv->{$this->access->FIELD_ACCESS_UPDATE} == $this->access->ACCESS){
            $this->theme("admin/registrasi/form-registrasi", $data);
        }else{
            redirect("notfound");
        }
    }
    
    public function save(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);

        $filter = $this->checkparam($this->registrasi->getrules(), $param);
        if(count($filter) == 0){
            $data = $this->registrasi->fetch($this->registrasi->FIELD_PRIMARY."='".$param['id']."'");
            $param[$this->registrasi->FIELD_REGISTRASI_DATEMODIFIED] = date("Y-m-d H:i:s");
            if(isset($data->{$this->registrasi->FIELD_PRIMARY})){
                $this->updateExc($param);
            }else{
                $param[$this->registrasi->FIELD_REGISTRASI_DATECREATED] = date("Y-m-d H:i:s");
                $this->insertExc($param);
            }
        }else{
            $this->response = $filter;
        }
        
        echo json_encode($this->response);
    }

    public function updateExc($param){
        $id = $param['id'];

        unset($param['id']);
        $result = $this->registrasi->update($param, $this->registrasi->FIELD_PRIMARY."='".$id."'");

        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data registrasi berhasil diperbaharui';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function insertExc($param){

        unset($param['id']);
        $result = $this->registrasi->insert($param);
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data registrasi berhasil diperbaharui';
        }else{
            $this->response['status'] = 500;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function deleteExc(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $result = $this->registrasi->delete($this->registrasi->FIELD_PRIMARY."='".$param['id']."'");
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data registrasi berhasil dihapus';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menghapus data, silahkan ulangi kembali';
        }

        echo json_encode($this->response);
    }

}