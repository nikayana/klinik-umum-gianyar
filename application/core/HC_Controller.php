<?php
class HC_Controller extends CI_Controller{
    //put your code here
    public $result;
    public $datas;
    public $token;
    public $profile;
    
    public function __construct() {
        parent::__construct();
        $this->profile = array();

        $this->load->model("users/Usersmodel", "users");
        $this->load->model("users/Accessmodel", "access");
        $this->load->model("users/Menuheadermodel", "menuheader");
        $this->load->model("users/Menumodel", "menu");
        $this->load->model("users/privilegemodel", "privilege");
        $this->load->model("profile/profilemodel", "comprof");

        
        $this->checksession();


        
    }

    public function checkpriv($priv){
      $item = array();
      if($this->session->has_userdata("priv")){
        $item = $this->access->fetchJoin("a.".$this->access->FIELD_PRIVILEGE_ID."='".$this->session->userdata("priv")."' AND LOWER(m.".$this->menu->FIELD_MENU_LINK.")='".$priv."'");
      }
      

      return $item;
    }

    //CHECK SESSION
    private function checksession(){
      $classname = $this->router->fetch_class();
      $methodname = $this->router->fetch_method();
      if(!$this->session->has_userdata("username")){
        if($classname !== "welcome"){
          redirect("");
        }
      }else{
        if($classname === "welcome" && $methodname === "index"){
            redirect("dashboard");
        }
      }
    }

    public function getprofile(){
      $profile = $this->users->fetch($this->users->FIELD_PRIMARY."=".$this->session->userdata("id"));
      return $profile;
    }

    public function checkparam($rules, $param){
        $result = array();
        foreach ($rules as $key => $value) {
          if(!isset($param[$value['field']])){
              $result['status'] = 404;
              $result['messages'] = "Field ".$value['name']." tidak ditemukan ";
          }else{
            if(empty($param[$value['field']]) && $value['required'] == TRUE){
              $result['status'] = 404;
              $result['messages'] = "Field ".$value['name']." tidak boleh kosong";
            }
          }
        }
        return $result;
    }

    public function getcomprof(){
      $data = array();
      $data = $this->comprof->fetch();
      return $data;
    }

    public function theme($path = "", $data = array()){
      $data['content'] = $path;
      $data['sb_menusheader'] = $this->genheadermenu();
      $data['sb_menus'] = $this->genmenu();
      $data['comprof'] = $this->getcomprof();
      
      $this->load->view("themes/main", $data);
    }

    private function genheadermenu(){
      $result = array();
      if($this->session->has_userdata("priv")){
        $where = "a.".$this->access->FIELD_PRIVILEGE_ID."=".$this->session->userdata("priv")." AND ".$this->privilege->FIELD_PRIVILEGE_STATUS."=".$this->privilege->ACTIVE_STATUS." AND mh.".$this->menuheader->FIELD_HEADER_STATUS."=".$this->menuheader->ACTIVE_STATUS;
        $order = "mh.".$this->menuheader->FIELD_HEADER_POSITION." ASC, mh.".$this->menuheader->FIELD_PRIMARY." ASC";
        $group = "mh.".$this->menuheader->FIELD_HEADER_NAME;
        $result = $this->access->getListJoin(0,0,$where, $order, $group);
      }

      return $result;
    }

    private function genmenu(){
      $result = array();
      if($this->session->has_userdata("priv")){
        $where = "a.".$this->access->FIELD_PRIVILEGE_ID."=".$this->session->userdata("priv")." AND ".$this->privilege->FIELD_PRIVILEGE_STATUS."=".$this->privilege->ACTIVE_STATUS." AND mh.".$this->menuheader->FIELD_HEADER_STATUS."=".$this->menuheader->ACTIVE_STATUS;
        $order = "mh.".$this->menuheader->FIELD_HEADER_POSITION." ASC, mh.".$this->menuheader->FIELD_PRIMARY." ASC";
        $group = "mh.".$this->menuheader->FIELD_HEADER_NAME;
        $data = $this->access->getListJoin(0,0,$where, $order, "");
        $headerid = 0;
        foreach($data AS $key => $value){
          $menu = array();
          $menu['link'] = $value->{$this->menu->FIELD_MENU_LINK};
          $menu['name'] = $value->{$this->menu->FIELD_MENU_NAME};
          $result['header'.$value->header_id][] = $menu;
        }
      }
      return $result;
    }
}
