<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Privilegemodel extends CI_Model{
    //TABLE PROPERTIES
    private $table = "homecare_privilege";
    public $FIELD_PRIMARY = "privilege_id";
    public $FIELD_PRIVILEGE_NAME = "privilege_name";
    public $FIELD_PRIVILEGE_STATUS = "privilege_status";
    public $FIELD_PRIVILEGE_DATECREATED = "privilege_datecreated";
    public $FIELD_PRIVILEGE_DATEMODIFIED = "privilege_datemodified";
    public $FIELD_PRIVILEGE_MODIFIEDBY = "privilege_modifiedby";
    public $FIELD_PRIVILEGE_SHOW_DATA = "privilege_show_data";

    public $ACTIVE_STATUS = 1;
    public $NONACTIVE_STATUS = 0;
    public $STATUS = array(
        "Tidak Aktif",
        "Aktif"
    );

    public $SHOW_DATA = array(
        "Semua",
        "Sesuai yang login"
    );
    public $ALL_DATA = 0;
    public $SELF_DATA = 1;

    //RULES
    private $rules;

    function __construct(){
        parent::__construct();
        $this->rules = array(
            array(
                "name" => "nama",
                "field" => $this->FIELD_PRIVILEGE_NAME,
                "required" => true,
            ),
            array(
                "name" => "status",
                "field" => $this->FIELD_PRIVILEGE_STATUS,
                "required" => false,
            )
        );
    }

    public function getrules(){
        return $this->rules;
    }

    public function fetch($where = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    public function getAll(){
        $result = array();
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

    public function getList($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            $result = $query->result();
        }
        $result = $query->result();
        return $result;
    }

    public function getCount($where= ""){
        $result = 0;

        if(!empty($where)){
            $this->db->where($where);
        }

        if(!empty($goup)){
            $this->db->group_by($group);
        }

        $query = $this->db->get($this->table);
        $result = $query->num_rows();

        return $result;
    }

    public function insert($param){
        $result = false;
        $this->db->set($param);

        if($this->db->insert($this->table)){
            $result = true;
        }

        return $result;
    }

    public function insertBatch($param){
        $result = false;
        if($this->db->insert_batch($this->table, $param)){
            $data = true;
        }

        return $result;
    }

    public function update($param, $where){
        $result = false;
        $this->db->set($param);
        $this->db->where($where);

        if($this->db->update($this->table)){
            $result = true;
        }

        return $result;
    }

    public function delete($where){
        $result = false;
        $this->db->where($where);
        if($this->db->delete($this->table)){
            $result = true;
        }
        return $result;
    }
    
    
}
?>
