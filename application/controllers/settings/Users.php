<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends HC_Controller {
    private $response;
    private $modul;
    private $priv;

	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->response['status'] = 404;
        $this->response['messages'] = "Unknown Error, Please contact your administrator";
        $this->response['datas'] = array();

        $this->load->model("master/doktermodel", "dokter");

        $this->modul = "settings/users";
        $this->priv = $this->checkpriv($this->modul);

    }

    public function index(){
        $data = array();
        $data['title'] = "Pengguna";
        $data['description'] = "Mengelola data pengguna sistem";
        $data['privilege'] = $this->privilege->getAll();
        $data['status'] = $this->users->STATUS;
        $data['priv'] = $this->priv;
        if(isset($this->priv->{$this->access->FIELD_ACCESS_VIEW}) && $this->priv->{$this->access->FIELD_ACCESS_VIEW} == $this->access->ACCESS){
            $this->theme("admin/users/main-users", $data);
        }else{
            redirect("notfound");
        }
        
    }

    public function datatables(){
        $result = array();
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $aColumns = array("user_id","user_name", "user_email", "user_status", "privilege_name", "user_datemodified");

        $sLimit = 20;
        $sOffset = 0;
        $sOrder = "";
        $sWhere = "";
        $sGroup = "";
        $sEcho = 0;
        if(isset($param["sEcho"])){
            $sEcho = 0;
        }
        if(isset($param['start']) && $param['start'] > 0){
            $sOffset = $param['start'];
        }
        
        if(isset($param['order'])){
            if(empty($sOrder)){
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }else{
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }
        }

        $sWhere = "1=1";
        if(isset($param[$this->users->FIELD_PRIVILEGE_ID]) && strlen($param[$this->users->FIELD_PRIVILEGE_ID]) > 0){
            $sWhere .=" AND u.".$this->users->FIELD_PRIVILEGE_ID."=".$param[$this->users->FIELD_PRIVILEGE_ID];
        }

        if(isset($param[$this->users->FIELD_USER_NAME]) && !empty($param[$this->users->FIELD_USER_NAME])){
            $sWhere .=" AND LOWER(u.".$this->users->FIELD_USER_NAME.") LIKE '%".$param[$this->users->FIELD_USER_NAME]."%'";
        }

        if(isset($param[$this->users->FIELD_USER_EMAIL]) && !empty($param[$this->users->FIELD_USER_EMAIL])){
            $sWhere .=" AND LOWER(u.".$this->users->FIELD_USER_EMAIL.") LIKE '%".$param[$this->users->FIELD_USER_EMAIL]."%'";
        }

        if(isset($param[$this->users->FIELD_USER_STATUS]) && strlen($param[$this->users->FIELD_USER_STATUS]) > 0){
            $sWhere .=" AND u.".$this->users->FIELD_USER_STATUS."=".$param[$this->users->FIELD_USER_STATUS];
        }

        $searchWhere = "";
        if(!empty($param['search']['value'])){

            for($i=0;$i<count($aColumns);$i++){
                if(empty($searchWhere)){
                    $searchWhere .=$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }else{
                    $searchWhere .=" OR ".$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }

            }

            if(!empty($sWhere)){
                $sWhere .=" AND (".$searchWhere.")";
            }else{
                $sWhere .= "(".$searchWhere.")";
            }

        }
        $sTotal = $this->users->getCountJoin($sWhere);
        $data = $this->users->getListJoin($sOffset,$sLimit,$sWhere,$sOrder,$sGroup);
        $output = array(
            "sEcho" => intval($sEcho),
            "iTotalRecords" => $sTotal,
            "iTotalDisplayRecords" => $sTotal,
            "aaData" => array()
        );

        foreach ($data AS $key => $value){
            $row = array();
            $button = "";

            if(isset($this->priv->{$this->access->FIELD_ACCESS_UPDATE}) && $this->priv->{$this->access->FIELD_ACCESS_UPDATE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->users->FIELD_PRIMARY}."' data-url='". site_url("settings/users/form")."' class='addedit btn btn-warning btn-xs m-t-10' data-toggle='tooltip' data-original-title='Edit' data-placement='top' style='margin-right:5px;' data-modal='#editor' data-title='Ubah Data Pengguna'><i class='fas fa-edit'></i></button>";
            }
            if(isset($this->priv->{$this->access->FIELD_ACCESS_DELETE}) && $this->priv->{$this->access->FIELD_ACCESS_DELETE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->users->FIELD_PRIMARY}."' data-url='". site_url("settings/users/deleteExc")."' class='delete btn btn-danger btn-xs m-t-10' data-toggle='tooltip' data-original-title='Hapus' data-placement='top' style='margin-right:5px;'><i class='fa fa-trash'></i></button>";
            }

            for($i = 0; $i < count($aColumns); $i++){
                    $reasstype = "";
                    if($aColumns[$i] == $this->users->FIELD_PRIMARY){
                            $row[] = $sOffset+$key+1;
                    }else if($aColumns[$i] == $this->users->FIELD_USER_DATEMODIFIED){
                            $row[] = $button;
                    }else if($aColumns[$i] == $this->users->FIELD_USER_STATUS){
                        $row[] = $this->users->STATUS[$value->{$aColumns[$i]}];
                    }else{
                            $row[] = $value->{$aColumns[$i]};
                    }


            }

            $row[] = $button;
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function form(){
		$data = array();
		$param = $this->input->post(NULL, TRUE);
		$param = $this->antiinjection->antiinject($param);
		$where = "";
		//GET Data
        $data['data'] = $this->users->fetch($this->users->FIELD_PRIMARY."='".$param['id']."'");
        $data['status'] = $this->users->STATUS;
        $data['privilege'] = $this->privilege->getAll();
        $data['profile'] = $this->getprofile();
        $data["dokter"] = $this->dokter->getList(0,0,$this->dokter->FIELD_DOKTER_STATUS."=".$this->dokter->ACTIVE_STATUS,"","");

		$this->response['html'] = $this->load->view("admin/users/users-form", $data, TRUE);
		$this->response['status'] = 200;
		$this->response['messages'] = '';
        $this->response['datas'] = array();

		echo json_encode($this->response);
    }
    
    public function save(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);

        $filter = $this->checkparam($this->users->getrules(), $param);
        if(count($filter) == 0){
            $data = $this->users->fetch($this->users->FIELD_PRIMARY."='".$param['id']."'");
            $param[$this->users->FIELD_USER_DATEMODIFIED] = date("Y-m-d H:i:s");
            if(isset($data->{$this->users->FIELD_PRIMARY})){
                $this->updateExc($param);
            }else{
                $param[$this->users->FIELD_USER_DATECREATED] = date("Y-m-d H:i:s");
                if(empty($param[$this->users->FIELD_USER_PASSWORD])){
                    $this->response['status'] = 404;
                    $this->response['messages'] = 'Kata sandi harus diisi apabila menambahkan data pengguna baru';
                }else{
                    $this->insertExc($param);
                }
            }
        }else{
            $this->response = $filter;
        }
        
        echo json_encode($this->response);
    }

    public function updateExc($param){
        $id = $param['id'];
        if(empty($param[$this->users->FIELD_USER_PASSWORD])){
            unset($param[$this->users->FIELD_USER_PASSWORD]);
        }else{
            $param[$this->users->FIELD_USER_PASSWORD] = $this->ruangencrypt->hash($param[$this->users->FIELD_USER_PASSWORD]);
        }
        unset($param['id']);
        $result = $this->users->update($param, $this->users->FIELD_PRIMARY."='".$id."'");

        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data pengguna berhasil diperbaharui';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function insertExc($param){
        $param[$this->users->FIELD_USER_PASSWORD] = $this->ruangencrypt->hash($param[$this->users->FIELD_USER_PASSWORD]);
        unset($param['id']);
        $result = $this->users->insert($param);
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data jenis menu berhasil diperbaharui';
        }else{
            $this->response['status'] = 500;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function deleteExc(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $result = $this->users->delete($this->users->FIELD_PRIMARY."='".$param['id']."'");
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data pengguna berhasil dihapus';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menghapus data, silahkan ulangi kembali';
        }

        echo json_encode($this->response);
    }
}