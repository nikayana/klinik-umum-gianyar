<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Jadwal extends HC_Controller {
    private $response;
    private $modul;
    private $priv;

	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->response['status'] = 404;
        $this->response['messages'] = "Unknown Error, Please contact your administrator";
        $this->response['datas'] = array();
        $this->load->model("operasional/jadwalmodel", "jadwal");
        $this->load->model("master/doktermodel", "dokter");

        $this->modul = "operasional/jadwal";
        $this->priv = $this->checkpriv($this->modul);

    }

    public function index(){
        $data = array();
        $data['title'] = "Jadwal Dokter";
        $data['description'] = "Mengelola data jadwal dokter";
        $data['hari'] = $this->jadwal->HARI;
        $data['priv'] = $this->priv;

        if(isset($this->priv->{$this->access->FIELD_ACCESS_VIEW}) && $this->priv->{$this->access->FIELD_ACCESS_VIEW} == $this->access->ACCESS){
            $this->theme("admin/jadwal/main-jadwal", $data);
        }else{
            redirect("notfound");
        }
        
    }

    public function datatables(){
        $result = array();
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $aColumns = array("jadwal_id","jadwal_hari", "jadwal_start", "jadwal_end","dokter_name", "jadwal_datemodified");

        $sLimit = 20;
        $sOffset = 0;
        $sOrder = "";
        $sWhere = "";
        $sGroup = "";
        $sEcho = 0;
        if(isset($param["sEcho"])){
            $sEcho = 0;
        }
        if(isset($param['start']) && $param['start'] > 0){
            $sOffset = $param['start'];
        }
        
        if(isset($param['order'])){
            if(empty($sOrder)){
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }else{
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }
        }

        $sWhere = "1=1";
        if(isset($param[$this->dokter->FIELD_DOKTER_NAME]) && !empty($param[$this->dokter->FIELD_DOKTER_NAME])){
            $sWhere .=" AND LOWER(".$this->dokter->FIELD_DOKTER_NAME.") LIKE '%".strtolower($param[$this->dokter->FIELD_DOKTER_NAME])."%'";
        }

        if(isset($param[$this->jadwal->FIELD_JADWAL_HARI]) && strlen($param[$this->jadwal->FIELD_JADWAL_HARI]) > 0){
            $sWhere .=" AND ".$this->jadwal->FIELD_JADWAL_HARI."=".$param[$this->jadwal->FIELD_JADWAL_HARI]."";
        }

        if(isset($param[$this->jadwal->FIELD_DOKTER_ID]) && strlen($param[$this->jadwal->FIELD_DOKTER_ID]) > 0){
            $sWhere .=" AND d.".$this->jadwal->FIELD_DOKTER_ID."=".$param[$this->jadwal->FIELD_DOKTER_ID]."";
        }

        if($this->priv->{$this->privilege->FIELD_PRIVILEGE_SHOW_DATA} == $this->privilege->SELF_DATA){
            $sWhere .=" AND d.".$this->jadwal->FIELD_DOKTER_ID."=".$this->session->userdata("dokter");
        }

        $searchWhere = "";
        if(!empty($param['search']['value'])){

            for($i=0;$i<count($aColumns);$i++){
                if(empty($searchWhere)){
                    $searchWhere .=$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }else{
                    $searchWhere .=" OR ".$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }

            }

            if(!empty($sWhere)){
                $sWhere .=" AND (".$searchWhere.")";
            }else{
                $sWhere .= "(".$searchWhere.")";
            }

        }
        $sTotal = $this->jadwal->getCountJoin($sWhere);
        $data = $this->jadwal->getListJoin($sOffset,$sLimit,$sWhere,$sOrder,$sGroup);
        $output = array(
            "sEcho" => intval($sEcho),
            "iTotalRecords" => $sTotal,
            "iTotalDisplayRecords" => $sTotal,
            "aaData" => array()
        );

        foreach ($data AS $key => $value){
            $row = array();
            $button = "";

            if(isset($this->priv->{$this->access->FIELD_ACCESS_UPDATE}) && $this->priv->{$this->access->FIELD_ACCESS_UPDATE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->jadwal->FIELD_PRIMARY}."' data-url='". site_url("operasional/jadwal/form")."' class='addedit btn btn-warning btn-xs m-t-10' data-toggle='tooltip' data-original-title='Edit' data-placement='top' style='margin-right:5px;' data-modal='#editor' data-title='Ubah Data Jadwal'><i class='fas fa-edit'></i></button>";
            }
            if(isset($this->priv->{$this->access->FIELD_ACCESS_DELETE}) && $this->priv->{$this->access->FIELD_ACCESS_DELETE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->jadwal->FIELD_PRIMARY}."' data-url='". site_url("operasional/jadwal/deleteExc")."' class='delete btn btn-danger btn-xs m-t-10' data-toggle='tooltip' data-original-title='Hapus' data-placement='top' style='margin-right:5px;'><i class='fa fa-trash'></i></button>";
            }

            for($i = 0; $i < count($aColumns); $i++){
                    $reasstype = "";
                    if($aColumns[$i] == $this->jadwal->FIELD_PRIMARY){
                            $row[] = $sOffset+$key+1;
                    }else if($aColumns[$i] == $this->jadwal->FIELD_JADWAL_DATEMODIFIED){
                            $row[] = $button;
                    }else if($aColumns[$i] == $this->jadwal->FIELD_JADWAL_HARI){
                        $displaystatus = "success";
                        if($value->{$aColumns[$i]} == $this->jadwal->SABTU || $value->{$aColumns[$i]} == $this->jadwal->MINGGU){
                            $displaystatus = "danger";
                        }
                        $row[] = "<div class='label label-table label-".$displaystatus."'>".$this->jadwal->HARI[$value->{$aColumns[$i]}]."</div>";
                    }else{
                            $row[] = $value->{$aColumns[$i]};
                    }


            }

            $row[] = $button;
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function form(){
		$data = array();
		$param = $this->input->post(NULL, TRUE);
		$param = $this->antiinjection->antiinject($param);
		$where = "";
		//GET Data
        $data['data'] = $this->jadwal->fetch($this->jadwal->FIELD_PRIMARY."='".$param['id']."'");
        $data['hari'] = $this->jadwal->HARI;
        $data['dokter'] = $this->dokter->getList(0,0,$this->dokter->FIELD_DOKTER_STATUS."=".$this->dokter->ACTIVE_STATUS,"","");
        $data['profile'] = $this->getprofile();

		$this->response['html'] = $this->load->view("admin/jadwal/jadwal-form", $data, TRUE);
		$this->response['status'] = 200;
		$this->response['messages'] = '';
        $this->response['datas'] = array();

		echo json_encode($this->response);
    }
    
    public function save(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);

        $filter = $this->checkparam($this->jadwal->getrules(), $param);
        
        if(count($filter) == 0){
            if($param[$this->jadwal->FIELD_JADWAL_START] < $param[$this->jadwal->FIELD_JADWAL_END]){
                $data = $this->jadwal->fetch($this->jadwal->FIELD_PRIMARY."='".$param['id']."'");
                $where = $this->jadwal->FIELD_JADWAL_HARI."=".$param[$this->jadwal->FIELD_JADWAL_HARI]." 
                AND 
                    (
                        '".$param[$this->jadwal->FIELD_JADWAL_START]."' BETWEEN ".$this->jadwal->FIELD_JADWAL_START." AND ".$this->jadwal->FIELD_JADWAL_END." 
                            OR 
                        '".$param[$this->jadwal->FIELD_JADWAL_END]."' BETWEEN ".$this->jadwal->FIELD_JADWAL_START." AND ".$this->jadwal->FIELD_JADWAL_END." 
                    ) 
                AND '".$param[$this->jadwal->FIELD_JADWAL_END]."'!=".$this->jadwal->FIELD_JADWAL_START." 
                AND ".$this->jadwal->FIELD_DOKTER_ID."=".$param[$this->jadwal->FIELD_DOKTER_ID]." 
                AND ".$this->jadwal->FIELD_PRIMARY."!=".$param['id'];
                $exist = $this->jadwal->getCount($where);
                if(isset($exist) && $exist == 0){
                    $param[$this->jadwal->FIELD_JADWAL_DATEMODIFIED] = date("Y-m-d H:i:s");
                    if(isset($data->{$this->jadwal->FIELD_PRIMARY})){
                        $this->updateExc($param);
                    }else{
                        $param[$this->jadwal->FIELD_JADWAL_DATECREATED] = date("Y-m-d H:i:s");
                        $this->insertExc($param);
                    }
                }else{
                    $this->response['status'] = 404;
                    $this->response['messages'] = 'Jadwal yang inputkan sudah ada';
                    $this->response['datas'] = array();
                }
            }else{
                $this->response['status'] = 404;
                $this->response['messages'] = 'Jam selesai harus lebih besar dari jam mulai';
                $this->response['datas'] = array();
            }
            
        }else{
            $this->response = $filter;
        }
        
        echo json_encode($this->response);
    }

    public function updateExc($param){
        $id = $param['id'];

        unset($param['id']);
        $result = $this->jadwal->update($param, $this->jadwal->FIELD_PRIMARY."='".$id."'");

        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data jadwal berhasil diperbaharui';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function insertExc($param){

        unset($param['id']);
        $result = $this->jadwal->insert($param);
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data jadwal berhasil diperbaharui';
        }else{
            $this->response['status'] = 500;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function deleteExc(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $result = $this->jadwal->delete($this->jadwal->FIELD_PRIMARY."='".$param['id']."'");
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data jadwal berhasil dihapus';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menghapus data, silahkan ulangi kembali';
        }

        echo json_encode($this->response);
    }

    public function selectdata(){
        $response = array();
        $result = array();
        $raw = array();
        $raw['id'] = "";
        $raw['text'] = "Pilih Dokter";
        $result[] = $raw;
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        if(!empty($param["tanggal_kunjungan"])){
            $param['tanggal_kunjungan'] = date("N", strtotime($param['tanggal_kunjungan']));
        }
        $where = "1=1";
        $order = "d.".$this->dokter->FIELD_DOKTER_NAME." ASC";
        if(isset($param['term']) && !empty($param['term'])){
            $where .= " AND LOWER(d.".$this->dokter->FIELD_DOKTER_NAME.") LIKE '%".$param['term']."%'";
        }
        if(!empty($param['tanggal_kunjungan']) && !empty($param['jam_kunjungan'])){
            $where .=" AND j.".$this->jadwal->FIELD_JADWAL_HARI."=".$param['tanggal_kunjungan']." 
            AND '".$param['jam_kunjungan']."' BETWEEN j.".$this->jadwal->FIELD_JADWAL_START." AND ".$this->jadwal->FIELD_JADWAL_END;

            $data = $this->jadwal->getListJoin(0,0,$where, $order, "");
            foreach($data AS $key => $value){
                $raw = array();
                $raw['id'] = $value->{$this->dokter->FIELD_PRIMARY};
                $raw['text'] = $value->{$this->dokter->FIELD_DOKTER_NAME};
                $result[] = $raw;
            }
        }
        

        
        
        $response['results'] = $result;
        echo json_encode($response);
    }

}