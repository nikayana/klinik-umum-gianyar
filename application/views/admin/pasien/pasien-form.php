<input type="hidden" name="id" value="<?php echo isset($data->pasien_id) ? $data->pasien_id : 0 ?>">
<input type="hidden" name="pasien_modifiedby" value="<?php echo isset($profile->user_id) ? $profile->user_id : "" ?>">
<div class="form-material">
    <div class="row">
        <div class="col-md-4 b-r"> 
            <div class="form-group">
                <label>Nomor Rekam Medis <font color='red'>*</font></label>
                <input type="text" name="pasien_norekam" class="form-control form-control-line" value="<?php echo isset($data->pasien_norekam) ? $data->pasien_norekam : "" ?>" placeholder='Isikan nomor rekam medis' required="true"> 
            </div>
        </div>
        <div class="col-md-4 b-r"> 
            <div class="form-group">
                <label>Nama Pasien <font color='red'>*</font></label>
                <input type="text" name="pasien_name" class="form-control form-control-line" value="<?php echo isset($data->pasien_name) ? $data->pasien_name : "" ?>" placeholder='Isikan nama pasien' required="true"> 
            </div>
        </div>
        <div class="col-md-4 b-r">
            <div class="form-group">
                <label>Jenis Kelamin <font color='red'>*</font></label>
                <select class='form-control form-control-line' name="pasien_jk" required="true">
                    <option value=""> Jenis Kelamin </option>
                    <?php
                        if(isset($jk)){
                            foreach($jk AS $key => $value){
                                $selected = "";
                                if(isset($data->pasien_jk) && $data->pasien_jk == $key){
                                    $selected = "selected='selected'";
                                }
                                ?>
                                <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                                <?php
                                
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-md-8 b-r"> 
            <div class="form-group">
                <label>Alamat <font color='red'>*</font></label>
                <input type="text" name="pasien_alamat" class="form-control form-control-line" value="<?php echo isset($data->pasien_alamat) ? $data->pasien_alamat : "" ?>" placeholder='Isikan alamat pasien' required="true"> 
            </div>
        </div>
        <div class="col-md-4 b-r"> 
            <div class="form-group">
                <label>Tanggal Lahir </label>
                <input type="text" name="pasien_tgllahir" class="form-control form-control-line datepickers" value="<?php echo isset($data->pasien_tgllahir) ? $data->pasien_tgllahir : "" ?>" placeholder='Isikan tanggal lahir pasien' required="true"> 
            </div>
        </div>
        <div class="col-md-4 b-r"> 
            <div class="form-group">
                <label>No Telepon</label>
                <input type="text" name="pasien_phone" class="form-control form-control-line" value="<?php echo isset($data->pasien_phone) ? $data->pasien_phone : "" ?>" placeholder='Isikan nomor telepon pasien'> 
            </div>
        </div>
        <div class="col-md-4 b-r">
            <div class="form-group">
                <label>Agama <font color='red'>*</font></label>
                <select class='form-control form-control-line selects' name="pasien_agama" required="true" style="width:100%">
                    <option value=""> Pilih Agama </option>
                    <?php
                        if(isset($agama)){
                            foreach($agama AS $key => $value){
                                $selected = "";
                                if(isset($data->pasien_agama) && $data->pasien_agama == $value->agama_id){
                                    $selected = "selected='selected'";
                                }
                                ?>
                                <option value='<?php echo $value->agama_id ?>' <?php echo $selected ?>><?php echo $value->agama_name ?></option>
                                <?php
                                
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-md-4 b-r">
            <div class="form-group">
                <label>Pekerjaan <font color='red'>*</font></label>
                <select class='form-control form-control-line selects' name="pasien_pekerjaan" required="true" style="width:100%">
                    <option value=""> Pilih Pekerjaan </option>
                    <?php
                        if(isset($pekerjaan)){
                            foreach($pekerjaan AS $key => $value){
                                $selected = "";
                                if(isset($data->pasien_pekerjaan) && $data->pasien_pekerjaan == $value->pekerjaan_id){
                                    $selected = "selected='selected'";
                                }
                                ?>
                                <option value='<?php echo $value->pekerjaan_id ?>' <?php echo $selected ?>><?php echo $value->pekerjaan_name ?></option>
                                <?php
                                
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-md-4 b-r"> 
            <div class="form-group">
                <label>Tempat Lahir</label>
                <input type="text" name="pasien_tempatlahir" class="form-control form-control-line" value="<?php echo isset($data->pasien_tempatlahir) ? $data->pasien_tempatlahir : "" ?>" placeholder='Isikan tempat lahir pasien'> 
            </div>
        </div>
        <div class="col-md-4 b-r"> 
            <div class="form-group">
                <label>Wilayah</label>
                <input type="text" name="pasien_wilayah" class="form-control form-control-line" value="<?php echo isset($data->pasien_wilayah) ? $data->pasien_wilayah : "" ?>" placeholder='Isikan wilayah'> 
            </div>
        </div>
        <div class="col-md-4 b-r"> 
            <div class="form-group">
                <label>Fax</label>
                <input type="text" name="pasien_fax" class="form-control form-control-line" value="<?php echo isset($data->pasien_fax) ? $data->pasien_fax : "" ?>" placeholder='Isikan nomor fax'> 
            </div>
        </div>
        <div class="col-md-4 b-r"> 
            <div class="form-group">
                <label>No. HP</label>
                <input type="text" name="pasien_hp" class="form-control form-control-line" value="<?php echo isset($data->pasien_hp) ? $data->pasien_hp : "" ?>" placeholder='Isikan nomor handphone'> 
            </div>
        </div>
        <div class="col-md-4 b-r"> 
            <div class="form-group">
                <label>Email</label>
                <input type="email" name="pasien_email" class="form-control form-control-line" value="<?php echo isset($data->pasien_email) ? $data->pasien_email : "" ?>" placeholder='Isikan alamat email'> 
            </div>
        </div>
        <div class="col-md-4 b-r"> 
            <div class="form-group">
                <label>Berat Badan (dalam kilogram)</label>
                <input type="text" name="pasien_berat" class="form-control form-control-line" value="<?php echo isset($data->pasien_berat) ? $data->pasien_berat : "" ?>" placeholder='Isikan berat badan pasien'> 
            </div>
        </div>
        <div class="col-md-4 b-r"> 
            <div class="form-group">
                <label>Tinggi Badan (dalam centimeter)</label>
                <input type="text" name="pasien_tinggi" class="form-control form-control-line" value="<?php echo isset($data->pasien_tinggi) ? $data->pasien_tinggi : "" ?>" placeholder='Isikan tinggi badan pasien'> 
            </div>
        </div>
        <div class="col-md-4 b-r"> 
            <div class="form-group">
                <label>Nomor JKN</label>
                <input type="text" name="pasien_nojkn" class="form-control form-control-line" value="<?php echo isset($data->pasien_nojkn) ? $data->pasien_nojkn : "" ?>" placeholder='Isikan nomor JKN'> 
            </div>
        </div>
        <div class="col-md-4 b-r">
            <div class="form-group">
                <label>Golongan Darah <font color='red'>*</font></label>
                <select class='form-control form-control-line selects' name="pasien_gol" required="true" style="width:100%">
                    <option value=""> Pilih Golongan Darah </option>
                    <?php
                        if(isset($golongan)){
                            foreach($golongan AS $key => $value){
                                $selected = "";
                                if(isset($data->pasien_gol) && $data->pasien_gol == $key){
                                    $selected = "selected='selected'";
                                }
                                ?>
                                <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                                <?php
                                
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
    </div>
</div>
