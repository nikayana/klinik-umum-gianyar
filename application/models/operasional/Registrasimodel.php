<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasimodel extends CI_Model{
    //TABLE PROPERTIES
    private $table = "homecare_registrasi";
    private $table_pasien = "homecare_pasien";
    private $table_agama = "homecare_agama";
    private $table_pekerjaan = "homecare_pekerjaan";
    public $FIELD_PRIMARY = "registrasi_id";
    public $FIELD_REGISTRASI_NO = "registrasi_no";
    public $FIELD_REGISTRASI_NOREKAM = "registrasi_norekam";
    public $FIELD_PASIEN_ID = "pasien_id";
    public $FIELD_PASIEN_ALAMAT = "pasien_alamat";
    public $FIELD_REGISTRASI_DATE = "registrasi_date";
    public $FIELD_REGISTRASI_PHONE = "registrasi_phone";
    public $FIELD_REGISTRASI_DATECREATED = "registrasi_datecreated";
    public $FIELD_REGISTRASI_DATEMODIFIED = "registrasi_datemodified";
    public $FIELD_REGISTRASI_MODIFIEDBY = "registrasi_modifiedby";
    public $FIELD_REGISTRASI_STATUS = "registrasi_status";
    public $FIELD_REGISTRASI_NAME = "registrasi_name";
    public $FIELD_PASIEN_JK = "pasien_jk";
    public $FIELD_PASIEN_STATUS = "pasien_status";
    public $FIELD_PASIEN_AGAMA= "pasien_agama";
    public $FIELD_PASIEN_PENDIDIKAN= "pasien_pendidikan";
    public $FIELD_PASIEN_GOL= "pasien_gol";
    public $FIELD_PASIEN_PEKERJAAN= "pasien_pekerjaan";
    public $FIELD_PASIEN_PHONE = "pasien_phone";
    public $FIELD_REGISTRASI_PEKERJAAN = "registrasi_pekerjaan";
    public $FIELD_RAWAT_RS_KURANG = "rawat_rs_kurang";
    public $FIELD_RAWAT_RS_LEBIH = "rawat_rs_lebih";
    public $FIELD_RAWAT_LAIN_LEBIH = "rawat_lain_kurang";
    public $FIELD_RAWAT_LAIN_KURANG = "rawat_lain_lebih";
    public $FIELD_KELUHAN_SAAT_INI = "keluhan_saat_ini";
    public $FIELD_REGISTRASI_BIAYA = "registrasi_biaya";
    public $FIELD_REGISTRASI_SUKU = "registrasi_suku";
    public $FIELD_REGISTRASI_ALAMAT = "registrasi_alamat";
    public $FIELD_REGISTRASI_JAMINAN = "registrasi_jaminan";

    public $BIAYA = array(
        "BPJS", //0
        "Asuransi Lainnya", //1
        "Biaya Sendiri (UMUM)" //2
    );

    public $JAMINAN = 0;
    public $SENDIRI = 1;

    public $ACTIVE_STATUS = 1;
    public $NONACTIVE_STATUS = 0;
    public $STATUS = array(
        "Non Aktif",
        "Aktif"
    );

    //RULES
    private $rules;

    function __construct(){
        parent::__construct();

        $this->rules  = array(
            array(
                "name" => "alamat kunjungan",
                "field" =>$this->FIELD_PASIEN_ALAMAT,
                "required" => true,
            ),
            array(
                "name" => "pasien",
                "field" => $this->FIELD_PASIEN_ID,
                "required" => true,
            ),
            array(
                "name" => "tanggal registrasi",
                "field" => $this->FIELD_REGISTRASI_DATE,
                "required" => true,
            ),
            array(
                "name" => "pendaftar",
                "field" => $this->FIELD_REGISTRASI_NAME,
                "required" => true,
            ),
            array(
                "name" => "nomor yang bisa dihubungi",
                "field" => $this->FIELD_REGISTRASI_PHONE,
                "required" => true,
            )
        );
    }

    public function getrules(){
        return $this->rules;
    }
    public function fetch($where = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    public function fetchJoin($where = ""){
        $result = array();
        
        $this->db->select("p.pasien_name, p.pasien_jk, p.pasien_tgllahir, a.agama_name, pk.pekerjaan_name, r.*");
        $this->db->from($this->table." AS r");
        $this->db->join($this->table_pasien." AS p", "r.".$this->FIELD_PASIEN_ID."=p.pasien_id","LEFT");
        $this->db->join($this->table_agama." AS a", "p.pasien_agama=a.agama_id","LEFT");
        $this->db->join($this->table_pekerjaan." AS pk", "p.pasien_pekerjaan=pk.pekerjaan_id","LEFT");
         
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    public function getAll(){
        $result = array();
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

    public function getList($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getListJoin($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        $this->db->select("p.pasien_name, r.*");
        $this->db->from($this->table." AS r");
        $this->db->join($this->table_pasien." AS p", "r.".$this->FIELD_PASIEN_ID."=p.pasien_id","LEFT");
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get();
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getCount($where= ""){
        $result = 0;

        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->num_rows();

        return $result;
    }

    public function getCountJoin($where= ""){
        $result = 0;
        
        $this->db->from($this->table." AS r");
        $this->db->join($this->table_pasien." AS p", "r.".$this->FIELD_PASIEN_ID."=p.pasien_id","LEFT");
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get();
        $result = $query->num_rows();

        return $result;
    }

    public function insert($param){
        $result = false;
        $this->db->set($param);

        if($this->db->insert($this->table)){
            $result = true;
        }

        return $result;
    }

    public function insertBatch($param){
        $result = false;
        if($this->db->insert_batch($this->table, $param)){
            $data = true;
        }

        return $result;
    }

    public function update($param, $where){
        $result = false;
        $this->db->set($param);
        $this->db->where($where);

        if($this->db->update($this->table)){
            $result = true;
        }

        return $result;
    }

    public function delete($where){
        $result = false;
        $this->db->where($where);
        if($this->db->delete($this->table)){
            $result = true;
        }
        return $result;
    }
    
}
?>
