
<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-dark">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo site_url("dashboard") ?>">
                <!-- Logo icon -->
                <b>
                    <i class="fa fa-home"></i> Klinik Pratama Wahyu Medika
                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                    <!-- Dark Logo icon -->
                    <!--img src="<?php echo site_url("assets/images/logo-icon.png") ?>" alt="homepage" class="dark-logo" /-->
                    <!-- Light Logo icon -->
                    <!--img src="<?php echo site_url("assets/images/logo-light-icon.png") ?>" alt="homepage" class="light-logo" /-->
                </b>
                <!--End Logo icon -->
            </a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler d-block d-md-none waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <li class="nav-item"> <a class="nav-link sidebartoggler d-none waves-effect waves-dark" href="javascript:void(0)"><i class="icon-menu"></i></a> </li>
                <!-- ============================================================== -->
                
            </ul>
            <ul class="navbar-nav mr-auto">
                <li class="d-none d-md-block d-lg-block">
                    <a href="<?php echo site_url("dashboard") ?>" class="p-l-15">
                        <!--This is logo text-->
                        <!--img src="<?php echo site_url("assets/images/logo-light-text.png") ?>" alt="home" class="light-logo" alt="home"-->
                    </a>
                </li>
            </ul>
            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->
            <ul class="navbar-nav my-lg-0">
                <li class="nav-item dropdown u-pro">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?php echo site_url("assets/images/users/1.jpg") ?>" alt="user" class=""> <span class="hidden-md-down">
                        <?php 
                            if($this->session->has_userdata("name")){
                                echo $this->session->userdata("name");
                            }
                        ?>
                        &nbsp;<i class="fa fa-angle-down"></i></span> </a>
                    <div class="dropdown-menu dropdown-menu-right animated flipInY">
                        <!-- text-->
                        <div class="dropdown-divider"></div>
                        <!-- text-->
                        <a href="<?php echo site_url("welcome/logout") ?>" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
                        <!-- text-->
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>