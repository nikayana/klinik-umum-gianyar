<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Agama extends HC_Controller {
    private $response;
    private $modul;
    private $priv;

	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->response['status'] = 404;
        $this->response['messages'] = "Unknown Error, Please contact your administrator";
        $this->response['datas'] = array();
        $this->load->model("master/agamamodel", "agama");

        $this->modul = "master/agama";
        $this->priv = $this->checkpriv($this->modul);

    }

    public function index(){
        $data = array();
        $data['title'] = "Agama";
        $data['description'] = "Mengelola master data agama";
        $data['status'] = $this->agama->STATUS;
        $data['priv'] = $this->priv;

        if(isset($this->priv->{$this->access->FIELD_ACCESS_VIEW}) && $this->priv->{$this->access->FIELD_ACCESS_VIEW} == $this->access->ACCESS){
            $this->theme("admin/agama/main-agama", $data);
        }else{
            redirect("notfound");
        }
        
    }

    public function datatables(){
        $result = array();
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $aColumns = array("agama_id","agama_name", "agama_status", "agama_datemodified");

        $sLimit = 20;
        $sOffset = 0;
        $sOrder = "";
        $sWhere = "";
        $sGroup = "";
        $sEcho = 0;
        if(isset($param["sEcho"])){
            $sEcho = 0;
        }
        if(isset($param['start']) && $param['start'] > 0){
            $sOffset = $param['start'];
        }
        
        if(isset($param['order'])){
            if(empty($sOrder)){
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }else{
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }
        }

        $sWhere = "1=1";
        if(isset($param[$this->agama->FIELD_AGAMA_NAME]) && !empty($param[$this->agama->FIELD_AGAMA_NAME])){
            $sWhere .=" AND LOWER(".$this->agama->FIELD_AGAMA_NAME.") LIKE '%".strtolower($param[$this->agama->FIELD_AGAMA_NAME])."%'";
        }

        if(isset($param[$this->agama->FIELD_AGAMA_STATUS]) && strlen($param[$this->agama->FIELD_AGAMA_STATUS]) > 0){
            $sWhere .=" AND ".$this->agama->FIELD_AGAMA_STATUS."=".$param[$this->agama->FIELD_AGAMA_STATUS]."";
        }
        $searchWhere = "";
        if(!empty($param['search']['value'])){

            for($i=0;$i<count($aColumns);$i++){
                if(empty($searchWhere)){
                    $searchWhere .=$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }else{
                    $searchWhere .=" OR ".$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }

            }

            if(!empty($sWhere)){
                $sWhere .=" AND (".$searchWhere.")";
            }else{
                $sWhere .= "(".$searchWhere.")";
            }

        }
        $sTotal = $this->agama->getCount($sWhere);
        $data = $this->agama->getList($sOffset,$sLimit,$sWhere,$sOrder,$sGroup);
        $output = array(
            "sEcho" => intval($sEcho),
            "iTotalRecords" => $sTotal,
            "iTotalDisplayRecords" => $sTotal,
            "aaData" => array()
        );

        foreach ($data AS $key => $value){
            $row = array();
            $button = "";

            if(isset($this->priv->{$this->access->FIELD_ACCESS_UPDATE}) && $this->priv->{$this->access->FIELD_ACCESS_UPDATE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->agama->FIELD_PRIMARY}."' data-url='". site_url("master/agama/form")."' class='addedit btn btn-warning btn-xs m-t-10' data-toggle='tooltip' data-original-title='Edit' data-placement='top' style='margin-right:5px;' data-modal='#editor' data-title='Ubah Master Data Agama'><i class='fas fa-edit'></i></button>";
            }
            if(isset($this->priv->{$this->access->FIELD_ACCESS_DELETE}) && $this->priv->{$this->access->FIELD_ACCESS_DELETE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->agama->FIELD_PRIMARY}."' data-url='". site_url("master/agama/deleteExc")."' class='delete btn btn-danger btn-xs m-t-10' data-toggle='tooltip' data-original-title='Hapus' data-placement='top' style='margin-right:5px;'><i class='fa fa-trash'></i></button>";
            }

            for($i = 0; $i < count($aColumns); $i++){
                    $reasstype = "";
                    if($aColumns[$i] == $this->agama->FIELD_PRIMARY){
                            $row[] = $sOffset+$key+1;
                    }else if($aColumns[$i] == $this->agama->FIELD_AGAMA_DATEMODIFIED){
                            $row[] = $button;
                    }else if($aColumns[$i] == $this->agama->FIELD_AGAMA_STATUS){
                        $displaystatus = "danger";
                        if($value->{$aColumns[$i]} == $this->agama->ACTIVE_STATUS){
                            $displaystatus = "success";
                        }
                        $row[] = "<div class='label label-table label-".$displaystatus."'>".$this->agama->STATUS[$value->{$aColumns[$i]}]."</div>";
                    }else{
                            $row[] = $value->{$aColumns[$i]};
                    }


            }

            $row[] = $button;
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function form(){
		$data = array();
		$param = $this->input->post(NULL, TRUE);
		$param = $this->antiinjection->antiinject($param);
		$where = "";
		//GET Data
        $data['data'] = $this->agama->fetch($this->agama->FIELD_PRIMARY."='".$param['id']."'");
        $data['status'] = $this->agama->STATUS;
        $data['profile'] = $this->getprofile();

		$this->response['html'] = $this->load->view("admin/agama/agama-form", $data, TRUE);
		$this->response['status'] = 200;
		$this->response['messages'] = '';
        $this->response['datas'] = array();

		echo json_encode($this->response);
    }
    
    public function save(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);

        $filter = $this->checkparam($this->agama->getrules(), $param);
        if(count($filter) == 0){
            $data = $this->agama->fetch($this->agama->FIELD_PRIMARY."='".$param['id']."'");
            $param[$this->agama->FIELD_AGAMA_DATEMODIFIED] = date("Y-m-d H:i:s");
            if(isset($data->{$this->agama->FIELD_PRIMARY})){
                $this->updateExc($param);
            }else{
                $param[$this->agama->FIELD_AGAMA_DATECREATED] = date("Y-m-d H:i:s");
                $this->insertExc($param);
            }
        }else{
            $this->response = $filter;
        }
        
        echo json_encode($this->response);
    }

    public function updateExc($param){
        $id = $param['id'];

        unset($param['id']);
        $result = $this->agama->update($param, $this->agama->FIELD_PRIMARY."='".$id."'");

        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data agama berhasil diperbaharui';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function insertExc($param){

        unset($param['id']);
        $result = $this->agama->insert($param);
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data agama berhasil diperbaharui';
        }else{
            $this->response['status'] = 500;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function deleteExc(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $result = $this->agama->delete($this->agama->FIELD_PRIMARY."='".$param['id']."'");
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data agama berhasil dihapus';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menghapus data, silahkan ulangi kembali';
        }

        echo json_encode($this->response);
    }

    public function selectdata(){
        $response = array();
        $result = array();
        $raw = array();
        $raw['id'] = "";
        $raw['text'] = "Semua Agama";
        $result[] = $raw;
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $where = "";
        $order = $this->agama->FIELD_AGAMA_NAME." ASC";
        if(isset($param['term']) && !empty($param['term'])){
            $where .= "LOWER(".$this->agama->FIELD_AGAMA_NAME.") LIKE '%".$param['term']."%'";
            $data = $this->agama->getList(0,0,$where, $order, "");
            foreach($data AS $key => $value){
                $raw = array();
                $raw['id'] = $value->{$this->agama->FIELD_PRIMARY};
                $raw['text'] = $value->{$this->agama->FIELD_AGAMA_NAME};
                $result[] = $raw;
            }
        }
        
        $response['results'] = $result;
        echo json_encode($response);
    }

}