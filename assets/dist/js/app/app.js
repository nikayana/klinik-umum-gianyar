"use strict";

function loaddatatables(selector, formselector){
    let url = $(selector).data("url");
    let data = $("form"+formselector).serializeArray();
    data = toobject(data);
    //$('#basic-datatables').DataTable().clear().destroy();
    $(selector).DataTable({
        "destroy" : true,
        "processing": true,
        "serverSide": true,
        "lengthChange": false,
        "pageLength" : 20,
        "searching" : false,
        "ajax": {
            type : "GET", 
            url : url, 
            data : data
          }
    });
}

function toobject(param){
  let resultobject = {};
  $.each(param, function(i, v) {
      resultobject[v.name] = v.value;
  });
  return resultobject;
}

function select2ajax(selector){
  $(selector).select2({
    ajax : {
      url : $(selector).data("url"),
      dataType : "json"
    }
  })
}

$("body").on("click", ".addedit", function(){
    $("#notif").hide();
    let modalselector = $(this).data("modal");
    let modaltitle = $(this).data("title");
    $("#savebtn").removeAttr("disabled");
    $(modalselector).modal("show");
    $.ajax({
      type        : "POST",
      url         : $(this).data("url"),
      data        : {
          id : $(this).data("id"),
          priv_id : $(this).data("privId")
      },
      dataType    : 'json',
      beforeSend  : function(){
          $(modalselector+" .modal-title").html(modaltitle);
          $(modalselector+" .modal-body").html("<i class='fa fa-refresh fa-spin'></i> mohon tunggu...");
      },
      success : function(data){
        if(data.error == 'true'){
          $(modalselector).modal("hide");
          toastcustom(data.titlemessage, data.message, 'danger');
        }else{
          $(modalselector+" .modal-body").html(data.html);
        }
        
        $(".selects").select2({
          dropdownParent: $(modalselector)
        });
      },
      error : function(xhr, err){
        $(modalselector).modal("hide");
        toastcustom('Ups, Terjadi kesalahan', formatErrorMessage(xhr, err), 'danger');
      }
    }).done(function(){
      imagecropsmall();
      
      $(".datepickers").datepicker({
        autoclose: true,
        todayHighlight: true,
        format : "yyyy-mm-dd"
      });
      $('.clockpicker').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
      });
      select2ajax(".select2ajax");
    });
});
  
$("body").on("click", ".delete", function(){
    let id = $(this).data("id");
    let url = $(this).data("url");
    Swal.fire({
        title: 'Apakah anda yakin untuk menghapus data ini?',
        text: "Data yang terhapus tidak dapat dikembalikan lagi",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: "Yes, hapus!",
        cancelButtonText: "Tidak"
    }).then((result) => {
        if (result.value) {
          $.ajax({
            type        : "POST",
            url         : url,
            data        : {
              id : id
            },
            dataType    : 'json',
            beforeSend  : function(){
                Swal.fire({
                  text : "Processing...",
                  showConfirmButton : false
                });
            },
            success : function(data){
              if(data.status != 200){
                Swal.fire("Failed", data.messages, "error");
              }else{
                Swal.fire("Success", data.messages, "success");
                loaddatatables("#tables", "#search");
              }
            },
            error : function(xhr, err){
              swal("Oops", formatErrorMessage(xhr, err), "error");
            }
          });
        }
    });
});

$("form#formdata").submit(function(){
  $("#notif").hide();
  let modalselector = $(this).data("modal");
  $.ajax({
    type        : "POST",
    url         : $(this).data("url"),
    data        : $(this).serialize(),
    dataType    : 'json',
    cache       : false,
    beforeSend  : function(){
        $("#savebtn").attr({"disabled" : "true"});
    },
    success : function(data){
      if(data.status == 200){
        $(modalselector).modal("hide");
        toastcustom("Success", data.messages, 'success');
        $("#resetbtn").trigger("click");
        loaddatatables("#tables", "#search");
      }else{
        alertcustom(data.messages, "danger");
      }

      $("#savebtn").removeAttr("disabled");
    },
    error : function(xhr, err){
      Swal.fire({
        title: 'Ups, Terjadi kesalahan',
        text: formatErrorMessage(xhr, err),
        type: 'warning'
      });
      $("#savebtn").removeAttr("disabled");
    }
  });
  return false;
});

$("form#formdatareg").submit(function(){
  $("#notif").hide();
  $.ajax({
    type        : "POST",
    url         : $(this).data("url"),
    data        : $(this).serialize(),
    dataType    : 'json',
    cache       : false,
    beforeSend  : function(){
        $("#savebtn").attr({"disabled" : "true"});
    },
    success : function(data){
      if(data.status == 200){
        toastcustom("Success", data.messages, 'success');
        window.location.href=$("#resetbtn").attr("href");
      }else{
        alertcustom(data.messages, "danger");
      }

      $("#savebtn").removeAttr("disabled");
    },
    error : function(xhr, err){
      Swal.fire({
        title: 'Ups, Terjadi kesalahan',
        text: formatErrorMessage(xhr, err),
        type: 'warning'
      });
      $("#savebtn").removeAttr("disabled");
    }
  });
  return false;
});

$("#searchformbtn").click(function(){
  $("#searchform").slideToggle("medium");
});
$("form#search").submit(function(){
  loaddatatables("#tables", "#search");
  return false;
});
select2ajax(".select2ajax");
select2ajax("#pelayanan");
$(".selectmain").select2();
loaddatatables("#tables", "#search");

function imagecrop(){
  let $image_crop = $('#image_demo').croppie({
      enableExif: true,
      viewport: {
          width:200,
          height:200,
          type:'circle' //circle
      },
      boundary:{
        width:$("#sample").width(),
        height:$("#sample").height()
      },
      showZoomer : false
  });
  
  $("#linkupload").click(function(){
    $("#upload_image").trigger("click");
  })

  $('#upload_image').change(function(){
    $("#imgdisplay").hide();
    let reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#displaydemo').fadeIn('fast');
  });
  $('#check').click(function(event){
    let self = $(this);
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
      $.ajax({
        type        : "POST",
        url         : self.data("url"),
        data        : {
            id : self.data("id"),
            pict : response,
            old_photo : $("#old_photo").val(),
            profile_modifiedby : $("#modifby").val()
        },
        dataType    : 'json',
        beforeSend  : function(){
            self.html("Menyimpan...").attr("disabled", "true");
        },
        success : function(data){
          self.removeAttr("disabled").html("<i class='fa fa-'check'></i> Simpan");
          if(data.status != 200){
            toastcustom("Gagal mengupload gambar", data.messages, 'danger');
          }else{
            toastcustom("Berhasil", data.messages, 'success');
            $("#imgpreview").attr("src", response);
            $('#displaydemo').hide();
            $("#imgdisplay").show();
          }
        },
        error : function(xhr, err){
          toastcustom('Ups, Terjadi kesalahan', formatErrorMessage(xhr, err), 'danger');
        }
      })
     
    })
  });
}

function imagecropsmall(){
  let $image_crop = $('#image_demosmall').croppie({
      enableExif: true,
      viewport: {
          width:200,
          height:200,
          type:'square' //circle
      },
      boundary:{
        height:$("#samplesmall").height()
      },
      showZoomer : false
  });
  
  $("#linkuploadsmall").click(function(){
    $("#upload_imagesmall").trigger("click");
  })

  $('#upload_imagesmall').change(function(){
    $("#imgdisplaysmall").hide();
    let reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#displaydemosmall').fadeIn('fast');
  });
  $('#checksmall').click(function(event){
    let self = $(this);
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
        $("#imgpreviewsmall").attr("src", response);
        $('#displaydemosmall').hide();
        $("#imgdisplaysmall").show();
        $("#dokter_pict").val(response);
    })
  });
}

imagecrop();

$("body").on("submit", "form.tawar",function(){
  $.ajax({
    type        : "POST",
    url         : $(this).data("url"),
    data        : $(this).serialize(),
    dataType    : 'json',
    cache       : false,
    beforeSend  : function(){
      $("#baranglelang").html("Memuat data...");
    },
    success : function(data){
      if(data.status == 200){
        Swal.fire({
          title: 'Penawaran Tersimpan',
          text: data.messages,
          type: 'success'
        });
      }else{
        Swal.fire({
          title: 'Gagal menyimpandata',
          text: data.messages,
          type: 'error'
        });
      }
      baranglelang();
    },
    error : function(xhr, err){
      Swal.fire({
        title: 'Ups, Terjadi kesalahan',
        text: formatErrorMessage(xhr, err),
        type: 'error'
      });
      
      baranglelang();
    }
  });
});



$(document).on("click", ".infoket", function(e) {
  $(this).popover();
});

$("body").on("click", ".history", function(){
  let modalselector = $(this).data("modal");
  let modaltitle = $(this).data("title");
  $(modalselector).modal("show");
  $.ajax({
    type        : "POST",
    url         : $(this).data("url"),
    data        : {
        barang_id : $(this).data("barangId"),
        jadwal_id : $(this).data("jadwalId")
    },
    dataType    : 'json',
    beforeSend  : function(){
        $(modalselector+" .modal-title").html(modaltitle);
        $(modalselector+" .modal-body").html("<i class='fa fa-refresh fa-spin'></i> mohon tunggu...");
    },
    success : function(data){
      if(data.status == 'true'){
        $(modalselector).modal("hide");
        toastcustom(data.titlemessage, data.message, 'danger');
      }else{
        $(modalselector+" .modal-body").html(data.html);
      }
    },
    error : function(xhr, err){
      $(modalselector).modal("hide");
      toastcustom('Ups, Terjadi kesalahan', formatErrorMessage(xhr, err), 'danger');
    }
  });
});

$("form#searchlelang").submit(function(){
  baranglelang();
  return false;
})
$(".datepickers").datepicker({
  autoclose: true,
  todayHighlight: true,
  format : "yyyy-mm-dd"
});

$('.clockpicker').clockpicker({
  placement: 'bottom',
  align: 'left',
  autoclose: true,
  'default': 'now'
});

$("#pasien_id").change(function(){
   
  $.ajax({
    type        : "POST",
    url         : $(this).data("trigger"),
    data        : {
      id : $(this).val()
    },
    dataType    : 'json',
    cache       : false,
    success : function(data){
      console.log(data);
      $("#pasien_name").val(data.datas.name);
      $("#pasien_alamat").val(data.datas.alamat);
      $("#registrasi_norekam").val(data.datas.norekam);
      $("#pasien_jk").val(data.datas.jk);
      $("#pasien_jk_display").val(data.datas.jk_display);
      $("#pasien_gol").val(data.datas.gol);
      $("#pasien_pekerjaan").val(data.datas.pekerjaan);
      $("#pasien_phone").val(data.datas.phone);
      $("#agama").val(data.datas.agama);
    },
    error : function(xhr, err){
    }
  });
});

$("#equaldata").click(function(){
  let lastdata = $(this).is(":checked");
  if(lastdata == true){
    let nama = $("#pasien_id option:selected" ).text();
    let alamat = $("#pasien_alamat").val();
    let pekerjaan = $("#pasien_pekerjaan option:selected" ).text();
    let phone = $("#pasien_phone").val();

    $("#registrasi_name").val(nama); 
    $("#registrasi_alamat").val(alamat); 
    $("#registrasi_pekerjaan").val(pekerjaan); 
    $("#registrasi_phone").val(phone); 
  }else{
    $("#registrasi_name").val(""); 
    $("#registrasi_alamat").val(""); 
    $("#registrasi_pekerjaan").val(""); 
    $("#registrasi_phone").val(""); 
  }
})
$(".select2ajaxcustom").select2({
  ajax : {
    url : $(".select2ajaxcustom").data("url"),
    dataType : "json",
    data : function(params){
      return {
        term : params.term,
        tanggal_kunjungan : $("#kunjungan_date").val(),
        jam_kunjungan : $("#kunjungan_time").val()
      };
    }
  }
});

$("form#report").submit(function(){
  $.ajax({
    type        : "POST",
    url         : $(this).data("url"),
    data        : $(this).serialize(),
    dataType    : 'json',
    cache       : false,
    beforeSend  : function(){
        $("#reportbtn").attr({"disabled" : "true"});
    },
    success : function(data){
      if(data.status == 200){
        $("#displayreport").html(data.html);
      }else{
        alertcustom(data.messages, "danger");
      }

      $("#reportbtn").removeAttr("disabled");
    },
    error : function(xhr, err){
      Swal.fire({
        title: 'Ups, Terjadi kesalahan',
        text: formatErrorMessage(xhr, err),
        type: 'warning'
      });
      $("#reportbtn").removeAttr("disabled");
    }
  });
  return false;
});
$("#printbtn").click(function(){
  let data = $("form#report").serialize();
  let url = $(this).data("url");
  window.open(url+"?"+data);
});

if($("#datepaging").length > 0){
  $("#datepaging").datepaginator({
    onSelectedDateChanged: function(a, t) {
      $("#kunjungan_date").val(moment(t).format("YYYY-MM-DD"));
      $("form#search").trigger("submit");
    }
  });
}

$('.select2ajax').select2();