<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AntiInjection
 *
 * @author STAF IT
 */
class Ruangencrypt{
    private $key = 'ru@angdigit@l';
    public function hash($param){
      return password_hash($this->key.$param, PASSWORD_DEFAULT, array('cost' => 10));
    }

    public function verify($param, $hash){
      if(password_verify($this->key.$param, $hash)){
        return "true";
      }else{
        return "false";
      }
    }
}
