<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Icd10model extends CI_Model{
    //TABLE PROPERTIES
    private $table = "homecare_icd10";
    public $FIELD_PRIMARY = "icd10_id";
    public $FIELD_ICD10_NAME = "icd10_name";
    public $FIELD_ICD10_KODE = "icd10_kode";
    public $FIELD_ICD10_STATUS = "icd10_status";
    public $FIELD_ICD10_DATECREATED = "icd10_datecreated";
    public $FIELD_ICD10_DATEMODIFIED = "icd10_datemodified";
    public $FIELD_ICD10_MODIFIEDBY = "icd10_modifiedby";

    public $ACTIVE_STATUS = 1;
    public $NONACTIVE_STATUS = 0;
    public $STATUS = array(
        "Non Aktif",
        "Aktif"
    );

    //RULES
    private $rules;

    function __construct(){
        parent::__construct();

        $this->rules  = array(
            array(
                "name" => "icd10_name",
                "field" =>$this->FIELD_ICD10_NAME,
                "required" => true,
            ),
            array(
                "name" => "icd10_kode",
                "field" => $this->FIELD_ICD10_KODE,
                "required" => true,
            ),
            array(
                "name" => "status",
                "field" => $this->FIELD_ICD10_STATUS,
                "required" => true,
            )
        );
    }

    public function getrules(){
        return $this->rules;
    }
    public function fetch($where = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    public function getAll(){
        $result = array();
        $this->db->where('icd10_status', 1);
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

    public function getList($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getCount($where= ""){
        $result = 0;

        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->num_rows();

        return $result;
    }

    public function insert($param){
        $result = false;
        $this->db->set($param);

        if($this->db->insert($this->table)){
            $result = true;
        }

        return $result;
    }

    public function insertBatch($param){
        $result = false;
        if($this->db->insert_batch($this->table, $param)){
            $data = true;
        }

        return $result;
    }

    public function update($param, $where){
        $result = false;
        $this->db->set($param);
        $this->db->where($where);

        if($this->db->update($this->table)){
            $result = true;
        }

        return $result;
    }

    public function delete($where){
        $result = false;
        $this->db->where($where);
        if($this->db->delete($this->table)){
            $result = true;
        }
        return $result;
    }
    
}
?>
