<input type="hidden" name="id" value="<?php echo isset($data->dokter_id) ? $data->dokter_id : 0 ?>">
<input type="hidden" name="dokter_modifiedby" value="<?php echo isset($profile->user_id) ? $profile->user_id : "" ?>">
<input type="hidden" name="dokter_oldphoto" value="<?php echo isset($data->dokter_pict) ? $data->dokter_pict : "" ?>">
<input type="hidden" name="dokter_pict" value="" id="dokter_pict">

<div class="row">
    <div class='col-md-3'>
        <div class="card" id="imgdisplaysmall"> <img class="card-img" src="../assets/images/background/hospitalbg.jpg" height="256" id="samplesmall">
            <input type="file" name="rawfile" style="display:none;" id="upload_imagesmall">
            <div class="card-img-overlay card-inverse text-white social-profile d-flex justify-content-center">
                <div class="align-self-center"> <img src="<?php echo isset($data->dokter_pict) ? site_url("assets/images/profile/".$data->dokter_pict) : site_url("assets/images/users/1.jpg") ?>" class="img-circle" width="100" id="imgpreviewsmall"><br><br>
                    <p class="text-white" align="center"> Untuk mengupload / mengubah gambar<br><a href="#" id="linkuploadsmall">klik disini</a></p>
                </div>
            </div>
        </div>
            
                
        <div id="displaydemosmall" style="display:none;">
            <div id="image_demosmall"></div>
            <center><button type="button" class="btn btn-info" id="checksmall"><i class='fa fa-check'></i> Simpan</button></center>
        </div>
    </div>
    <div class='col-md-9 form-material'>
        <div class="row">
            <div class="col-md-4 b-r"> 
                <div class="form-group">
                    <label>Nama Dokter <font color='red'>*</font></label>
                    <input type="text" name="dokter_name" class="form-control form-control-line" value="<?php echo isset($data->dokter_name) ? $data->dokter_name : "" ?>" placeholder='Isikan nama dokter' required="true"> 
                </div>
            </div>
            <div class="col-md-4 b-r">
                <div class="form-group">
                    <label>Jenis Kelamin <font color='red'>*</font></label>
                    <select class='form-control form-control-line' name="dokter_jk" required="true">
                        <option value=""> Pilih Jenis Kelamin </option>
                        <?php
                            if(isset($jk)){
                                foreach($jk AS $key => $value){
                                    $selected = "";
                                    if(isset($data->dokter_jk) && $data->dokter_jk == $key){
                                        $selected = "selected='selected'";
                                    }
                                    ?>
                                    <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                                    <?php
                                    
                                }
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-4 b-r"> 
                <div class="form-group">
                    <label>No Telepon</label>
                    <input type="text" name="dokter_phone" class="form-control form-control-line" value="<?php echo isset($data->dokter_phone) ? $data->dokter_phone : "" ?>" placeholder='Isikan nomor telepon dokter'> 
                </div>
            </div>
            <div class="col-md-8 b-r"> 
                <div class="form-group">
                    <label>Alamat</label>
                    <input type="text" name="dokter_alamat" class="form-control form-control-line" value="<?php echo isset($data->dokter_alamat) ? $data->dokter_alamat : "" ?>" placeholder='Isikan alamat dokter'> 
                </div>
            </div>
            <div class="col-md-4 b-r"> 
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="dokter_email" class="form-control form-control-line" value="<?php echo isset($data->dokter_email) ? $data->dokter_email : "" ?>" placeholder='Isikan alamat email dokter'> 
                </div>
            </div>
            <div class="col-md-4 b-r">
                <div class="form-group">
                    <label>Status <font color='red'>*</font></label>
                    <select class='form-control form-control-line' name="dokter_status" required="true">
                        <option value=""> Pilih Status </option>
                        <?php
                            if(isset($status)){
                                foreach($status AS $key => $value){
                                    $selected = "";
                                    if(isset($data->dokter_status) && $data->dokter_status == $key){
                                        $selected = "selected='selected'";
                                    }
                                    ?>
                                    <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                                    <?php
                                    
                                }
                            }
                        ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

