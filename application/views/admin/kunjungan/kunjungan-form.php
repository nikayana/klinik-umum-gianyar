<input type="hidden" name="id" value="<?php echo isset($data->kunjungan_id) ? $data->kunjungan_id : 0 ?>">
<input type="hidden" name="kunjungan_modifiedby" value="<?php echo isset($profile->user_id) ? $profile->user_id : "" ?>">
<div class="form-material">
    <div class="row">
        <div class="col-md-12 b-r"> 
            <div class="form-group">
                <label>Pasien <font color='red'>*</font></label>
                <select class='form-control form-control-line selects' style='width:100%;' name="registrasi_id" required="true">
                    <option value=""> Pilih Pasien </option>
                    <?php
                        if(isset($registrasi)){
                            foreach($registrasi AS $key => $value){
                                $selected = "";
                                if(isset($data->registrasi_id) && $data->registrasi_id == $value->registrasi_id){
                                    $selected = "selected='selected'";
                                }

                                ?>
                                    <option value="<?php echo $value->registrasi_id ?>" <?php echo $selected ?>><?php echo "#".$value->registrasi_no." ".$value->pasien_name ?></option>
                                <?php
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Tanggal Kunjungan <font color='red'>*</font></label>
                <input type="text" name="kunjungan_date" class="form-control form-control-line datepickers" value="<?php echo isset($data->kunjungan_date) ? $data->kunjungan_date : "" ?>" placeholder='Isikan tanggal kunjungan' required="true"> 
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Jam Kunjungan <font color='red'>*</font></label>
                <input type="text" name="kunjungan_time" class="form-control form-control-line clockpicker" value="<?php echo isset($data->kunjungan_time) ? $data->kunjungan_time : "" ?>" placeholder='Isikan jam kunjungan' required="true"> 
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Dokter <font color='red'>*</font></label>
                <select class='form-control form-control-line selects' style='width:100%;' name="dokter_id" required="true">
                    <option value=""> Pilih Pasien </option>
                    <?php
                        if(isset($dokter)){
                            foreach($dokter AS $key => $value){
                                $selected = "";
                                if(isset($data->dokter_id) && $data->dokter_id == $value->dokter_id){
                                    $selected = "selected='selected'";
                                }

                                ?>
                                    <option value="<?php echo $value->dokter_id ?>" <?php echo $selected ?>><?php echo $value->dokter_name ?></option>
                                <?php
                            }
                        }
                    ?>
                </select> 
            </div>
        </div>
    </div>
</div>
