<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kunjunganpelayanan extends HC_Controller {
    private $response;
    private $modul;
    private $priv;

	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->response['status'] = 404;
        $this->response['messages'] = "Unknown Error, Please contact your administrator";
        $this->response['datas'] = array();
        $this->load->model("profile/pelayananmodel", "pelayanan");
        $this->load->model("profile/perawatanmodel", "perawatan");
        $this->load->model("operasional/kunjunganitemmodel", "kunjunganpelayanan");

        $this->modul = "operasional/kunjungan";
        $this->priv = $this->checkpriv($this->modul);

    }

    /*public function index(){
        $data = array();
        $data['title'] = "Pasien";
        $data['description'] = "Mengelola data pasien";
        $data['jk'] = $this->kunjunganpelayanan->JK;
        $data['agama'] = $this->agama->getList(0,0,$this->agama->FIELD_AGAMA_STATUS."=".$this->agama->ACTIVE_STATUS, "", "");
        $data['pekerjaan'] = $this->pekerjaan->getList(0,0,$this->pekerjaan->FIELD_PEKERJAAN_STATUS."=".$this->pekerjaan->ACTIVE_STATUS, "", "");
        $data['priv'] = $this->priv;

        if(isset($this->priv->{$this->access->FIELD_ACCESS_VIEW}) && $this->priv->{$this->access->FIELD_ACCESS_VIEW} == $this->access->ACCESS){
            $this->theme("admin/pasien/main-pasien", $data);
        }else{
            redirect("notfound");
        }
        
    }*/

   /* public function getdata(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);

        $data = $this->kunjunganpelayanan->fetch($this->kunjunganpelayanan->FIELD_PRIMARY."=".$param['id']);
        if(isset($data->{$this->kunjunganpelayanan->FIELD_PRIMARY})){
            $this->response['status'] = 200;
            $this->response['messages'] = "Data pasien ditemukan";
            $this->response['datas'] = array(
                "name" => $data->{$this->kunjunganpelayanan->FIELD_PASIEN_NAME},
                "alamat" => $data->{$this->kunjunganpelayanan->FIELD_PASIEN_ALAMAT},
                "norekam" => $data->{$this->kunjunganpelayanan->FIELD_PASIEN_NOREKAM},
                "jk" => $data->{$this->kunjunganpelayanan->FIELD_PASIEN_JK},
                "jk_display" => $this->kunjunganpelayanan->JK[$data->{$this->kunjunganpelayanan->FIELD_PASIEN_JK}],
                "gol" => $this->kunjunganpelayanan->GOL[$data->{$this->kunjunganpelayanan->FIELD_PASIEN_GOL}],
                "pekerjaan" => $data->{$this->kunjunganpelayanan->FIELD_PASIEN_PEKERJAAN},
                "phone" => $data->{$this->kunjunganpelayanan->FIELD_PASIEN_PHONE}
            );
        }else{
            $this->response['status'] = 200;
            $this->response['messages'] = "Data pasien tidak ditemukan";
        }

        echo json_encode($this->response);
    }*/

    public function datatables(){
        $result = array();
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $aColumns = array("kunjungan_pelayanan_id","pelayanan_name", "perawatan_name","pelayanan_harga", "pelayanan_lainnya", "kunjungan_pelayanan_datemodified");

        $sLimit = 20;
        $sOffset = 0;
        $sOrder = "";
        $sWhere = "";
        $sGroup = "";
        $sEcho = 0;
        if(isset($param["sEcho"])){
            $sEcho = 0;
        }
        if(isset($param['start']) && $param['start'] > 0){
            $sOffset = $param['start'];
        }
        
        if(isset($param['order'])){
            if(empty($sOrder)){
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }else{
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }
        }

        $sWhere = "1=1";

        if(isset($param['kunjungan_id'])){
            $sWhere .=" AND kp.".$this->kunjunganpelayanan->FIELD_KUNJUNGAN_ID."='".$param[$this->kunjunganpelayanan->FIELD_KUNJUNGAN_ID]."'";
        }

        $searchWhere = "";
        if(!empty($param['search']['value'])){

            for($i=0;$i<count($aColumns);$i++){
                if(empty($searchWhere)){
                    $searchWhere .=$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }else{
                    $searchWhere .=" OR ".$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }

            }

            if(!empty($sWhere)){
                $sWhere .=" AND (".$searchWhere.")";
            }else{
                $sWhere .= "(".$searchWhere.")";
            }

        }
        $sTotal = $this->kunjunganpelayanan->getCountJoin($sWhere);
        $data = $this->kunjunganpelayanan->getListJoin($sOffset,$sLimit,$sWhere,$sOrder,$sGroup);
        $output = array(
            "sEcho" => intval($sEcho),
            "iTotalRecords" => $sTotal,
            "iTotalDisplayRecords" => $sTotal,
            "aaData" => array()
        );

        foreach ($data AS $key => $value){
            $row = array();
            $button = "";

            if(isset($this->priv->{$this->access->FIELD_ACCESS_UPDATE}) && $this->priv->{$this->access->FIELD_ACCESS_UPDATE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->kunjunganpelayanan->FIELD_PRIMARY}."' data-url='". site_url("operasional/kunjunganpelayanan/form")."' class='addedit btn btn-warning btn-xs m-t-10' data-toggle='tooltip' data-original-title='Edit' data-placement='top' style='margin-right:5px;' data-modal='#editor' data-title='Ubah Pelayanan yang diinginkan'><i class='fas fa-edit'></i></button>";
            }
            if(isset($this->priv->{$this->access->FIELD_ACCESS_DELETE}) && $this->priv->{$this->access->FIELD_ACCESS_DELETE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->kunjunganpelayanan->FIELD_PRIMARY}."' data-url='". site_url("operasional/kunjunganpelayanan/deleteExc")."' class='delete btn btn-danger btn-xs m-t-10' data-toggle='tooltip' data-original-title='Hapus' data-placement='top' style='margin-right:5px;'><i class='fa fa-trash'></i></button>";
            }

            for($i = 0; $i < count($aColumns); $i++){
                    $reasstype = "";
                    if($aColumns[$i] == $this->kunjunganpelayanan->FIELD_PRIMARY){
                            $row[] = $sOffset+$key+1;
                    }else if($aColumns[$i] == $this->kunjunganpelayanan->FIELD_KUNJUNGAN_PELAYANAN_DATEMODIFIED){
                            $row[] = $button;
                    }else if($aColumns[$i] == $this->kunjunganpelayanan->FIELD_PELAYANAN_HARGA){
                        $row[] = "Rp ".number_format($value->{$aColumns[$i]}, 0,",",".");
                    }else{
                            $row[] = $value->{$aColumns[$i]};
                    }


            }

            $row[] = $button;
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function form($id = NULL){
		$data = array();
		$param = $this->input->post(NULL, TRUE);
		$param = $this->antiinjection->antiinject($param);
		$where = "";
        $data['kunjungan_id'] = isset($id) ? $id : 0;
        $data['data'] = $this->kunjunganpelayanan->fetch($this->kunjunganpelayanan->FIELD_PRIMARY."='".$param['id']."'");
        $data['perawatan'] = $this->perawatan->getList(0,0,$this->perawatan->FIELD_PERAWATAN_STATUS."=".$this->perawatan->ACTIVE_STATUS,"","");
        $data['profile'] = $this->getprofile();

		$this->response['html'] = $this->load->view("admin/kunjungan/pelayanan-form", $data, TRUE);
		$this->response['status'] = 200;
		$this->response['messages'] = '';
        $this->response['datas'] = array();

		echo json_encode($this->response);
    }
    
    public function save(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);

        $filter = $this->checkparam($this->kunjunganpelayanan->getrules(), $param);
        if(count($filter) == 0){
            $param[$this->kunjunganpelayanan->FIELD_PELAYANAN_ID] = 0;
            $perawatan = $this->perawatan->fetch($this->perawatan->FIELD_PRIMARY."=".$param[$this->kunjunganpelayanan->FIELD_PERAWATAN_ID]);
            if(isset($perawatan->{$this->perawatan->FIELD_PRIMARY})){
                $param[$this->kunjunganpelayanan->FIELD_PELAYANAN_ID] = $perawatan->{$this->perawatan->FIELD_PRIMARY};
            }
            $data = $this->kunjunganpelayanan->fetch($this->kunjunganpelayanan->FIELD_PRIMARY."='".$param['id']."'");
            $param[$this->kunjunganpelayanan->FIELD_KUNJUNGAN_PELAYANAN_DATEMODIFIED] = date("Y-m-d H:i:s");
            if(isset($data->{$this->kunjunganpelayanan->FIELD_PRIMARY})){
                $this->updateExc($param);
            }else{
                $param[$this->kunjunganpelayanan->FIELD_KUNJUNGAN_PELAYANAN_DATECREATED] = date("Y-m-d H:i:s");
                $this->insertExc($param);
            }
        }else{
            $this->response = $filter;
        }
        
        echo json_encode($this->response);
    }

    public function updateExc($param){
        $id = $param['id'];

        unset($param['id']);
        $result = $this->kunjunganpelayanan->update($param, $this->kunjunganpelayanan->FIELD_PRIMARY."='".$id."'");

        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data pelayanan berhasil diperbaharui';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function insertExc($param){

        unset($param['id']);
        $result = $this->kunjunganpelayanan->insert($param);
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data pelayanan berhasil diperbaharui';
        }else{
            $this->response['status'] = 500;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function deleteExc(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $result = $this->kunjunganpelayanan->delete($this->kunjunganpelayanan->FIELD_PRIMARY."='".$param['id']."'");
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data pelayanan berhasil dihapus';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menghapus data, silahkan ulangi kembali';
        }

        echo json_encode($this->response);
    }

    /*public function print($id){

        $mpdf = new \Mpdf\Mpdf();
        $data = array();
        $data['comprof'] = $this->getcomprof();
        $data['data'] = $this->kunjungan->fetchJoin("r.".$this->registrasi->FIELD_PRIMARY."=".$id);
        $data['jk'] = $this->pasien->JK;
        $data['status'] = $this->pasien->STATUS;
        $data['gol'] = $this->pasien->GOL;
    
        $mpdf->WriteHTML($this->load->view("admin/registrasi/print-registrasi", $data, TRUE));
        $mpdf->Output("Form Registrasi.pdf", 'I');

    }*/
    

}