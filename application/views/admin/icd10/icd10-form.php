<input type="hidden" name="id" value="<?php echo isset($data->icd10_id) ? $data->icd10_id : 0 ?>">
<input type="hidden" name="icd10_modifiedby" value="<?php echo isset($profile->user_id) ? $profile->user_id : "" ?>">  

<div class="row"> 
    <div class='col-md-12 form-material'>
        <div class="row"> 
            <div class="col-md-4 b-r"> 
                <div class="form-group">
                    <label>Kode</label>
                    <input type="text" name="icd10_kode" class="form-control form-control-line" value="<?php echo isset($data->icd10_kode) ? $data->icd10_kode : "" ?>" placeholder='Isikan Kode ICD10'> 
                </div>
            </div>  
            <div class="col-md-4 b-r"> 
                <div class="form-group">
                    <label>Nama ICD10 <font color='red'>*</font></label>
                    <input type="text" name="icd10_name" class="form-control form-control-line" value="<?php echo isset($data->icd10_name) ? $data->icd10_name : "" ?>" placeholder='Isikan nama ICD10' required="true"> 
                </div>
            </div> 
            <div class="col-md-4 b-r">
                <div class="form-group">
                    <label>Status <font color='red'>*</font></label>
                    <select class='form-control form-control-line' name="icd10_status" required="true">
                        <option value=""> Pilih Status </option>
                        <?php
                            if(isset($status)){
                                foreach($status AS $key => $value){
                                    $selected = "";
                                    if(isset($data->icd10_status) && $data->icd10_status == $key){
                                        $selected = "selected='selected'";
                                    }
                                    ?>
                                    <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                                    <?php
                                    
                                }
                            }
                        ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

