<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">
                    <b>Registrasi  Pasien</b>
                </h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard") ?>"><i class='fa fa-home'></i> Beranda</a></li>
                        <li class="breadcrumb-item active">Registrasi</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        
        <!-- ============================================================== -->
        <!-- Sales Chart and browser state-->
        <!-- ============================================================== -->
        <div class="row">
            <!-- Column -->
            <form id="formdatareg" data-url="<?php echo site_url("operasional/registrasi/save") ?>" data-modal="#editor">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Informasi Pasien</h4>
                        <p class="alert alert-danger">Jika pasien tidak ada, silahkan untuk membuat pasien baru pada menu master data pasien.</p>
                        <input type="hidden" name="id" value="<?php echo isset($data->registrasi_id) ? $data->registrasi_id : 0 ?>">
                        <input type="hidden" name="registrasi_modifiedby" value="<?php echo isset($profile->user_id) ? $profile->user_id : "" ?>">
                        <div class="form-material">
                            <div class="row">
                                <div class="col-md-6 b-r">
                                    <div class="form-group">
                                        <label>Nomor Registrasi <font color='red'>*</font></label>
                                        <input type="text" name="registrasi_no" class="form-control form-control-line" value="<?php echo isset($data->registrasi_no) ? $data->registrasi_no : $noreg ?>" placeholder='Isikan nomor registrasi' required="true" readonly> 
                                    </div>
                                </div>
                                <div class="col-md-6 b-r">
                                    <div class="form-group">
                                        <label>Tanggal Registrasi <font color='red'>*</font></label>
                                        <input type="text" name="registrasi_date" class="form-control form-control-line datepickers" value="<?php echo isset($data->registrasi_date) ? $data->registrasi_date : date("Y-m-d") ?>" placeholder='Isikan tanggal registrasi' required="true"> 
                                    </div>
                                </div>
                                <div class="col-md-3 b-r">
                                    <div class="form-group"> 
                                        <label>Cari Pasien <font color='red'>*</font></label>
                                        <select class='form-control form-control-line select2' name="pasien_id" required="true" style="width:100%" data-url="<?php echo site_url("operasional/pasien/selectdata") ?>" data-trigger="<?php echo site_url("operasional/pasien/getdata") ?>" id="pasien_id">
                                            <option value=""> Pilih Pasien </option>
                                            <?php 
                                                
                                                foreach($pasien as $key => $pasien){
                                                    ?>
                                                        <option value="<?php echo $pasien->pasien_id ?>"><?php echo $pasien->pasien_norekam.' - '.$pasien->pasien_name ?></option>
                                                    <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 b-r">
                                    <div class="form-group">
                                        <label>Nomor Rekam Medis <font color='red'>*</font></label>
                                        <input type="text" name="registrasi_norekam" class="form-control form-control-line" value="<?php echo isset($data->registrasi_norekam) ? $data->registrasi_norekam : "" ?>" placeholder='Nomor Rekam Medis Pasien' required="true" id="registrasi_norekam" readonly> 
                                    </div>
                                </div>
                                
                                <div class="col-md-3 b-r">
                                    <div class="form-group">
                                        <label>Jenis Kelamin</label>
                                        <input type="text" id="pasien_jk_display" name="pasien_jk_display" class="form-control form-control-line" value="<?php echo isset($data->pasien_jk) ? $jk[$data->pasien_jk] : "" ?>" disabled> 
                                        <input type="hidden" id="pasien_jk" name="pasien_jk" class="form-control form-control-line" value="<?php echo isset($data->pasien_jk) ? $data->pasien_jk: "" ?>"> 
                                    </div>
                                </div>  
                                <div class="col-md-3 b-r">
                                    <div class="form-group">
                                        <label>Agama</label>
                                        <select name="pasien_agama" class=" form-control" id="agama">
                                            <option value="">Pilih Agama</option>
                                            <?php  
                                                if(isset($agama)){
                                                    foreach($agama AS $key => $value){
                                                        $selected = "";
                                                        if(isset($data->pasien_agama) && $data->pasien_agama == $value->agama_id){
                                                            $selected = "selected='selected'";
                                                        }
                                                        ?>
                                                            <option value="<?php echo $value->agama_id ?>" <?php echo $selected ?>><?php echo $value->agama_name ?></option>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div> 
                                <div class="col-md-3 b-r">
                                    <div class="form-group">
                                        <label>Pekerjaan</label>
                                        <select id="pasien_pekerjaan" name="pasien_pekerjaan" class=" form-control" required style="width:100%;" required>
                                            <option value="">Pilih Pekerjaan</option>
                                            <?php  
                                                if(isset($pekerjaan)){
                                                    foreach($pekerjaan AS $key => $value){
                                                        $selected = "";
                                                        if(isset($data->pasien_pekerjaan) && $data->pasien_pekerjaan == $value->pekerjaan_id){
                                                            $selected = "selected='selected'";
                                                        }
                                                        ?>
                                                            <option value="<?php echo $value->pekerjaan_id ?>" <?php echo $selected ?>><?php echo $value->pekerjaan_name ?></option>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 b-r">
                                    <div class="form-group">
                                        <label>Golongan Darah</label>
                                        <input type="text"  id="pasien_gol" name="pasien_gol" class="form-control form-control-line" value="<?php echo isset($data->pasien_gol) ? $data->pasien_gol : "" ?>" placeholder="Isikan golongan darah pasien" readonly> 
                                    </div>
                                </div>
                                <div class="col-md-3 b-r">
                                    <div class="form-group">
                                        <label>Telp / HP <font color='red'>*</font></label>
                                        <input type="text" name="pasien_phone" class="form-control form-control-line" value="<?php echo isset($data->pasien_phone) ? $data->pasien_phone : "" ?>" placeholder='Isikan nomor telepon / hp pasien' id="pasien_phone" required="true"> 
                                    </div>
                                </div>
                                <div class="col-md-3 b-r">
                                    <div class="form-group">
                                        <label>Alamat KTP <font color='red'>*</font></label>
                                        <input type="text" name="pasien_alamat" class="form-control form-control-line" value="<?php echo isset($data->pasien_alamat) ? $data->pasien_alamat : "" ?>" placeholder='Isikan alamat pasien' id="pasien_alamat" required="true"> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <div>
                                <h5 class="card-title">Informasi Penanggung Jawab</h5>
                            </div>
                            <div class="ml-auto">
                                <input type="checkbox" id="equaldata"> Sama seperti data pasien
                            </div>
                        </div>
                        <div class="form-material">
                            <div class="row">
                                <div class="col-md-4 b-r">
                                    <div class="form-group">
                                        <label>Nama Pendaftar <font color='red'>*</font></label>
                                        <input type="text" name="registrasi_name" class="form-control form-control-line" value="<?php echo isset($data->registrasi_name) ? $data->registrasi_name : "" ?>" placeholder='Isikan nama pendaftar pasien' required="true" id="registrasi_name"> 
                                    </div>
                                </div>
                                <div class="col-md-4 b-r">
                                    <div class="form-group">
                                        <label>Pekerjaan <font color='red'>*</font></label>
                                        <input type="text" name="registrasi_pekerjaan" class="form-control form-control-line" value="<?php echo isset($data->registrasi_pekerjaan) ? $data->registrasi_pekerjaan : "" ?>" placeholder='Isikan pekerjaan penanggung jawab' id="registrasi_pekerjaan"> 
                                    </div>
                                </div>
                                <div class="col-md-4 b-r">
                                    <div class="form-group">
                                        <label>Telp / HP<font color='red'>*</font></label>
                                        <input type="text" name="registrasi_phone" class="form-control form-control-line" value="<?php echo isset($data->registrasi_phone) ? $data->registrasi_phone : "" ?>" placeholder='Isikan nomor telepon yang bisa dihubungi' required="true" id="registrasi_phone"> 
                                    </div>
                                </div>
                                <div class="col-md-12 b-r">
                                    <div class="form-group">
                                        <label>Alamat KTP<font color='red'>*</font></label>
                                        <input type="text" name="registrasi_alamat" class="form-control form-control-line" value="<?php echo isset($data->registrasi_alamat) ? $data->registrasi_alamat : "" ?>" placeholder='Isikan alamat pekerjaan penanggung jawab' required="true" id="registrasi_alamat"> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Riwayat Penyakit</h4>
                        <div class="form-material">
                            <div class="row">
                                 
                                <div class="col-md-6 b-r">
                                    <div class="form-group">
                                        <label>Keluhan Saat ini <font color='red'>*</font></label>
                                        <textarea name="keluhan_saat_ini" id="keluhan_saat_ini" class="form-control form-control-line"><?php echo isset($data->keluhan_saat_ini) ? $data->keluhan_saat_ini : "" ?></textarea>
                                    </div>
                                </div>
                                
                                <div class="col-md-6 b-r">
                                    <div class="form-group">
                                        <label>Jaminan Perawatan<font color='red'>*</font></label>
                                        <select class='form-control form-control-line'  name="registrasi_jaminan" required="true">
                                            <option value=""> Pilih Jaminan </option>
                                            <?php 
                                                if(isset($biaya)){
                                                    foreach($biaya AS $key => $value){
                                                        $selected = "";
                                                        if(isset($data->registrasi_biaya) && $data->registrasi_biaya == $key){
                                                            $selected = "selected='selected'";
                                                        }
                                                        ?>
                                                        <option value="<?php echo $key ?>" <?php echo $selected ?>><?php echo $value ?></option>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-md-12 b-r text-right">
                                    <div class="alert" id="notif" style="display:none;"></div>
                                    <button type="submit" class="btn btn-success" id="savebtn"><i class='fa fa-check'></i> Simpan</button>
                                    <a href="<?php echo site_url("operasional/registrasi") ?>" class="btn btn-danger" id="resetbtn"><i class='fa fa-history'></i> Reset</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            </form>
            <!-- Column -->
        </div>
        
        <!-- ============================================================== -->
        <!-- Comment - chats -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- End Comment - chats -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</div>