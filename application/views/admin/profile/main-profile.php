<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">
                    <b>Profil Rumah Sakit</b>
                </h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard") ?>"><i class='fa fa-home'></i> Beranda</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        
        <!-- ============================================================== -->
        <!-- Sales Chart and browser state-->
        <!-- ============================================================== -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-3 col-xlg-3 col-md-5">
                <div class="card" id="imgdisplay"> <img class="card-img" src="../assets/images/background/hospitalbg.jpg" height="456" id="sample">
                <input type="file" name="rawfile" style="display:none;" id="upload_image">
                    <div class="card-img-overlay card-inverse text-white social-profile d-flex justify-content-center">
                        <div class="align-self-center"> <img src="<?php echo isset($comprof->profile_logo) ? site_url("assets/images/profile/".$comprof->profile_logo) : site_url("assets/images/users/1.jpg") ?>" class="img-circle" width="200" id="imgpreview"><br><br>
                            <p class="text-white" align="center"> Untuk mengupload / mengubah gambar<br><a href="#" id="linkupload">klik disini</a></p>
                        </div>
                    </div>
                </div>
                
                    
                <div id="displaydemo" style="display:none;">
                    <div id="image_demo"></div>
                    <input type="hidden" id="old_photo" name="old_photo" value="<?php echo isset($comprof->profile_logo) ? $comprof->profile_logo : "" ?>">
                    <center><button type="button" class="btn btn-info" id="check" data-url="<?php echo site_url("profile/company/uploadimg") ?>" data-id="<?php echo isset($comprof->profile_id) ? $comprof->profile_id : "" ?>"><i class='fa fa-check'></i> Simpan</button></center>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-9 col-xlg-9 col-md-7">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"><?php echo isset($title) ? $title : "" ?></h4>
                        <h6 class="card-subtitle"> <?php echo isset($description) ? $description : "" ?> </h6>
                        <form class="form-material m-t-40" id="formdata" data-url="<?php echo site_url("profile/company/save") ?>">
                            <input type="hidden" name="id" value="<?php echo isset($comprof->profile_id) ? $comprof->profile_id : "" ?>">
                            <input type="hidden" id="modifby" name="profile_modifiedby" value="<?php echo $this->session->has_userdata("id") ? $this->session->userdata("id") : "" ?>">
                            <div class="row">
                                <div class="col-md-3 col-xs-6 b-r"> 
                                    <div class="form-group">
                                        <label><b>Nama Rumah Sakit <font color='red'>*</font></b></label>
                                        <input type="text" name="profile_name" class="form-control form-control-line" value="<?php echo isset($comprof->profile_name) ? $comprof->profile_name : "" ?>" placeholder='Isikan nama rumah sakit' required="true"> 
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> 
                                    <div class="form-group">
                                        <label><b>No Telepon</b> <font color='red'>*</font></label>
                                        <input type="text" name="profile_phone" class="form-control form-control-line" value="<?php echo isset($comprof->profile_phone) ? $comprof->profile_phone : "" ?>" placeholder='Isikan nomor telepon rumah sakit' required="true"> 
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> 
                                    <div class="form-group">
                                        <label><b>No Fax</b></label>
                                        <input type="text" name="profile_fax" class="form-control form-control-line" value="<?php echo isset($comprof->profile_fax) ? $comprof->profile_fax : "" ?>" placeholder='Isikan nomor fax rumah sakit' required="true"> 
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> 
                                    <div class="form-group">
                                        <label><b>Email</b> <font color='red'>*</font></label>
                                        <input type="email" name="profile_email" class="form-control form-control-line" value="<?php echo isset($comprof->profile_email) ? $comprof->profile_email : "" ?>" placeholder='Isikan alamat email rumah sakit' required="true"> 
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12 b-r"> 
                                    <div class="form-group">
                                        <label><b>Alamat</b> <font color='red'>*</font></label>
                                        <input type="text" name="profile_address" class="form-control form-control-line" value="<?php echo isset($comprof->profile_address) ? $comprof->profile_address : "" ?>" placeholder='Isikan alamat rumah sakit' required="true"> 
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12 b-r"> 
                                    <div class="form-group">
                                        <label><b>Visi Rumah Sakit</b></label>
                                        <textarea name="profile_visi" class="form-control form-control-line" placeholder="Isikan visi rumah sakit"><?php echo isset($comprof->profile_visi) ? $comprof->profile_visi : "" ?></textarea> 
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12 b-r"> 
                                    <div class="form-group">
                                        <label><b>Misi Rumah Sakit</b></label>
                                        <textarea name="profile_misi" class="form-control form-control-line" placeholder="Isikan misi rumah sakit"><?php echo isset($comprof->profile_misi) ? $comprof->profile_misi : "" ?></textarea> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-xs-6 b-r">
                                    <button type="submit" class="btn btn-success" id="savebtn"><i class='fa fa-check'></i> Simpan </button>
                                    <button type="reset" class="btn btn-default"><i class='fa fa-history'></i> Reset </button>
                                </div>
                                <div class='col-md-9 col-xs-12 b-r'>
                                    <div class="alert" id="notif" style="display:none;"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        
        <!-- ============================================================== -->
        <!-- Comment - chats -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- End Comment - chats -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</div>
<div class="modal fade" id="editor" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	   <div class="modal-dialog modal-lg" role="document">
		     <div class="modal-content">
              <form id="formdata" data-url="<?php echo site_url("settings/privilege/save") ?>" data-modal="#editor" class="needs-validation" novalidate>
			        <div class="modal-header">
			            <h5 class="modal-title">Modal title</h5>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
		                </button>
                    </div>
	                <div class="modal-body">
				            
        			</div>
        			<div class="modal-footer">
                        
                        <div class="col-md-12 mb-3">
                            <div class="alert" id="notif" style="display:none;"></div>
                        </div>
        				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Tutup</button>
        				<button type="submit" class="btn btn-success" id="savebtn"><i class='fa fa-check'></i> Simpan</button>
        			</div>
              </form>
          </div>
     </div>
</div>