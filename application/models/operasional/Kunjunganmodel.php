<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kunjunganmodel extends CI_Model{
    //TABLE PROPERTIES
    private $table = "homecare_kunjungan";
    private $table_icd10 = "homecare_icd10";
    private $table_dokter = "homecare_dokter";
    private $table_registrasi = "homecare_registrasi";
    private $table_pasien = "homecare_pasien";
    private $table_kunjungan_pelayanan = "homecare_kunjungan_pelayanan";
    private $table_pelayanan = "homecare_pelayanan";
    public $FIELD_PRIMARY = "kunjungan_id";
    public $FIELD_KUNJUNGAN_DATE = "kunjungan_date";
    public $FIELD_KUNJUNGAN_TIME = "kunjungan_time";
    public $FIELD_DOKTER_ID = "dokter_id";
    public $FIELD_REGISTRASI_ID = "registrasi_id";
    public $FIELD_AMNESA_KELUHAN = "amnesa_keluhan";
    public $FIELD_RIWAYAT_PENYAKIT = "riwayat_penyakit";
    public $FIELD_RIWAYAT_ALERGI_STATUS = "riwayat_alergi_status";
    public $FIELD_RIWAYAT_ALERGI= "riwayat_alergi";
    public $FIELD_OBAT_KONSUMSI = "obat_konsumsi";
    public $FIELD_ASSESSMENT = "assessment";
    public $FIELD_PLANING_TREATMENT = "planing_treatment";
    public $FIELD_PERKIRAAN_BIAYA = "perkiraan_biaya";
    public $FIELD_PETUGAS = "petugas";
    public $FIELD_BAHAN_DIPERLUKAN= "bahan_diperlukan";
    public $FIELD_KENDARAAN = "kendaraan";
    public $FIELD_JARAK_TEMPUH = "jarak_tempuh";
    public $FIELD_WAKTU_TEMPU = "waktu_tempuh";
    public $FIELD_BBM = "bbm";
    public $FIELD_JAM_BERANGKAT = "jam_berangkat";
    public $FIELD_JAM_PULANG = "jam_pulang";
    public $FIELD_JAM_TIBA = "jam_tiba";
    public $FIELD_KUNJUNGAN_DATECREATED = "kunjungan_datecreated";
    public $FIELD_KUNJUNGAN_DATEMODIFIED = "kunjungan_datemodified";
    public $FIELD_ATM = "atm";
    public $FIELD_ATM_LAINNYA = "atm_lainnya";
    public $FIELD_KETERSEDIAAN_INTERNET = "ketersediaan_internet";
    public $FIELD_STATUS_PEMERIKSAAN_LAB = "status_pemeriksaan_lab";
    public $FIELD_FILE_PEMERIKSAAN_LAB = "file_pemeriksaan_lab";
    public $FIELD_TGL_PEMERIKSAAN_LAB = "tgl_pemeriksaan_lab";
    public $FIELD_USER_LAB = "user_lab";
    public $FIELD_ICD10 = "icd10";

    public $PASIEN_BARU = 0;
    public $PASIEN_LAMA = 1;
    public $RSUD = array(
        "Pasien Baru",
        "Pasien Lama"
    );

    public $KENDARAAN = array(
        "Mobil",
        "Motor Dinas"
    );

    public $MOBIL = 0;
    public $MOTOR_DINAS = 1;

    public $ATM = array(
        "Bank NTT",
        "Bank BRI",
        "Lainnya"
    );

    public $BANK_NTT = 0;
    public $BANK_BRI = 1;
    public $LAINNYA = 2;

    public $INTERNET = array(
        "Tidak Ada",
        "Ada"
    );

    //RULES
    private $rules;

    function __construct(){
        parent::__construct();

        $this->rules  = array(
            array(
                "name" => "tanggal kunjungan",
                "field" =>$this->FIELD_KUNJUNGAN_DATE,
                "required" => true,
            ),
            array(
                "name" => "jam kunjungan",
                "field" => $this->FIELD_KUNJUNGAN_TIME,
                "required" => true,
            ),
            array(
                "name" => "dokter",
                "field" => $this->FIELD_DOKTER_ID,
                "required" => true,
            ),
            array(
                "name" => "pendaftar",
                "field" => $this->FIELD_REGISTRASI_ID,
                "required" => true,
            )
        );
    }

    public function getrules(){
        return $this->rules;
    }
    public function fetch($where = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->row();
        return $result;
    }

    public function fetchJoin($where = ""){
        $result = array();
        
        $this->db->select("k.*, r.registrasi_norekam, p.pasien_name, p.pasien_tgllahir, p.pasien_jk,
        p.pasien_alamat, p.pasien_phone");
        $this->db->from($this->table." AS k");
        $this->db->join($this->table_registrasi." AS r", "k.".$this->FIELD_REGISTRASI_ID."=r.registrasi_id","LEFT");
        $this->db->join($this->table_pasien." AS p", "r.pasien_id=p.pasien_id","LEFT");
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    public function getAll(){
        $result = array();
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

    public function getList($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get($this->table);
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getListJoin($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        $this->db->select("p.pasien_name, r.registrasi_norekam, d.dokter_name, k.*,  group_concat(pl.pelayanan_name separator ', ') AS pelayanan");
        $this->db->from($this->table." AS k");
        $this->db->join($this->table_registrasi." AS r", "k.".$this->FIELD_REGISTRASI_ID."=r.registrasi_id","LEFT");
        $this->db->join($this->table_pasien." AS p", "r.pasien_id=p.pasien_id","LEFT");
        $this->db->join($this->table_dokter." AS d", "k.".$this->FIELD_DOKTER_ID."=d.dokter_id","LEFT");
        $this->db->join($this->table_kunjungan_pelayanan." AS kp", "k.".$this->FIELD_PRIMARY."=kp.kunjungan_id", "LEFT");
        $this->db->join($this->table_pelayanan." AS pl", "kp.pelayanan_id=pl.pelayanan_id", "LEFT");
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get();
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getListJoinReport($start = 0, $limit = 0, $where = "", $order = "", $group = ""){
        $result = array();
        $this->db->select("p.pasien_name, r.registrasi_norekam, d.dokter_name, p.pasien_jk, p.pasien_alamat, r.registrasi_name, r.registrasi_phone, k.*");
        $this->db->from($this->table." AS k");
        $this->db->join($this->table_registrasi." AS r", "k.".$this->FIELD_REGISTRASI_ID."=r.registrasi_id","LEFT");
        $this->db->join($this->table_pasien." AS p", "r.pasien_id=p.pasien_id","LEFT");
        $this->db->join($this->table_dokter." AS d", "k.".$this->FIELD_DOKTER_ID."=d.dokter_id","LEFT");
        if(!empty($where)){
            $this->db->where($where);
        }

        if($start > 0 || $limit > 0){
            $this->db->limit($limit, $start);
        }

        if(!empty($group)){
            $this->db->group_by($group);
        }

        if(!empty($order)){
            $this->db->order_by($order);
        }

        $query = $this->db->get();
        if($query->num_rows() > 0){
            $result = $query->result();
        }

        $result = $query->result();
        return $result;
    }

    public function getListJoinReportAll($where = ""){
        $result = array();
        
        $this->db->select("i.icd10_kode, i.icd10_name, count(k.icd10) as jumlah");
        $this->db->from($this->table." AS k");
        $this->db->join($this->table_icd10." AS i", "k.".$this->FIELD_ICD10."=i.icd10_id","INNER"); 
        
        if(!empty($where)){
            $this->db->where($where);
        }

        $this->db->group_by("i.icd10_kode");
        $this->db->order_by("jumlah desc");
  
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $result = $query->result();
        }
        $result = $query->result();

        
        return $result;
    }

    public function getCount($where= ""){
        $result = 0;

        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get($this->table);
        $result = $query->num_rows();

        return $result;
    }

    public function getCountJoin($where= ""){
        $result = 0;
        $this->db->from($this->table." AS k");
        $this->db->join($this->table_registrasi." AS r", "k.".$this->FIELD_REGISTRASI_ID."=r.registrasi_id","LEFT");
        $this->db->join($this->table_pasien." AS p", "r.pasien_id=p.pasien_id","LEFT");
        $this->db->join($this->table_dokter." AS d", "k.".$this->FIELD_DOKTER_ID."=d.dokter_id","LEFT");
        if(!empty($where)){
            $this->db->where($where);
        }

        $query = $this->db->get();
        $result = $query->num_rows();

        return $result;
    }

    public function insert($param){
        $result = false;
        $this->db->set($param);

        if($this->db->insert($this->table)){
            $result = true;
        }

        return $result;
    }

    public function insertBatch($param){
        $result = false;
        if($this->db->insert_batch($this->table, $param)){
            $data = true;
        }

        return $result;
    }

    public function update($param, $where){
        $result = false;
        $this->db->set($param);
        $this->db->where($where);

        if($this->db->update($this->table)){
            $result = true;
        }

        return $result;
    }

    public function delete($where){
        $result = false;
        $this->db->where($where);
        if($this->db->delete($this->table)){
            $result = true;
        }
        return $result;
    }
    
}
?>
