<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Company extends HC_Controller {
    private $response;
    private $modul;
    private $priv;
	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->response['status'] = 404;
        $this->response['messages'] = "Unknown Error, Please contact your administrator";
        $this->response['datas'] = array();
        $this->modul = "profile/company";
        $this->priv = $this->checkpriv($this->modul);

    }

    public function index(){
        $data = array();
        $data['title'] = "Profil Rumah Sakit";
        $data['description'] = "Mengelola data profile rumah sakit";
        $data['priv'] = $this->priv;
    
        if(isset($this->priv->{$this->access->FIELD_ACCESS_VIEW}) && $this->priv->{$this->access->FIELD_ACCESS_VIEW} == $this->access->ACCESS){
            $this->theme("admin/profile/main-profile", $data);
        }else{
            redirect("notfound");
        }
        
    }
    
    public function save(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);

        $filter = $this->checkparam($this->comprof->getrules(), $param);
        if(count($filter) == 0){
            $data = $this->comprof->fetch($this->comprof->FIELD_PRIMARY."='".$param['id']."'");
            $param[$this->comprof->FIELD_PROFILE_DATEMODIFIED] = date("Y-m-d H:i:s");
            if(isset($data->{$this->comprof->FIELD_PRIMARY})){
                $this->updateExc($param);
            }else{
                $param[$this->comprof->FIELD_PROFILE_DATECREATED] = date("Y-m-d H:i:s");
                $this->insertExc($param);
            }
        }else{
            $this->response = $filter;
        }
        
        echo json_encode($this->response);
    }

    public function uploadimg(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        if(empty($param['pict'])){
            unset($param['pict']);
        }else{
            $param[$this->comprof->FIELD_PROFILE_LOGO] = $this->input->post('pict');
            $param[$this->comprof->FIELD_PROFILE_LOGO] = $this->convertimage($param[$this->comprof->FIELD_PROFILE_LOGO]);
            unset($param['pict']);
        }
        $this->updateExc($param);

        echo json_encode($this->response);
    }

    private function convertimage($param){
        $image_array_1 = explode(";", $param);
        $image_array_2 = explode(",", $image_array_1[1]);
        $data = base64_decode($image_array_2[1]);
        $path = "./assets/images/profile/";
        $imageName = "logo_".date("YmdHis"). '.png';
        file_put_contents($path.$imageName, $data);

        return $imageName;
    }

    public function updateExc($param){
        $id = $param['id'];

        if(isset($param['old_photo'])){
            
            if(file_exists("./assets/images/profile/".$param['old_photo']) && !empty($param['old_photo']) && !empty($param[$this->comprof->FIELD_PROFILE_LOGO])){
                @unlink("./assets/images/profile/".$param['old_photo']);
            }
        }
        if(isset($param['old_photo'])){
            unset($param['old_photo']);
        }
        
        unset($param['id']);
        $result = $this->comprof->update($param, $this->comprof->FIELD_PRIMARY."='".$id."'");

        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data profil rumah sakit berhasil diperbaharui';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function insertExc($param){

        unset($param['id']);
        $result = $this->comprof->insert($param);
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data profil rumah sakit berhasil diperbaharui';
        }else{
            $this->response['status'] = 500;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function deleteExc(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $result = $this->comprof->delete($this->comprof->FIELD_PRIMARY."='".$param['id']."'");
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data profil rumah sakit berhasil dihapus';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menghapus data, silahkan ulangi kembali';
        }

        echo json_encode($this->response);
    }

}