<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pasien extends HC_Controller {
    private $response;
    private $modul;
    private $priv;

	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->response['status'] = 404;
        $this->response['messages'] = "Unknown Error, Please contact your administrator";
        $this->response['datas'] = array();
        $this->load->model("master/agamamodel", "agama");
        $this->load->model("master/pekerjaanmodel", "pekerjaan");
        $this->load->model("operasional/pasienmodel", "pasien");

        $this->modul = "operasional/pasien";
        $this->priv = $this->checkpriv($this->modul);

    }

    public function index(){
        $data = array();
        $data['title'] = "Pasien";
        $data['description'] = "Mengelola data pasien";
        $data['jk'] = $this->pasien->JK;
        $data['agama'] = $this->agama->getList(0,0,$this->agama->FIELD_AGAMA_STATUS."=".$this->agama->ACTIVE_STATUS, "", "");
        $data['pekerjaan'] = $this->pekerjaan->getList(0,0,$this->pekerjaan->FIELD_PEKERJAAN_STATUS."=".$this->pekerjaan->ACTIVE_STATUS, "", "");
        $data['priv'] = $this->priv;

        if(isset($this->priv->{$this->access->FIELD_ACCESS_VIEW}) && $this->priv->{$this->access->FIELD_ACCESS_VIEW} == $this->access->ACCESS){
            $this->theme("admin/pasien/main-pasien", $data);
        }else{
            redirect("notfound");
        }
        
    }

    public function getdata(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);

        $data = $this->pasien->fetch($this->pasien->FIELD_PRIMARY."=".$param['id']);
        if(isset($data->{$this->pasien->FIELD_PRIMARY})){
            $this->response['status'] = 200;
            $this->response['messages'] = "Data pasien ditemukan";
            $this->response['datas'] = array(
                "name" => $data->{$this->pasien->FIELD_PASIEN_NAME},
                "alamat" => $data->{$this->pasien->FIELD_PASIEN_ALAMAT},
                "norekam" => $data->{$this->pasien->FIELD_PASIEN_NOREKAM},
                "jk" => $data->{$this->pasien->FIELD_PASIEN_JK},
                "jk_display" => $this->pasien->JK[$data->{$this->pasien->FIELD_PASIEN_JK}],
                "gol" => $this->pasien->GOL[$data->{$this->pasien->FIELD_PASIEN_GOL}],
                "pekerjaan" => $data->{$this->pasien->FIELD_PASIEN_PEKERJAAN},
                "phone" => $data->{$this->pasien->FIELD_PASIEN_PHONE},
                "agama" => $data->{$this->pasien->FIELD_PASIEN_AGAMA}, 
            );
        }else{
            $this->response['status'] = 200;
            $this->response['messages'] = "Data pasien tidak ditemukan";
        }

        echo json_encode($this->response);
    }

    public function datatables(){
        $result = array();
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $aColumns = array("pasien_id","pasien_norekam", "pasien_name", "pasien_tgllahir", "pasien_jk", "pasien_berat", "pasien_tinggi", "pasien_datemodified");

        $sLimit = 20;
        $sOffset = 0;
        $sOrder = "";
        $sWhere = "";
        $sGroup = "";
        $sEcho = 0;
        if(isset($param["sEcho"])){
            $sEcho = 0;
        }
        if(isset($param['start']) && $param['start'] > 0){
            $sOffset = $param['start'];
        }
        
        if(isset($param['order'])){
            if(empty($sOrder)){
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }else{
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }
        }

        $sWhere = "1=1";
        if(isset($param[$this->pasien->FIELD_PASIEN_NAME]) && !empty($param[$this->pasien->FIELD_PASIEN_NAME])){
            $sWhere .=" AND LOWER(p.".$this->pasien->FIELD_PASIEN_NAME.") LIKE '%".strtolower($param[$this->pasien->FIELD_PASIEN_NAME])."%'";
        }

        if(isset($param[$this->pasien->FIELD_PASIEN_NOREKAM]) && !empty($param[$this->pasien->FIELD_PASIEN_NOREKAM])){
            $sWhere .=" AND LOWER(p.".$this->pasien->FIELD_PASIEN_NOREKAM.") LIKE '%".strtolower($param[$this->pasien->FIELD_PASIEN_NOREKAM])."%'";
        }

        if(isset($param[$this->pasien->FIELD_PASIEN_JK]) && strlen($param[$this->pasien->FIELD_PASIEN_JK]) > 0){
            $sWhere .=" AND p.".$this->pasien->FIELD_PASIEN_JK."=".$param[$this->pasien->FIELD_PASIEN_JK]."";
        }

        if(isset($param[$this->pasien->FIELD_PASIEN_PEKERJAAN]) && strlen($param[$this->pasien->FIELD_PASIEN_PEKERJAAN]) > 0){
            $sWhere .=" AND p.".$this->pasien->FIELD_PASIEN_PEKERJAAN."=".$param[$this->pasien->FIELD_PASIEN_PEKERJAAN]."";
        }

        if(isset($param[$this->pasien->FIELD_PASIEN_AGAMA]) && strlen($param[$this->pasien->FIELD_PASIEN_AGAMA]) > 0){
            $sWhere .=" AND p.".$this->pasien->FIELD_PASIEN_AGAMA."=".$param[$this->pasien->FIELD_PASIEN_AGAMA]."";
        }


        $searchWhere = "";
        if(!empty($param['search']['value'])){

            for($i=0;$i<count($aColumns);$i++){
                if(empty($searchWhere)){
                    $searchWhere .=$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }else{
                    $searchWhere .=" OR ".$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }

            }

            if(!empty($sWhere)){
                $sWhere .=" AND (".$searchWhere.")";
            }else{
                $sWhere .= "(".$searchWhere.")";
            }

        }
        $sTotal = $this->pasien->getCountJoin($sWhere);
        $data = $this->pasien->getListJoin($sOffset,$sLimit,$sWhere,$sOrder,$sGroup);
        $output = array(
            "sEcho" => intval($sEcho),
            "iTotalRecords" => $sTotal,
            "iTotalDisplayRecords" => $sTotal,
            "aaData" => array()
        );

        foreach ($data AS $key => $value){
            $row = array();
            $button = "";

            if(isset($this->priv->{$this->access->FIELD_ACCESS_UPDATE}) && $this->priv->{$this->access->FIELD_ACCESS_UPDATE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->pasien->FIELD_PRIMARY}."' data-url='". site_url("operasional/pasien/form")."' class='addedit btn btn-warning btn-xs m-t-10' data-toggle='tooltip' data-original-title='Edit' data-placement='top' style='margin-right:5px;' data-modal='#editor' data-title='Ubah Data Pasien'><i class='fas fa-edit'></i></button>";
            }
            if(isset($this->priv->{$this->access->FIELD_ACCESS_DELETE}) && $this->priv->{$this->access->FIELD_ACCESS_DELETE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->pasien->FIELD_PRIMARY}."' data-url='". site_url("operasional/pasien/deleteExc")."' class='delete btn btn-danger btn-xs m-t-10' data-toggle='tooltip' data-original-title='Hapus' data-placement='top' style='margin-right:5px;'><i class='fa fa-trash'></i></button>";
            }

            for($i = 0; $i < count($aColumns); $i++){
                    $reasstype = "";
                    if($aColumns[$i] == $this->pasien->FIELD_PRIMARY){
                            $row[] = $sOffset+$key+1;
                    }else if($aColumns[$i] == $this->pasien->FIELD_PASIEN_DATEMODIFIED){
                            $row[] = $button;
                    }else if($aColumns[$i] == $this->pasien->FIELD_PASIEN_JK){
                        $displaystatus = "danger";
                        if($value->{$aColumns[$i]} == $this->pasien->LAKI){
                            $displaystatus = "success";
                        }
                        $row[] = "<div class='label label-table label-".$displaystatus."'>".$this->pasien->JK[$value->{$aColumns[$i]}]."</div>";
                    }else if($aColumns[$i] == $this->pasien->FIELD_PASIEN_TGLLAHIR){
                        $row[] = date("d F Y", strtotime($value->{$this->pasien->FIELD_PASIEN_TGLLAHIR}));
                    }else{
                            $row[] = $value->{$aColumns[$i]};
                    }


            }

            $row[] = $button;
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function form(){
		$data = array();
		$param = $this->input->post(NULL, TRUE);
		$param = $this->antiinjection->antiinject($param);
		$where = "";
		//GET Data
        $data['data'] = $this->pasien->fetch($this->pasien->FIELD_PRIMARY."='".$param['id']."'");
        $data['jk'] = $this->pasien->JK;
        $data['golongan'] = $this->pasien->GOL;
        $data['agama'] = $this->agama->getList(0,0,$this->agama->FIELD_AGAMA_STATUS."=".$this->agama->ACTIVE_STATUS, "", "");
        $data['pekerjaan'] = $this->pekerjaan->getList(0,0,$this->pekerjaan->FIELD_PEKERJAAN_STATUS."=".$this->pekerjaan->ACTIVE_STATUS, "", "");
        $data['profile'] = $this->getprofile();

		$this->response['html'] = $this->load->view("admin/pasien/pasien-form", $data, TRUE);
		$this->response['status'] = 200;
		$this->response['messages'] = '';
        $this->response['datas'] = array();

		echo json_encode($this->response);
    }
    
    public function save(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);

        $filter = $this->checkparam($this->pasien->getrules(), $param);
        if(count($filter) == 0){
            $data = $this->pasien->fetch($this->pasien->FIELD_PRIMARY."='".$param['id']."'");
            $param[$this->pasien->FIELD_PASIEN_DATEMODIFIED] = date("Y-m-d H:i:s");
            if(isset($data->{$this->pasien->FIELD_PRIMARY})){
                $this->updateExc($param);
            }else{
                $param[$this->pasien->FIELD_PASIEN_DATECREATED] = date("Y-m-d H:i:s");
                $this->insertExc($param);
            }
        }else{
            $this->response = $filter;
        }
        
        echo json_encode($this->response);
    }

    public function updateExc($param){
        $id = $param['id'];

        unset($param['id']);
        $result = $this->pasien->update($param, $this->pasien->FIELD_PRIMARY."='".$id."'");

        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data pasien berhasil diperbaharui';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function insertExc($param){

        unset($param['id']);
        $result = $this->pasien->insert($param);
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data pasien berhasil diperbaharui';
        }else{
            $this->response['status'] = 500;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function deleteExc(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $result = $this->pasien->delete($this->pasien->FIELD_PRIMARY."='".$param['id']."'");
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data pasien berhasil dihapus';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menghapus data, silahkan ulangi kembali';
        }

        echo json_encode($this->response);
    }

    public function selectdata(){
        $response = array();
        $result = array();
        $raw = array();
        $raw['id'] = "";
        $raw['text'] = "Pilih Pasien";
        $result[] = $raw;
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $where = "";
        $order = $this->pasien->FIELD_PASIEN_NAME." ASC";
        if(isset($param['term']) && !empty($param['term'])){
            $where .= "LOWER(".$this->pasien->FIELD_PASIEN_NAME.") LIKE '%".$param['term']."%'";
            $data = $this->pasien->getList(0,0,$where, $order, "");
            foreach($data AS $key => $value){
                $raw = array();
                $raw['id'] = $value->{$this->pasien->FIELD_PRIMARY};
                $raw['text'] = $value->{$this->pasien->FIELD_PASIEN_NAME};
                $result[] = $raw;
            }
        }
        
        $response['results'] = $result;
        echo json_encode($response);
    }

}