/*
 Navicat Premium Data Transfer

 Source Server         : DOCKER DB
 Source Server Type    : MySQL
 Source Server Version : 100508
 Source Host           : localhost:3307
 Source Schema         : miniklinik

 Target Server Type    : MySQL
 Target Server Version : 100508
 File Encoding         : 65001

 Date: 17/04/2022 21:50:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for homecare_access
-- ----------------------------
DROP TABLE IF EXISTS `homecare_access`;
CREATE TABLE `homecare_access` (
  `access_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(1) DEFAULT NULL,
  `access_add` int(1) DEFAULT NULL,
  `access_view` int(1) DEFAULT NULL,
  `access_update` int(1) DEFAULT NULL,
  `access_delete` int(1) DEFAULT NULL,
  `access_datecreated` datetime DEFAULT NULL,
  `access_datemodified` datetime DEFAULT NULL,
  `access_modifiedby` int(11) DEFAULT NULL,
  `privilege_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`access_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of homecare_access
-- ----------------------------
BEGIN;
INSERT INTO `homecare_access` VALUES (5, 2, 1, 1, 1, 1, '2020-07-07 13:32:21', '2020-07-07 13:32:21', 0, 1);
INSERT INTO `homecare_access` VALUES (6, 7, 1, 1, 1, 1, '2020-07-07 13:32:38', '2020-07-08 05:41:56', 0, 1);
INSERT INTO `homecare_access` VALUES (10, 5, 1, 1, 1, 1, '2020-07-07 13:37:10', '2020-07-08 06:11:57', 0, 1);
INSERT INTO `homecare_access` VALUES (12, 4, 1, 1, 1, 1, '2020-07-07 13:40:08', '2020-07-08 06:51:34', 0, 1);
INSERT INTO `homecare_access` VALUES (14, 6, 1, 1, 1, 1, '2020-07-07 13:40:34', '2020-07-08 06:06:11', 0, 1);
INSERT INTO `homecare_access` VALUES (16, 9, 1, 1, 1, 0, '2020-07-09 13:33:23', '2020-07-09 13:33:23', 4, 2);
INSERT INTO `homecare_access` VALUES (17, 2, 1, 1, 1, 1, '2020-07-09 14:03:27', '2020-07-09 14:03:27', 4, 2);
INSERT INTO `homecare_access` VALUES (18, 3, 1, 1, 1, 1, '2020-07-26 08:55:23', '2020-07-26 08:58:28', 4, 1);
INSERT INTO `homecare_access` VALUES (23, 19, 1, 1, 1, 1, '2020-08-05 15:03:15', '2020-08-05 15:03:15', 4, 1);
INSERT INTO `homecare_access` VALUES (24, 20, 1, 1, 1, 1, '2020-08-08 06:39:07', '2020-08-08 06:39:07', 4, 1);
INSERT INTO `homecare_access` VALUES (30, 2, 1, 1, 1, 1, '2020-08-27 00:00:35', '2020-08-27 00:00:35', 8, 6);
INSERT INTO `homecare_access` VALUES (31, 19, 1, 1, 1, 1, '2020-08-27 00:01:20', '2020-08-27 00:01:29', 8, 6);
INSERT INTO `homecare_access` VALUES (35, 24, 1, 1, 1, 1, '2020-08-27 21:48:17', '2020-08-27 21:48:17', 8, 1);
INSERT INTO `homecare_access` VALUES (36, 25, 1, 1, 1, 1, '2020-08-27 22:40:20', '2020-08-27 22:40:20', 8, 1);
INSERT INTO `homecare_access` VALUES (38, 22, 0, 1, 0, 0, '2020-08-28 20:45:52', '2020-08-28 20:45:52', 8, 7);
INSERT INTO `homecare_access` VALUES (39, 23, 1, 1, 1, 1, '2020-08-28 20:46:05', '2020-08-28 20:46:05', 8, 7);
INSERT INTO `homecare_access` VALUES (40, 25, 1, 1, 1, 1, '2020-08-28 20:46:29', '2020-08-28 20:46:29', 8, 8);
INSERT INTO `homecare_access` VALUES (41, 24, 1, 1, 1, 1, '2020-08-28 20:46:42', '2020-08-28 20:46:42', 8, 8);
INSERT INTO `homecare_access` VALUES (42, 2, 0, 1, 0, 0, '2020-08-28 21:23:55', '2020-08-28 21:23:55', 8, 7);
INSERT INTO `homecare_access` VALUES (43, 2, 0, 1, 0, 0, '2020-08-28 21:24:12', '2020-08-28 21:24:12', 8, 8);
INSERT INTO `homecare_access` VALUES (44, 15, 1, 1, 1, 1, '2021-01-07 13:33:02', '2021-01-07 13:33:02', 8, 7);
INSERT INTO `homecare_access` VALUES (45, 22, 0, 1, 0, 0, '2021-01-25 22:09:25', '2021-01-25 22:09:25', 8, 9);
INSERT INTO `homecare_access` VALUES (47, 21, 1, 1, 1, 0, '2021-01-25 22:10:23', '2021-01-25 22:10:23', 8, 9);
INSERT INTO `homecare_access` VALUES (48, 2, 0, 1, 0, 0, '2021-01-25 22:43:45', '2021-01-25 22:43:45', 8, 9);
INSERT INTO `homecare_access` VALUES (49, 2, 0, 1, 0, 0, '2022-03-13 15:29:10', '2022-03-13 15:29:10', 8, 10);
INSERT INTO `homecare_access` VALUES (50, 26, 1, 1, 1, 1, '2022-03-13 15:33:15', '2022-03-13 15:33:15', 8, 1);
INSERT INTO `homecare_access` VALUES (51, 27, 0, 1, 0, 0, '2022-03-21 14:35:12', '2022-03-21 14:35:12', 8, 10);
INSERT INTO `homecare_access` VALUES (52, 27, 1, 1, 1, 1, '2022-03-21 14:36:34', '2022-03-21 15:21:14', 8, 1);
INSERT INTO `homecare_access` VALUES (53, 28, 1, 1, 1, 1, '2022-03-22 14:07:48', '2022-03-22 14:07:48', 8, 1);
INSERT INTO `homecare_access` VALUES (54, 29, 1, 1, 1, 1, '2022-03-31 12:27:55', '2022-03-31 12:27:55', 8, 1);
INSERT INTO `homecare_access` VALUES (55, 29, 1, 1, 1, 1, '2022-04-02 05:20:38', '2022-04-02 05:20:38', 8, 6);
COMMIT;

-- ----------------------------
-- Table structure for homecare_agama
-- ----------------------------
DROP TABLE IF EXISTS `homecare_agama`;
CREATE TABLE `homecare_agama` (
  `agama_id` int(11) NOT NULL AUTO_INCREMENT,
  `agama_name` varchar(255) DEFAULT NULL,
  `agama_status` char(1) DEFAULT NULL,
  `agama_datecreated` datetime DEFAULT NULL,
  `agama_datemodified` datetime DEFAULT NULL,
  `agama_modifiedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`agama_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of homecare_agama
-- ----------------------------
BEGIN;
INSERT INTO `homecare_agama` VALUES (1, 'Islam', '1', '2020-08-04 15:45:04', '2020-08-04 15:45:04', 4);
INSERT INTO `homecare_agama` VALUES (2, 'Hindu', '1', '2020-08-04 15:45:11', '2020-08-04 15:45:11', 4);
INSERT INTO `homecare_agama` VALUES (3, 'Budha', '1', '2020-08-04 15:45:17', '2020-08-04 15:45:17', 4);
INSERT INTO `homecare_agama` VALUES (4, 'Kristen', '1', '2020-08-04 15:45:24', '2020-08-04 15:45:24', 4);
INSERT INTO `homecare_agama` VALUES (5, 'Protestan', '1', '2020-08-04 15:45:58', '2020-08-04 15:45:58', 4);
INSERT INTO `homecare_agama` VALUES (6, 'Konghucu', '1', '2020-08-04 15:46:13', '2020-08-04 15:46:13', 4);
COMMIT;

-- ----------------------------
-- Table structure for homecare_dokter
-- ----------------------------
DROP TABLE IF EXISTS `homecare_dokter`;
CREATE TABLE `homecare_dokter` (
  `dokter_id` int(11) NOT NULL AUTO_INCREMENT,
  `dokter_name` varchar(255) DEFAULT NULL,
  `dokter_phone` varchar(255) DEFAULT NULL,
  `dokter_alamat` varchar(255) DEFAULT NULL,
  `dokter_email` varchar(255) DEFAULT NULL,
  `dokter_jk` char(1) DEFAULT NULL,
  `dokter_pict` varchar(255) DEFAULT NULL,
  `dokter_status` char(1) DEFAULT NULL,
  `dokter_datecreated` datetime DEFAULT NULL,
  `dokter_datemodified` datetime DEFAULT NULL,
  `dokter_modifiedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`dokter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of homecare_dokter
-- ----------------------------
BEGIN;
INSERT INTO `homecare_dokter` VALUES (8, 'dr. Bopy Grisnawati Hamaduna', '085239777111', 'Kambajawa', 'Bopygrisnawati@gmail.com', '0', NULL, '1', '2021-01-31 18:27:04', '2021-01-31 18:27:04', 8);
INSERT INTO `homecare_dokter` VALUES (11, 'dr. Ervan suryanti umbu lapu', '085234112346', 'waingapu', 'Ervansuryanti@gmail.com', '1', NULL, '1', '2021-01-31 19:46:55', '2021-01-31 19:46:55', 8);
INSERT INTO `homecare_dokter` VALUES (13, 'dr. Crystina Ayu Putry', '082117234567', 'Waingapu', 'Crystinaayu@gmail.com', '0', NULL, '1', '2021-01-31 19:51:47', '2021-01-31 19:51:47', 8);
INSERT INTO `homecare_dokter` VALUES (14, 'dr. Ernesto Njurumana', '082346556267', 'Waingapu', 'Ernestonjurumana@gmail.com', '1', NULL, '1', '2021-01-31 19:53:31', '2021-01-31 19:53:31', 8);
COMMIT;

-- ----------------------------
-- Table structure for homecare_icd10
-- ----------------------------
DROP TABLE IF EXISTS `homecare_icd10`;
CREATE TABLE `homecare_icd10` (
  `icd10_id` int(11) NOT NULL AUTO_INCREMENT,
  `icd10_kode` varchar(255) DEFAULT NULL,
  `icd10_name` varchar(255) DEFAULT NULL,
  `icd10_status` char(1) DEFAULT NULL,
  `icd10_datecreated` datetime DEFAULT NULL,
  `icd10_datemodified` datetime DEFAULT NULL,
  `icd10_modifiedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`icd10_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of homecare_icd10
-- ----------------------------
BEGIN;
INSERT INTO `homecare_icd10` VALUES (15, 'A00-B99', 'Penyakit infeksius dan parasitik', '1', '2022-03-13 15:56:57', '2022-03-13 16:01:10', 8);
INSERT INTO `homecare_icd10` VALUES (16, 'A00-C01', 'Penyakit Fluenza', '1', '2022-03-13 15:56:57', '2022-03-13 15:56:57', 8);
COMMIT;

-- ----------------------------
-- Table structure for homecare_jadwal
-- ----------------------------
DROP TABLE IF EXISTS `homecare_jadwal`;
CREATE TABLE `homecare_jadwal` (
  `jadwal_id` int(11) NOT NULL AUTO_INCREMENT,
  `jadwal_hari` int(11) DEFAULT NULL,
  `jadwal_start` time DEFAULT NULL,
  `jadwal_end` time DEFAULT NULL,
  `jadwal_datecreated` datetime DEFAULT NULL,
  `jadwal_datemodified` datetime DEFAULT NULL,
  `jadwal_modifiedby` int(11) DEFAULT NULL,
  `dokter_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`jadwal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of homecare_jadwal
-- ----------------------------
BEGIN;
INSERT INTO `homecare_jadwal` VALUES (8, 1, '08:00:00', '14:00:00', '2021-01-31 18:28:25', '2021-01-31 18:28:25', 8, 8);
INSERT INTO `homecare_jadwal` VALUES (9, 2, '08:00:00', '14:00:00', '2021-01-31 18:28:42', '2021-01-31 18:28:42', 8, 8);
INSERT INTO `homecare_jadwal` VALUES (10, 3, '08:00:00', '14:00:00', '2021-01-31 20:41:52', '2021-01-31 20:41:52', 8, 11);
COMMIT;

-- ----------------------------
-- Table structure for homecare_kunjungan
-- ----------------------------
DROP TABLE IF EXISTS `homecare_kunjungan`;
CREATE TABLE `homecare_kunjungan` (
  `kunjungan_id` int(11) NOT NULL AUTO_INCREMENT,
  `kunjungan_date` date DEFAULT NULL,
  `kunjungan_time` time DEFAULT NULL,
  `dokter_id` int(11) DEFAULT NULL,
  `registrasi_id` int(11) DEFAULT NULL,
  `amnesa_keluhan` text DEFAULT NULL,
  `riwayat_penyakit` text DEFAULT NULL,
  `riwayat_alergi_status` char(1) DEFAULT NULL,
  `riwayat_alergi` text DEFAULT NULL,
  `obat_konsumsi` text DEFAULT NULL,
  `assessment` varchar(255) DEFAULT NULL,
  `planing_treatment` varchar(255) DEFAULT NULL,
  `perkiraan_biaya` double DEFAULT NULL,
  `petugas` varchar(255) DEFAULT NULL,
  `bahan_diperlukan` varchar(255) DEFAULT NULL,
  `kendaraan` char(1) DEFAULT NULL,
  `jarak_tempuh` double DEFAULT NULL,
  `waktu_tempuh` double DEFAULT NULL,
  `bbm` double DEFAULT NULL,
  `jam_berangkat` time DEFAULT NULL,
  `jam_pulang` time DEFAULT NULL,
  `jam_tiba` time DEFAULT NULL,
  `kunjungan_datecreated` datetime DEFAULT NULL,
  `kunjungan_datemodified` datetime DEFAULT NULL,
  `kunjungan_modifiedby` int(11) DEFAULT NULL,
  `atm` char(1) DEFAULT NULL,
  `atm_lainnya` varchar(255) DEFAULT NULL,
  `ketersediaan_internet` char(1) DEFAULT NULL,
  `no_rel` varchar(255) DEFAULT NULL,
  `kunjungan_pasien` char(1) DEFAULT NULL,
  `tindakan_dokter` text DEFAULT NULL,
  `status_pemeriksaan_lab` int(11) DEFAULT 0,
  `file_pemeriksaan_lab` varchar(255) DEFAULT NULL,
  `tgl_pemeriksaan_lab` datetime DEFAULT NULL,
  `user_lab` int(11) DEFAULT NULL,
  `icd10` int(11) DEFAULT NULL,
  `resep_obat` text DEFAULT NULL,
  PRIMARY KEY (`kunjungan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of homecare_kunjungan
-- ----------------------------
BEGIN;
INSERT INTO `homecare_kunjungan` VALUES (8, '2022-04-01', '20:00:00', 13, 29, 'sakit perut baru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-01 12:58:16', '2022-04-01 14:08:08', 8, NULL, NULL, NULL, NULL, '0', 'cek badan', 0, NULL, NULL, NULL, 15, 'parasetamol');
INSERT INTO `homecare_kunjungan` VALUES (9, '2022-03-31', '21:55:00', 14, 30, 'm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-01 13:34:12', '2022-04-01 14:09:06', 8, NULL, NULL, NULL, NULL, '0', 'v', 0, NULL, NULL, NULL, 16, 'xxx');
INSERT INTO `homecare_kunjungan` VALUES (10, '2022-04-01', '22:10:00', 11, 31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-01 14:11:58', '2022-04-01 14:11:58', 8, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `homecare_kunjungan` VALUES (11, '2022-04-02', '13:00:00', 8, 32, 'sakiperut bawah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-02 05:14:35', '2022-04-02 05:16:30', 8, NULL, NULL, NULL, NULL, '0', 'cek tubuh', 1, NULL, NULL, NULL, 16, 'obat x obat  b');
INSERT INTO `homecare_kunjungan` VALUES (12, '2022-04-12', '20:00:00', 13, 33, 'cek gula', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-12 12:04:13', '2022-04-14 01:00:53', 8, NULL, NULL, NULL, NULL, NULL, 'cek aj', 1, '33.pdf', '2022-04-14 00:00:00', 8, 16, 'parasetamol xxx');
COMMIT;

-- ----------------------------
-- Table structure for homecare_kunjungan_pelayanan
-- ----------------------------
DROP TABLE IF EXISTS `homecare_kunjungan_pelayanan`;
CREATE TABLE `homecare_kunjungan_pelayanan` (
  `kunjungan_id` int(11) DEFAULT NULL,
  `perawatan_id` int(11) DEFAULT NULL,
  `pelayanan_id` int(11) DEFAULT NULL,
  `pelayanan_lainnya` varchar(255) DEFAULT NULL,
  `pelayanan_harga` double DEFAULT NULL,
  `kunjungan_pelayanan_datecreated` datetime DEFAULT NULL,
  `kunjungan_pelayanan_datemodified` datetime DEFAULT NULL,
  `kunjungan_pelayanan_modifiedby` int(11) DEFAULT NULL,
  `kunjungan_pelayanan_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`kunjungan_pelayanan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for homecare_layanan
-- ----------------------------
DROP TABLE IF EXISTS `homecare_layanan`;
CREATE TABLE `homecare_layanan` (
  `layanan_id` int(11) NOT NULL AUTO_INCREMENT,
  `kunjungan_id` int(11) DEFAULT NULL,
  `pelayanan_id` int(11) DEFAULT NULL,
  `pelayanan_catatan` text DEFAULT NULL,
  `pelayanan_lainnya` varchar(255) DEFAULT NULL,
  `pelayanan_datecreated` datetime DEFAULT NULL,
  `pelayanan_datemodified` datetime DEFAULT NULL,
  `pelayanan_modifiedby` int(11) DEFAULT NULL,
  `pelayanan_harga` double DEFAULT NULL,
  PRIMARY KEY (`layanan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for homecare_menu
-- ----------------------------
DROP TABLE IF EXISTS `homecare_menu`;
CREATE TABLE `homecare_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) DEFAULT NULL,
  `menu_link` varchar(255) DEFAULT NULL,
  `header_id` int(11) DEFAULT NULL,
  `menu_status` int(1) DEFAULT NULL,
  `menu_datecreated` datetime DEFAULT NULL,
  `menu_datemodified` datetime DEFAULT NULL,
  `menu_modifiedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of homecare_menu
-- ----------------------------
BEGIN;
INSERT INTO `homecare_menu` VALUES (2, 'Dashboard', 'dashboard', 12, 1, '2020-07-03 08:14:10', '2020-07-05 14:29:12', 0);
INSERT INTO `homecare_menu` VALUES (3, 'Profil Klinik', 'profile/company', 35, 1, '2020-07-05 14:29:22', '2022-04-14 01:43:54', 8);
INSERT INTO `homecare_menu` VALUES (4, 'Pengguna Sistem', 'settings/users', 14, 1, '2020-07-05 14:29:45', '2022-03-13 14:09:56', 8);
INSERT INTO `homecare_menu` VALUES (5, 'Menu Header', 'settings/menuheader', 13, 1, '2020-07-05 14:30:39', '2020-07-26 08:40:17', 4);
INSERT INTO `homecare_menu` VALUES (6, 'Sub Menu', 'settings/menu', 13, 1, '2020-07-05 14:30:53', '2020-07-26 08:40:33', 4);
INSERT INTO `homecare_menu` VALUES (7, 'Hak Akses', 'settings/privilege', 13, 1, '2020-07-05 14:31:27', '2020-07-26 08:44:42', 4);
INSERT INTO `homecare_menu` VALUES (19, 'Pasien', 'operasional/pasien', 14, 1, '2020-08-05 15:02:56', '2020-08-05 15:02:56', 4);
INSERT INTO `homecare_menu` VALUES (20, 'Dokter', 'master/dokter', 14, 1, '2020-08-08 06:38:54', '2020-08-08 06:38:54', 4);
INSERT INTO `homecare_menu` VALUES (24, 'Laporan Kunjungan', 'report/report/reportkunjungan', 16, 1, '2020-08-09 03:49:40', '2020-08-09 03:49:40', 4);
INSERT INTO `homecare_menu` VALUES (25, 'Laporan Tranding Penyakit', 'report/report/reporttopten', 16, 1, '2020-08-09 03:50:25', '2022-04-14 01:42:43', 8);
INSERT INTO `homecare_menu` VALUES (26, 'ICD 10', 'master/icd10', 14, 1, '2022-03-13 15:32:05', '2022-03-13 15:32:05', 8);
INSERT INTO `homecare_menu` VALUES (27, 'Pemeriksaan Dokter', 'operasional/kunjungan', 36, 1, '2022-03-21 14:34:11', '2022-04-01 13:57:31', 8);
INSERT INTO `homecare_menu` VALUES (28, 'Pemeriksaan Lab', 'operasional/lab', 36, 1, '2022-03-22 14:07:22', '2022-03-22 14:15:33', 8);
INSERT INTO `homecare_menu` VALUES (29, 'Register Pasien', 'operasional/registrasi', 36, 1, '2022-03-31 12:27:22', '2022-03-31 12:34:40', 8);
COMMIT;

-- ----------------------------
-- Table structure for homecare_menu_header
-- ----------------------------
DROP TABLE IF EXISTS `homecare_menu_header`;
CREATE TABLE `homecare_menu_header` (
  `header_id` int(11) NOT NULL AUTO_INCREMENT,
  `header_name` varchar(2555) DEFAULT NULL,
  `header_status` int(11) DEFAULT NULL,
  `header_datecreated` datetime DEFAULT NULL,
  `header_datemodified` datetime DEFAULT NULL,
  `header_modifiedby` int(11) DEFAULT NULL,
  `header_icon` varchar(255) DEFAULT NULL,
  `header_position` int(1) DEFAULT NULL,
  PRIMARY KEY (`header_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of homecare_menu_header
-- ----------------------------
BEGIN;
INSERT INTO `homecare_menu_header` VALUES (12, 'Beranda', 1, '2020-07-03 03:14:29', '2020-07-07 15:12:34', 0, 'icon-speedometer', 1);
INSERT INTO `homecare_menu_header` VALUES (13, 'Pengaturan', 1, '2022-03-13 14:03:42', '2022-04-14 01:43:37', 8, 'fa fa-cogs', 3);
INSERT INTO `homecare_menu_header` VALUES (14, 'Master Data', 1, '2020-07-03 04:32:57', '2020-08-04 15:42:40', 4, 'fa fa-server', 4);
INSERT INTO `homecare_menu_header` VALUES (16, 'Laporan', 1, '2020-07-03 04:34:19', '2021-04-21 22:37:19', 8, 'ti-agenda', 6);
INSERT INTO `homecare_menu_header` VALUES (35, 'Tentang Kami', 1, '2020-08-03 10:43:05', '2020-08-05 14:19:15', 4, 'fa fa-info-circle', 2);
INSERT INTO `homecare_menu_header` VALUES (36, 'Pemeriksaan', 1, '2020-08-09 03:46:46', '2022-03-21 14:33:13', 8, 'fa fa-stethoscope', 5);
COMMIT;

-- ----------------------------
-- Table structure for homecare_pasien
-- ----------------------------
DROP TABLE IF EXISTS `homecare_pasien`;
CREATE TABLE `homecare_pasien` (
  `pasien_id` int(11) NOT NULL AUTO_INCREMENT,
  `pasien_name` varchar(255) DEFAULT NULL,
  `pasien_alamat` varchar(255) DEFAULT NULL,
  `pasien_tgllahir` date DEFAULT NULL,
  `pasien_jk` char(1) DEFAULT NULL,
  `pasien_pekerjaan` int(11) DEFAULT NULL,
  `pasien_agama` int(11) DEFAULT NULL,
  `pasien_norekam` varchar(255) DEFAULT NULL,
  `pasien_nojkn` varchar(255) DEFAULT NULL,
  `pasien_berat` double DEFAULT NULL,
  `pasien_tinggi` double DEFAULT NULL,
  `pasien_phone` varchar(255) DEFAULT NULL,
  `pasien_datecreated` datetime DEFAULT NULL,
  `pasien_datemodified` datetime DEFAULT NULL,
  `pasien_modifiedby` int(11) DEFAULT NULL,
  `pasien_tempatlahir` varchar(255) DEFAULT NULL,
  `pasien_kodepos` varchar(255) DEFAULT NULL,
  `pasien_wilayah` varchar(255) DEFAULT NULL,
  `pasien_fax` varchar(255) DEFAULT NULL,
  `pasien_hp` varchar(255) DEFAULT NULL,
  `pasien_email` varchar(255) DEFAULT NULL,
  `pasien_gol` char(1) DEFAULT NULL,
  PRIMARY KEY (`pasien_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of homecare_pasien
-- ----------------------------
BEGIN;
INSERT INTO `homecare_pasien` VALUES (11, 'i made joko', 'badung', '2022-03-02', '1', 69, 4, '0001', '-', 10, 189, '08123123', '2022-03-31 12:40:12', '2022-03-31 12:40:12', 8, 'badung', NULL, 'mengwi', '-', '09123123', 'masd@mas.com', '2');
INSERT INTO `homecare_pasien` VALUES (12, 'mita', 'dps', '2022-03-15', '0', 2, 1, '0002', '', 0, 0, '', '2022-03-31 13:13:05', '2022-03-31 13:13:05', 8, '', NULL, '', '', '', '', '2');
INSERT INTO `homecare_pasien` VALUES (13, 'putu', 'm', '2022-03-31', '1', 1, 1, '0003', '66', 2, 2, '00', '2022-04-02 05:11:14', '2022-04-02 05:11:14', 8, 'm', NULL, 'm', '0', '0', 'gg@ff.nn', '2');
COMMIT;

-- ----------------------------
-- Table structure for homecare_pekerjaan
-- ----------------------------
DROP TABLE IF EXISTS `homecare_pekerjaan`;
CREATE TABLE `homecare_pekerjaan` (
  `pekerjaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `pekerjaan_name` varchar(255) DEFAULT NULL,
  `pekerjaan_status` char(1) DEFAULT NULL,
  `pekerjaan_datecreated` datetime DEFAULT NULL,
  `pekerjaan_datemodified` datetime DEFAULT NULL,
  `pekerjaan_modifiedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`pekerjaan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of homecare_pekerjaan
-- ----------------------------
BEGIN;
INSERT INTO `homecare_pekerjaan` VALUES (1, 'Belum / Tidak Bekerja', '1', '2020-08-04 16:02:12', '2020-08-04 16:02:12', 4);
INSERT INTO `homecare_pekerjaan` VALUES (2, 'Mengurus Rumah Tangga', '1', '2020-08-04 16:02:31', '2020-08-04 16:02:31', 4);
INSERT INTO `homecare_pekerjaan` VALUES (3, 'Pelajar / Mahasiswa', '1', '2020-08-04 16:02:46', '2020-08-04 16:02:46', 4);
INSERT INTO `homecare_pekerjaan` VALUES (4, 'Pensiunan', '1', '2020-08-04 16:02:58', '2020-08-04 16:02:58', 4);
INSERT INTO `homecare_pekerjaan` VALUES (5, 'Pegawai Negeri Sipil', '1', '2020-08-04 16:03:10', '2020-08-04 16:03:21', 4);
INSERT INTO `homecare_pekerjaan` VALUES (6, 'Tentara Nasional Indonesia', '1', '2020-08-04 16:03:37', '2020-08-04 16:03:37', 4);
INSERT INTO `homecare_pekerjaan` VALUES (7, 'Kepolisian RI', '1', '2020-08-04 16:03:49', '2020-08-04 16:03:49', 4);
INSERT INTO `homecare_pekerjaan` VALUES (8, 'Perdagangan', '1', '2020-08-04 16:04:02', '2020-08-04 16:04:02', 4);
INSERT INTO `homecare_pekerjaan` VALUES (9, 'Petani / Pekebun', '1', '2020-08-04 16:04:16', '2020-08-04 16:04:16', 4);
INSERT INTO `homecare_pekerjaan` VALUES (10, 'Peternak', '1', '2020-08-04 16:04:25', '2020-08-04 16:04:25', 4);
INSERT INTO `homecare_pekerjaan` VALUES (11, 'Nelayan / Perikanan', '1', '2020-08-04 16:04:39', '2020-08-04 16:04:39', 4);
INSERT INTO `homecare_pekerjaan` VALUES (12, 'Industri', '1', '2020-08-04 16:04:46', '2020-08-04 16:04:46', 4);
INSERT INTO `homecare_pekerjaan` VALUES (13, 'Konstruksi', '1', '2020-08-04 16:05:02', '2020-08-04 16:05:13', 4);
INSERT INTO `homecare_pekerjaan` VALUES (14, 'Transportasi', '1', '2020-08-04 16:05:21', '2020-08-04 16:05:21', 4);
INSERT INTO `homecare_pekerjaan` VALUES (15, 'Karyawan Swasta', '1', '2020-08-04 16:06:05', '2020-08-04 16:06:05', 4);
INSERT INTO `homecare_pekerjaan` VALUES (16, 'Karyawan BUMN', '1', '2020-08-04 16:06:22', '2020-08-04 16:06:22', 4);
INSERT INTO `homecare_pekerjaan` VALUES (17, 'Karyawan BUMD', '1', '2020-08-04 16:06:29', '2020-08-04 16:06:29', 4);
INSERT INTO `homecare_pekerjaan` VALUES (18, 'Karyawan Honorer', '1', '2020-08-04 16:06:41', '2020-08-04 16:06:41', 4);
INSERT INTO `homecare_pekerjaan` VALUES (19, 'Buruh Harian Lepas', '1', '2020-08-04 16:06:57', '2020-08-04 16:06:57', 4);
INSERT INTO `homecare_pekerjaan` VALUES (20, 'Buruh Tani / Pekebunan', '1', '2020-08-04 16:07:09', '2020-08-04 16:07:09', 4);
INSERT INTO `homecare_pekerjaan` VALUES (21, 'Buruh Nelayan / Perikanan', '1', '2020-08-04 16:07:30', '2020-08-04 16:07:30', 4);
INSERT INTO `homecare_pekerjaan` VALUES (22, 'Buruh Peternakan', '1', '2020-08-04 16:07:42', '2020-08-04 16:07:42', 4);
INSERT INTO `homecare_pekerjaan` VALUES (23, 'Pembantu Rumah Tangga', '1', '2020-08-04 16:07:56', '2020-08-04 16:07:56', 4);
INSERT INTO `homecare_pekerjaan` VALUES (24, 'Tukang Cukur', '1', '2020-08-04 16:08:07', '2020-08-04 16:08:07', 4);
INSERT INTO `homecare_pekerjaan` VALUES (25, 'Tukang Listrik', '1', '2020-08-04 16:08:14', '2020-08-04 16:08:14', 4);
INSERT INTO `homecare_pekerjaan` VALUES (26, 'Tukang Batu', '1', '2020-08-04 16:08:20', '2020-08-04 16:08:20', 4);
INSERT INTO `homecare_pekerjaan` VALUES (27, 'Tukang Kayu', '1', '2020-08-04 16:08:28', '2020-08-04 16:08:28', 4);
INSERT INTO `homecare_pekerjaan` VALUES (28, 'Tukang Sol Sepatu', '1', '2020-08-04 16:08:42', '2020-08-04 16:08:42', 4);
INSERT INTO `homecare_pekerjaan` VALUES (29, 'Tukang Las / Pandai Besi', '1', '2020-08-04 16:08:55', '2020-08-04 16:08:55', 4);
INSERT INTO `homecare_pekerjaan` VALUES (30, 'Tukang Jahit', '1', '2020-08-04 16:09:23', '2020-08-04 16:09:23', 4);
INSERT INTO `homecare_pekerjaan` VALUES (31, 'Tukang Gigi', '1', '2020-08-04 16:09:31', '2020-08-04 16:09:31', 4);
INSERT INTO `homecare_pekerjaan` VALUES (32, 'Penata Rias', '1', '2020-08-04 16:09:40', '2020-08-04 16:09:40', 4);
INSERT INTO `homecare_pekerjaan` VALUES (33, 'Penata Busana', '1', '2020-08-04 16:09:52', '2020-08-04 16:09:52', 4);
INSERT INTO `homecare_pekerjaan` VALUES (34, 'Penata Rambut', '1', '2020-08-04 16:10:01', '2020-08-04 16:10:01', 4);
INSERT INTO `homecare_pekerjaan` VALUES (35, 'Mekanik', '1', '2020-08-04 16:10:10', '2020-08-04 16:10:10', 4);
INSERT INTO `homecare_pekerjaan` VALUES (36, 'Seniman', '1', '2020-08-04 16:10:15', '2020-08-04 16:10:15', 4);
INSERT INTO `homecare_pekerjaan` VALUES (37, 'Tabib', '1', '2020-08-04 16:10:24', '2020-08-04 16:10:24', 4);
INSERT INTO `homecare_pekerjaan` VALUES (38, 'Paraji', '1', '2020-08-04 16:10:31', '2020-08-04 16:10:31', 4);
INSERT INTO `homecare_pekerjaan` VALUES (39, 'Perancang Busana', '1', '2020-08-04 16:10:48', '2020-08-04 16:10:48', 4);
INSERT INTO `homecare_pekerjaan` VALUES (40, 'Penterjemah', '1', '2020-08-04 16:11:02', '2020-08-04 16:11:02', 4);
INSERT INTO `homecare_pekerjaan` VALUES (41, 'Imam Masjid', '1', '2020-08-04 16:11:13', '2020-08-04 16:11:13', 4);
INSERT INTO `homecare_pekerjaan` VALUES (42, 'Pendeta', '1', '2020-08-04 16:11:18', '2020-08-04 16:11:18', 4);
INSERT INTO `homecare_pekerjaan` VALUES (43, 'Pastor', '1', '2020-08-04 16:11:29', '2020-08-04 16:11:29', 4);
INSERT INTO `homecare_pekerjaan` VALUES (44, 'Wartawan', '1', '2020-08-04 16:11:35', '2020-08-04 16:11:35', 4);
INSERT INTO `homecare_pekerjaan` VALUES (45, 'Ustadz / Mubaligh', '1', '2020-08-04 16:11:54', '2020-08-04 16:11:54', 4);
INSERT INTO `homecare_pekerjaan` VALUES (46, 'Juru Masak', '1', '2020-08-04 16:12:05', '2020-08-04 16:12:05', 4);
INSERT INTO `homecare_pekerjaan` VALUES (47, 'Promotor Acara', '1', '2020-08-04 16:12:16', '2020-08-04 16:12:16', 4);
INSERT INTO `homecare_pekerjaan` VALUES (48, 'Anggota DPR-RI', '1', '2020-08-04 16:12:29', '2020-08-04 16:12:29', 4);
INSERT INTO `homecare_pekerjaan` VALUES (49, 'Anggota DPD', '1', '2020-08-04 16:12:38', '2020-08-04 16:12:38', 4);
INSERT INTO `homecare_pekerjaan` VALUES (50, 'Anggota BPK', '1', '2020-08-04 16:12:51', '2020-08-04 16:12:51', 4);
INSERT INTO `homecare_pekerjaan` VALUES (51, 'Presiden', '1', '2020-08-04 16:12:57', '2020-08-04 16:12:57', 4);
INSERT INTO `homecare_pekerjaan` VALUES (52, 'Wakil Presiden', '1', '2020-08-04 16:13:04', '2020-08-04 16:13:04', 4);
INSERT INTO `homecare_pekerjaan` VALUES (53, 'Anggota Mahkamah Konstitusi', '1', '2020-08-04 16:13:19', '2020-08-04 16:13:19', 4);
INSERT INTO `homecare_pekerjaan` VALUES (54, 'Anggota Kabinet / Kementerian', '1', '2020-08-04 16:13:35', '2020-08-04 16:13:35', 4);
INSERT INTO `homecare_pekerjaan` VALUES (55, 'Duta Besar', '1', '2020-08-04 16:13:45', '2020-08-04 16:13:45', 4);
INSERT INTO `homecare_pekerjaan` VALUES (56, 'Gubernur', '1', '2020-08-04 16:13:52', '2020-08-04 16:13:52', 4);
INSERT INTO `homecare_pekerjaan` VALUES (57, 'Wakil Gubernur', '1', '2020-08-04 16:14:01', '2020-08-04 16:14:01', 4);
INSERT INTO `homecare_pekerjaan` VALUES (58, 'Bupati', '1', '2020-08-04 16:14:12', '2020-08-04 16:14:12', 4);
INSERT INTO `homecare_pekerjaan` VALUES (59, 'Wakil Bupati', '1', '2020-08-04 16:14:22', '2020-08-04 16:14:22', 4);
INSERT INTO `homecare_pekerjaan` VALUES (60, 'Walikota', '1', '2020-08-04 16:14:30', '2020-08-04 16:14:30', 4);
INSERT INTO `homecare_pekerjaan` VALUES (61, 'Wakil Walikota', '1', '2020-08-04 16:14:37', '2020-08-04 16:14:37', 4);
INSERT INTO `homecare_pekerjaan` VALUES (62, 'Anggota DPRD Provinsi', '1', '2020-08-04 16:14:50', '2020-08-04 16:14:50', 4);
INSERT INTO `homecare_pekerjaan` VALUES (63, 'Anggota DPRD Kabupaten / Kota', '1', '2020-08-04 16:15:03', '2020-08-04 16:15:03', 4);
INSERT INTO `homecare_pekerjaan` VALUES (64, 'Dosen', '1', '2020-08-04 16:15:13', '2020-08-04 16:15:13', 4);
INSERT INTO `homecare_pekerjaan` VALUES (65, 'Guru', '1', '2020-08-04 16:15:18', '2020-08-04 16:15:18', 4);
INSERT INTO `homecare_pekerjaan` VALUES (66, 'Pilot', '1', '2020-08-04 16:15:26', '2020-08-04 16:15:26', 4);
INSERT INTO `homecare_pekerjaan` VALUES (67, 'Pengacara', '1', '2020-08-04 16:15:43', '2020-08-04 16:15:43', 4);
INSERT INTO `homecare_pekerjaan` VALUES (68, 'Notaris', '1', '2020-08-04 16:15:52', '2020-08-04 16:15:52', 4);
INSERT INTO `homecare_pekerjaan` VALUES (69, 'Arsitek', '1', '2020-08-04 16:15:58', '2020-08-04 16:15:58', 4);
INSERT INTO `homecare_pekerjaan` VALUES (70, 'Akuntan', '1', '2020-08-04 16:16:07', '2020-08-04 16:16:07', 4);
INSERT INTO `homecare_pekerjaan` VALUES (71, 'Konsultan', '1', '2020-08-04 16:16:13', '2020-08-04 16:16:13', 4);
INSERT INTO `homecare_pekerjaan` VALUES (72, 'Dokter', '1', '2020-08-04 16:16:23', '2020-08-04 16:16:23', 4);
INSERT INTO `homecare_pekerjaan` VALUES (73, 'Bidan', '1', '2020-08-04 16:16:29', '2020-08-04 16:16:29', 4);
INSERT INTO `homecare_pekerjaan` VALUES (74, 'Perawat', '1', '2020-08-04 16:16:35', '2020-08-04 16:16:35', 4);
INSERT INTO `homecare_pekerjaan` VALUES (75, 'Apoteker', '1', '2020-08-04 16:16:47', '2020-08-04 16:16:47', 4);
INSERT INTO `homecare_pekerjaan` VALUES (76, 'Psikiater / Psikolog', '1', '2020-08-04 16:17:00', '2020-08-04 16:17:00', 4);
INSERT INTO `homecare_pekerjaan` VALUES (77, 'Penyiar Televisi', '1', '2020-08-04 16:17:12', '2020-08-04 16:17:12', 4);
INSERT INTO `homecare_pekerjaan` VALUES (78, 'Penyiar Radio', '1', '2020-08-04 16:17:19', '2020-08-04 16:17:19', 4);
INSERT INTO `homecare_pekerjaan` VALUES (79, 'Pelaut', '1', '2020-08-04 16:17:28', '2020-08-04 16:17:28', 4);
INSERT INTO `homecare_pekerjaan` VALUES (80, 'Peneliti', '1', '2020-08-04 16:17:34', '2020-08-04 16:17:34', 4);
INSERT INTO `homecare_pekerjaan` VALUES (81, 'Sopir', '1', '2020-08-04 16:17:40', '2020-08-04 16:17:40', 4);
INSERT INTO `homecare_pekerjaan` VALUES (82, 'Pialang', '1', '2020-08-04 16:17:50', '2020-08-04 16:17:50', 4);
INSERT INTO `homecare_pekerjaan` VALUES (83, 'Paranormal', '1', '2020-08-04 16:17:56', '2020-08-04 16:17:56', 4);
INSERT INTO `homecare_pekerjaan` VALUES (84, 'Pedagang', '1', '2020-08-04 16:18:12', '2020-08-04 16:18:12', 4);
INSERT INTO `homecare_pekerjaan` VALUES (85, 'Perangkat Desa', '1', '2020-08-04 16:18:18', '2020-08-04 16:18:18', 4);
INSERT INTO `homecare_pekerjaan` VALUES (86, 'Kepala Desa', '1', '2020-08-04 16:18:30', '2020-08-04 16:18:30', 4);
INSERT INTO `homecare_pekerjaan` VALUES (87, 'Biarawati', '1', '2020-08-04 16:18:51', '2020-08-04 16:18:51', 4);
INSERT INTO `homecare_pekerjaan` VALUES (88, 'Wiraswasta', '1', '2020-08-04 16:19:02', '2020-08-04 16:19:02', 4);
COMMIT;

-- ----------------------------
-- Table structure for homecare_pelayanan
-- ----------------------------
DROP TABLE IF EXISTS `homecare_pelayanan`;
CREATE TABLE `homecare_pelayanan` (
  `pelayanan_id` int(11) NOT NULL AUTO_INCREMENT,
  `pelayanan_name` varchar(255) DEFAULT NULL,
  `pelayanan_desc` text DEFAULT NULL,
  `pelayanan_status` char(1) DEFAULT NULL,
  `pelayanan_datecreated` datetime DEFAULT NULL,
  `pelayanan_datemodified` datetime DEFAULT NULL,
  `pelayanan_modifiedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`pelayanan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of homecare_pelayanan
-- ----------------------------
BEGIN;
INSERT INTO `homecare_pelayanan` VALUES (3, 'Pemeriksaan Dokter', 'pelayanan medis atau  keperawatan untuk pasien', '1', '2020-08-03 15:06:48', '2022-03-13 15:25:46', 8);
INSERT INTO `homecare_pelayanan` VALUES (5, 'Layanan Laboratorium', 'pemberian pelayanan laboratorium pada pasien', '1', '2020-08-04 13:44:42', '2022-03-13 15:26:10', 8);
COMMIT;

-- ----------------------------
-- Table structure for homecare_perawatan
-- ----------------------------
DROP TABLE IF EXISTS `homecare_perawatan`;
CREATE TABLE `homecare_perawatan` (
  `perawatan_id` int(11) NOT NULL AUTO_INCREMENT,
  `perawatan_name` varchar(255) DEFAULT NULL,
  `perawatan_status` char(1) DEFAULT NULL,
  `perawatan_datecreated` datetime DEFAULT NULL,
  `perawatan_datemodified` datetime DEFAULT NULL,
  `perawatan_modifiedby` int(11) DEFAULT NULL,
  `pelayanan_id` int(11) DEFAULT NULL,
  `perawatan_harga` double DEFAULT NULL,
  PRIMARY KEY (`perawatan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of homecare_perawatan
-- ----------------------------
BEGIN;
INSERT INTO `homecare_perawatan` VALUES (2, 'Perawatan luka', '1', '2020-08-03 15:46:50', '2020-08-16 04:10:37', 8, 3, 100000);
INSERT INTO `homecare_perawatan` VALUES (3, 'Perawatan NGT / selang makan', '1', '2020-08-03 15:46:59', '2020-08-16 04:10:45', 8, 3, 100000);
INSERT INTO `homecare_perawatan` VALUES (4, 'Perawatan keteter / selang kencing', '1', '2020-08-04 13:46:02', '2020-08-16 04:10:51', 8, 3, 100000);
INSERT INTO `homecare_perawatan` VALUES (5, 'Perawatan / melakukan injeksi', '1', '2020-08-04 13:46:18', '2020-08-16 04:10:57', 8, 3, 100000);
INSERT INTO `homecare_perawatan` VALUES (6, 'Perawatan infus', '1', '2020-08-04 13:46:34', '2020-08-16 04:11:03', 8, 3, 100000);
INSERT INTO `homecare_perawatan` VALUES (7, 'Perawatan emergency', '1', '2020-08-04 13:46:50', '2020-08-16 04:11:08', 8, 3, 100000);
INSERT INTO `homecare_perawatan` VALUES (8, 'Perawatan kolostomi', '1', '2020-08-04 13:47:05', '2020-08-16 04:11:14', 8, 3, 100000);
INSERT INTO `homecare_perawatan` VALUES (9, 'Perawatan 24 jam dan 12 jam', '1', '2020-08-04 13:47:24', '2020-08-16 04:11:21', 8, 3, 100000);
INSERT INTO `homecare_perawatan` VALUES (10, 'Pemulihan pasca stroke', '1', '2020-08-04 13:47:42', '2020-08-16 04:11:26', 8, 4, 100000);
INSERT INTO `homecare_perawatan` VALUES (11, 'Nyeri pinggang bawah', '1', '2020-08-04 13:47:57', '2020-08-16 04:11:31', 8, 4, 100000);
INSERT INTO `homecare_perawatan` VALUES (12, 'Nyeri bahu', '1', '2020-08-04 13:48:19', '2020-08-16 04:11:40', 8, 4, 100000);
INSERT INTO `homecare_perawatan` VALUES (13, 'Nyeri lutut', '1', '2020-08-04 13:48:34', '2020-08-16 04:11:44', 8, 4, 1000000);
INSERT INTO `homecare_perawatan` VALUES (14, 'Cedera olaraga', '1', '2020-08-04 13:48:48', '2020-08-16 04:11:51', 8, 4, 100000);
INSERT INTO `homecare_perawatan` VALUES (15, 'Pemulihan pasca patah tulang', '1', '2020-08-04 13:49:08', '2020-08-16 04:11:56', 8, 4, 100000);
INSERT INTO `homecare_perawatan` VALUES (16, 'Gangguan postur pada anak dan remaja', '1', '2020-08-04 13:49:23', '2020-08-16 04:12:01', 8, 4, 100000);
INSERT INTO `homecare_perawatan` VALUES (17, 'Leher tengeng', '1', '2020-08-04 13:49:36', '2020-08-16 04:12:07', 8, 4, 100000);
INSERT INTO `homecare_perawatan` VALUES (18, 'Keterlambatan tumbuh kembang', '1', '2020-08-04 13:49:55', '2020-08-16 04:12:15', 8, 4, 1000000);
INSERT INTO `homecare_perawatan` VALUES (19, 'Wajah merot', '1', '2020-08-04 13:50:13', '2020-08-16 04:12:21', 8, 4, 100000);
INSERT INTO `homecare_perawatan` VALUES (20, 'Pengambilan specimen pemeriksaan', '1', '2020-08-04 13:50:56', '2020-08-16 04:12:25', 8, 5, 1000000);
INSERT INTO `homecare_perawatan` VALUES (21, 'Pemeriksaan darah lengkap', '1', '2020-08-04 14:39:56', '2020-08-16 04:12:33', 8, 5, 100000);
INSERT INTO `homecare_perawatan` VALUES (22, 'Pemeriksaan gula darah sewaktu', '1', '2020-08-04 14:40:11', '2020-08-16 04:12:41', 8, 5, 1000000);
COMMIT;

-- ----------------------------
-- Table structure for homecare_privilege
-- ----------------------------
DROP TABLE IF EXISTS `homecare_privilege`;
CREATE TABLE `homecare_privilege` (
  `privilege_id` int(11) NOT NULL AUTO_INCREMENT,
  `privilege_name` varchar(255) DEFAULT NULL,
  `privilege_status` int(1) DEFAULT NULL,
  `privilege_datecreated` datetime DEFAULT NULL,
  `privilege_datemodified` datetime DEFAULT NULL,
  `privilege_modifiedby` int(11) DEFAULT NULL,
  `privilege_show_data` char(1) NOT NULL,
  PRIMARY KEY (`privilege_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of homecare_privilege
-- ----------------------------
BEGIN;
INSERT INTO `homecare_privilege` VALUES (1, 'Administrator', 1, '2020-07-03 08:44:30', '2022-03-31 12:29:37', 8, '0');
INSERT INTO `homecare_privilege` VALUES (6, 'Rekam Medis', 1, '2020-08-26 23:43:30', '2020-08-26 23:43:30', 8, '1');
INSERT INTO `homecare_privilege` VALUES (7, 'Dokter', 1, '2020-08-28 20:45:04', '2020-08-28 20:45:04', 8, '1');
INSERT INTO `homecare_privilege` VALUES (8, 'Kepala', 1, '2020-08-28 20:45:15', '2020-08-28 20:45:15', 8, '1');
INSERT INTO `homecare_privilege` VALUES (10, 'Laboratorium', 1, '2022-03-13 14:54:33', '2022-03-13 14:54:33', 8, '1');
COMMIT;

-- ----------------------------
-- Table structure for homecare_profile
-- ----------------------------
DROP TABLE IF EXISTS `homecare_profile`;
CREATE TABLE `homecare_profile` (
  `profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_name` varchar(255) DEFAULT NULL,
  `profile_email` varchar(255) DEFAULT NULL,
  `profile_phone` varchar(255) DEFAULT NULL,
  `profile_fax` varchar(255) DEFAULT NULL,
  `profile_address` varchar(255) DEFAULT NULL,
  `profile_visi` text DEFAULT NULL,
  `profile_misi` text DEFAULT NULL,
  `profile_datecreated` datetime DEFAULT NULL,
  `profile_datemodified` datetime DEFAULT NULL,
  `profile_modifiedby` int(11) DEFAULT NULL,
  `profile_logo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of homecare_profile
-- ----------------------------
BEGIN;
INSERT INTO `homecare_profile` VALUES (1, 'Klinik Pratama Wahyu Medika', '-', '-', '-', 'Jln. Adam Malik No.54 Waingapu Sumba Timur', 'Rumah sakit rujukan terdepan dalam pelayanan', '1. Memberikan pelayanan yang sesuai standar kepada lapisan masyarakat\r\n2. Memberikan kepuasan kepada pelanggan rumah sakit baik internal maupun eksternal\r\n3. Pengembangan sumber daya manusia yang profesional\r\n4. Pengelolaan rumah sakit yang mandiri efektif dan efisien', '2020-08-03 07:53:51', '2020-08-27 21:53:39', 8, 'logo_20210602061757.png');
COMMIT;

-- ----------------------------
-- Table structure for homecare_registrasi
-- ----------------------------
DROP TABLE IF EXISTS `homecare_registrasi`;
CREATE TABLE `homecare_registrasi` (
  `registrasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `registrasi_no` varchar(255) DEFAULT NULL,
  `registrasi_norekam` varchar(255) DEFAULT NULL,
  `pasien_id` int(11) DEFAULT NULL,
  `pasien_alamat` varchar(255) DEFAULT NULL,
  `registrasi_date` date DEFAULT NULL,
  `registrasi_phone` varchar(255) DEFAULT NULL,
  `registrasi_datecreated` datetime DEFAULT NULL,
  `registrasi_datemodified` datetime DEFAULT NULL,
  `registrasi_modifiedby` int(11) DEFAULT NULL,
  `registrasi_status` char(1) DEFAULT '0',
  `registrasi_name` varchar(255) DEFAULT NULL,
  `pasien_jk` char(1) DEFAULT NULL,
  `pasien_status` char(1) DEFAULT NULL,
  `pasien_suku` varchar(255) DEFAULT NULL,
  `pasien_agama` int(11) DEFAULT NULL,
  `pasien_pendidikan` varchar(255) DEFAULT NULL,
  `pasien_gol` char(1) DEFAULT NULL,
  `pasien_pekerjaan` int(11) DEFAULT NULL,
  `pasien_phone` varchar(255) DEFAULT NULL,
  `registrasi_pekerjaan` varchar(255) DEFAULT NULL,
  `rawat_rs_kurang` char(1) DEFAULT NULL,
  `rawat_rs_lebih` char(1) DEFAULT NULL,
  `rawat_lain_kurang` char(1) DEFAULT NULL,
  `rawat_lain_lebih` char(1) DEFAULT NULL,
  `keluhan_saat_ini` tinytext DEFAULT NULL,
  `registrasi_biaya` char(1) DEFAULT NULL,
  `registrasi_suku` varchar(255) DEFAULT NULL,
  `registrasi_alamat` varchar(255) DEFAULT NULL,
  `registrasi_jaminan` char(1) DEFAULT NULL,
  PRIMARY KEY (`registrasi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of homecare_registrasi
-- ----------------------------
BEGIN;
INSERT INTO `homecare_registrasi` VALUES (29, '135100-03-20220331135100', '0001', 11, 'badung', '2022-03-31', '08123123', '2022-03-31 13:51:20', '2022-03-31 13:51:20', 8, '0', '0001 - i made joko', '1', NULL, NULL, 4, NULL, 'B', 69, '08123123', 'Arsitek', NULL, NULL, NULL, NULL, 'sakit perut', NULL, NULL, 'badung', '1');
INSERT INTO `homecare_registrasi` VALUES (30, '132254-04-20220401132254', '0001', 11, 'badung', '2022-04-01', '08123123', '2022-04-01 13:23:23', '2022-04-01 13:23:23', 8, '0', '0001 - i made joko', '1', NULL, NULL, 4, NULL, 'B', 69, '08123123', 'Arsitek', NULL, NULL, NULL, NULL, 'sakit kepala', NULL, NULL, 'badung', '0');
INSERT INTO `homecare_registrasi` VALUES (31, '141043-04-20220401141043', '0002', 12, 'dps', '2022-04-01', '088765', '2022-04-01 14:11:05', '2022-04-01 14:11:05', 8, '0', '0002 - mita', '0', NULL, NULL, 1, NULL, 'B', 2, '09764433', 'Mengurus Rumah Tangga', NULL, NULL, NULL, NULL, 'm', NULL, NULL, 'dps', '2');
INSERT INTO `homecare_registrasi` VALUES (32, '051121-04-20220402051121', '0003', 13, 'm', '2022-04-02', '00', '2022-04-02 05:12:16', '2022-04-02 05:12:16', 8, '0', '0003 - putu', '1', NULL, NULL, 1, NULL, 'B', 1, '00', 'Belum / Tidak Bekerja', NULL, NULL, NULL, NULL, 'sakit perut', NULL, NULL, 'm', '0');
INSERT INTO `homecare_registrasi` VALUES (33, '114835-04-20220412114835', '0002', 12, 'dps', '2022-04-12', '083113777821', '2022-04-12 11:49:12', '2022-04-12 11:49:12', 8, '0', '0002 - mita', '0', NULL, NULL, 1, NULL, 'B', 2, '083113777821', 'Mengurus Rumah Tangga', NULL, NULL, NULL, NULL, 'sakit pinggang', NULL, NULL, 'dps', '1');
COMMIT;

-- ----------------------------
-- Table structure for homecare_users
-- ----------------------------
DROP TABLE IF EXISTS `homecare_users`;
CREATE TABLE `homecare_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `user_address` varchar(255) DEFAULT NULL,
  `user_phone` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `privilege_id` int(11) DEFAULT NULL,
  `user_username` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_status` int(1) DEFAULT 0,
  `user_datecreated` datetime DEFAULT NULL,
  `user_datemodified` datetime DEFAULT NULL,
  `user_modifiedby` int(11) DEFAULT NULL,
  `dokter_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of homecare_users
-- ----------------------------
BEGIN;
INSERT INTO `homecare_users` VALUES (8, 'Administrator', 'Denpasar', '0361', 'admin@homecare.com', 1, 'admin', '$2y$10$Ly6gJ4/QLViT7.Ngx7qB3.OPFnhz5KfAwT.aCLrhNDnR4ekll/S5a', 1, '2020-08-10 15:21:23', '2021-06-02 06:18:29', 8, 8);
INSERT INTO `homecare_users` VALUES (9, 'Ayu', 'badung', '123123', 'ads@asd.asd', 6, 'loket', '$2y$10$6ApSfJAiqB1Fdu9.9Al0tewobiCUPppzYpWPRub1NkdCFq2c04RbO', 1, '2020-08-26 23:44:16', '2022-04-02 05:21:03', 8, 0);
INSERT INTO `homecare_users` VALUES (12, 'Kadek Kartika Listya Adnyani', 'Badung', '081', 'kadeklistyaadnyani@gmail.com', 8, 'kadek', '$2y$10$pzVnK8veGa0SHwV0GZXzkOy05zxcvhj3hbaej7dyxlIsvq0bTAlnG', 1, '2020-08-28 21:20:35', '2020-08-28 21:20:35', 8, 0);
INSERT INTO `homecare_users` VALUES (16, 'dr. Bopy G. Hamaduna', 'Melolo', '081339335333', 'bopy@gmail', 7, '1990120521002', '$2y$10$8uhlDTk6v.Ky97tZp3src.t5q1A48zqWRcxjdcgrC5WrcNIJRU4pa', 1, '2021-01-31 18:45:08', '2021-01-31 18:45:08', 8, 8);
INSERT INTO `homecare_users` VALUES (20, 'Nikayana', 'mengwi', '0831131123123123', 'nika@gmail.com', 10, 'labkes', '$2y$10$FlkKoaz0E1mVg8hJKR7XReDNOOcxhcZE3jY76i1HfH5TD7amfFzf2', 1, '2022-03-13 14:55:29', '2022-03-13 14:55:29', 8, 0);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
