<input type="hidden" name="id" value="<?php echo isset($data->pekerjaan_id) ? $data->pekerjaan_id : 0 ?>">
<input type="hidden" name="pekerjaan_modifiedby" value="<?php echo isset($profile->user_id) ? $profile->user_id : "" ?>">
<div class="form-material">
    <div class="row">
        <div class="col-md-12 b-r"> 
            <div class="form-group">
                <label>Nama Pekerjaan <font color='red'>*</font></label>
                <input type="text" name="pekerjaan_name" class="form-control form-control-line" value="<?php echo isset($data->pekerjaan_name) ? $data->pekerjaan_name : "" ?>" placeholder='Isikan nama pelayanan rumah sakit' required="true"> 
            </div>
            <div class="form-group">
                <label>Status <font color='red'>*</font></label>
                <select class='form-control form-control-line' name="pekerjaan_status" required="true">
                    <option value=""> Pilih Status </option>
                    <?php
                        if(isset($status)){
                            foreach($status AS $key => $value){
                                $selected = "";
                                if(isset($data->pekerjaan_status) && $data->pekerjaan_status == $key){
                                    $selected = "selected='selected'";
                                }
                                ?>
                                <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                                <?php
                                
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
    </div>
</div>
