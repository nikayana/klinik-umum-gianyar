<input type="hidden" name="id" value="<?php echo isset($data->agama_id) ? $data->agama_id : 0 ?>">
<input type="hidden" name="agama_modifiedby" value="<?php echo isset($profile->user_id) ? $profile->user_id : "" ?>">
<div class="form-material">
    <div class="row">
        <div class="col-md-12 b-r"> 
            <div class="form-group">
                <label>Nama Agama <font color='red'>*</font></label>
                <input type="text" name="agama_name" class="form-control form-control-line" value="<?php echo isset($data->agama_name) ? $data->agama_name : "" ?>" placeholder='Isikan nama pelayanan rumah sakit' required="true"> 
            </div>
            <div class="form-group">
                <label>Status <font color='red'>*</font></label>
                <select class='form-control form-control-line' name="agama_status" required="true">
                    <option value=""> Pilih Status </option>
                    <?php
                        if(isset($status)){
                            foreach($status AS $key => $value){
                                $selected = "";
                                if(isset($data->agama_status) && $data->agama_status == $key){
                                    $selected = "selected='selected'";
                                }
                                ?>
                                <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                                <?php
                                
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
    </div>
</div>
