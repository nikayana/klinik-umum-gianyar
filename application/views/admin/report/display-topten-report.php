<html>
    <head>
        <title>Form Screening Pasien </title>
    </head>
    <body>
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <thead>
                <tr>
                    <th style='padding:5px;' align="center">
                        <center>
                            <img src="<?php echo isset($profile->profile_logo) ? site_url("assets/images/profile/".$profile->profile_logo) : "" ?>" width="100">
                            <h3>
                                Laporan Tranding Penyakit<br>
                                <?php echo isset($profile->profile_name) ? $profile->profile_name : "" ?>
                            </h3>
                            <?php echo isset($profile->profile_address) ? $profile->profile_address : "" ?><br>
                            Telepon: <?php echo isset($profile->profile_phone) ? $profile->profile_phone : "" ?>. Email: <?php echo isset($profile->profile_email) ? $profile->profile_email : "" ?>.<br>
                            
                        </center>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="padding:5px;">
                        <table border="1" cellpadding="1" cellspacing="1" width="100%">
                            <thead>
                                <tr>
                                    <th style="padding:5px;">No</th>
                                    <th style="padding:5px;">Kode</th>
                                    <th style="padding:5px;">Nama</th>
                                    <th style="padding:5px;">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if(isset($data)){
                                        foreach($data AS $key => $value){
                                            ?>
                                            <tr>
                                                <td style="padding:5px;"><?php echo $key+1 ?></td>
                                                <td style="padding:5px;"><?php echo $value->icd10_kode ?></td>
                                                <td style="padding:5px;"><?php echo $value->icd10_name ?></td>
                                                <td style="padding:5px;"><?php echo number_format($value->jumlah,0,",",".") ?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                ?>
                                
                            </tbody>
                        </table>
                    </td>
            </tbody>
        </table>
    </body>
</html>