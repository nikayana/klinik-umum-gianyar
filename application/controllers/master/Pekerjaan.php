<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pekerjaan extends HC_Controller {
    private $response;
    private $modul;
    private $priv;

	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->response['status'] = 404;
        $this->response['messages'] = "Unknown Error, Please contact your administrator";
        $this->response['datas'] = array();
        $this->load->model("master/pekerjaanmodel", "pekerjaan");

        $this->modul = "master/pekerjaan";
        $this->priv = $this->checkpriv($this->modul);

    }

    public function index(){
        $data = array();
        $data['title'] = "Pekerjaan";
        $data['description'] = "Mengelola master data pekerjaan";
        $data['status'] = $this->pekerjaan->STATUS;
        $data['priv'] = $this->priv;

        if(isset($this->priv->{$this->access->FIELD_ACCESS_VIEW}) && $this->priv->{$this->access->FIELD_ACCESS_VIEW} == $this->access->ACCESS){
            $this->theme("admin/pekerjaan/main-pekerjaan", $data);
        }else{
            redirect("notfound");
        }
        
    }

    public function datatables(){
        $result = array();
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $aColumns = array("pekerjaan_id","pekerjaan_name", "pekerjaan_status", "pekerjaan_datemodified");

        $sLimit = 20;
        $sOffset = 0;
        $sOrder = "";
        $sWhere = "";
        $sGroup = "";
        $sEcho = 0;
        if(isset($param["sEcho"])){
            $sEcho = 0;
        }
        if(isset($param['start']) && $param['start'] > 0){
            $sOffset = $param['start'];
        }
        
        if(isset($param['order'])){
            if(empty($sOrder)){
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }else{
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }
        }

        $sWhere = "1=1";
        if(isset($param[$this->pekerjaan->FIELD_PEKERJAAN_NAME]) && !empty($param[$this->pekerjaan->FIELD_PEKERJAAN_NAME])){
            $sWhere .=" AND LOWER(".$this->pekerjaan->FIELD_PEKERJAAN_NAME.") LIKE '%".strtolower($param[$this->pekerjaan->FIELD_PEKERJAAN_NAME])."%'";
        }

        if(isset($param[$this->pekerjaan->FIELD_PEKERJAAN_STATUS]) && strlen($param[$this->pekerjaan->FIELD_PEKERJAAN_STATUS]) > 0){
            $sWhere .=" AND ".$this->pekerjaan->FIELD_PEKERJAAN_STATUS."=".$param[$this->pekerjaan->FIELD_PEKERJAAN_STATUS]."";
        }
        $searchWhere = "";
        if(!empty($param['search']['value'])){

            for($i=0;$i<count($aColumns);$i++){
                if(empty($searchWhere)){
                    $searchWhere .=$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }else{
                    $searchWhere .=" OR ".$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }

            }

            if(!empty($sWhere)){
                $sWhere .=" AND (".$searchWhere.")";
            }else{
                $sWhere .= "(".$searchWhere.")";
            }

        }
        $sTotal = $this->pekerjaan->getCount($sWhere);
        $data = $this->pekerjaan->getList($sOffset,$sLimit,$sWhere,$sOrder,$sGroup);
        $output = array(
            "sEcho" => intval($sEcho),
            "iTotalRecords" => $sTotal,
            "iTotalDisplayRecords" => $sTotal,
            "aaData" => array()
        );

        foreach ($data AS $key => $value){
            $row = array();
            $button = "";

            if(isset($this->priv->{$this->access->FIELD_ACCESS_UPDATE}) && $this->priv->{$this->access->FIELD_ACCESS_UPDATE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->pekerjaan->FIELD_PRIMARY}."' data-url='". site_url("master/pekerjaan/form")."' class='addedit btn btn-warning btn-xs m-t-10' data-toggle='tooltip' data-original-title='Edit' data-placement='top' style='margin-right:5px;' data-modal='#editor' data-title='Ubah Master Data Pekerjaan'><i class='fas fa-edit'></i></button>";
            }
            if(isset($this->priv->{$this->access->FIELD_ACCESS_DELETE}) && $this->priv->{$this->access->FIELD_ACCESS_DELETE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->pekerjaan->FIELD_PRIMARY}."' data-url='". site_url("master/pekerjaan/deleteExc")."' class='delete btn btn-danger btn-xs m-t-10' data-toggle='tooltip' data-original-title='Hapus' data-placement='top' style='margin-right:5px;'><i class='fa fa-trash'></i></button>";
            }

            for($i = 0; $i < count($aColumns); $i++){
                    $reasstype = "";
                    if($aColumns[$i] == $this->pekerjaan->FIELD_PRIMARY){
                            $row[] = $sOffset+$key+1;
                    }else if($aColumns[$i] == $this->pekerjaan->FIELD_PEKERJAAN_DATEMODIFIED){
                            $row[] = $button;
                    }else if($aColumns[$i] == $this->pekerjaan->FIELD_PEKERJAAN_STATUS){
                        $displaystatus = "danger";
                        if($value->{$aColumns[$i]} == $this->pekerjaan->ACTIVE_STATUS){
                            $displaystatus = "success";
                        }
                        $row[] = "<div class='label label-table label-".$displaystatus."'>".$this->pekerjaan->STATUS[$value->{$aColumns[$i]}]."</div>";
                    }else{
                            $row[] = $value->{$aColumns[$i]};
                    }


            }

            $row[] = $button;
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function form(){
		$data = array();
		$param = $this->input->post(NULL, TRUE);
		$param = $this->antiinjection->antiinject($param);
		$where = "";
		//GET Data
        $data['data'] = $this->pekerjaan->fetch($this->pekerjaan->FIELD_PRIMARY."='".$param['id']."'");
        $data['status'] = $this->pekerjaan->STATUS;
        $data['profile'] = $this->getprofile();

		$this->response['html'] = $this->load->view("admin/pekerjaan/pekerjaan-form", $data, TRUE);
		$this->response['status'] = 200;
		$this->response['messages'] = '';
        $this->response['datas'] = array();

		echo json_encode($this->response);
    }
    
    public function save(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);

        $filter = $this->checkparam($this->pekerjaan->getrules(), $param);
        if(count($filter) == 0){
            $data = $this->pekerjaan->fetch($this->pekerjaan->FIELD_PRIMARY."='".$param['id']."'");
            $param[$this->pekerjaan->FIELD_PEKERJAAN_DATEMODIFIED] = date("Y-m-d H:i:s");
            if(isset($data->{$this->pekerjaan->FIELD_PRIMARY})){
                $this->updateExc($param);
            }else{
                $param[$this->pekerjaan->FIELD_PEKERJAAN_DATECREATED] = date("Y-m-d H:i:s");
                $this->insertExc($param);
            }
        }else{
            $this->response = $filter;
        }
        
        echo json_encode($this->response);
    }

    public function updateExc($param){
        $id = $param['id'];

        unset($param['id']);
        $result = $this->pekerjaan->update($param, $this->pekerjaan->FIELD_PRIMARY."='".$id."'");

        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data pekerjaan berhasil diperbaharui';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function insertExc($param){

        unset($param['id']);
        $result = $this->pekerjaan->insert($param);
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data pekerjaan berhasil diperbaharui';
        }else{
            $this->response['status'] = 500;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function deleteExc(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $result = $this->pekerjaan->delete($this->pekerjaan->FIELD_PRIMARY."='".$param['id']."'");
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data pekerjaan berhasil dihapus';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menghapus data, silahkan ulangi kembali';
        }

        echo json_encode($this->response);
    }

    public function selectdata(){
        $response = array();
        $result = array();
        $raw = array();
        $raw['id'] = "";
        $raw['text'] = "Semua Agama";
        $result[] = $raw;
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $where = "";
        $order = $this->pekerjaan->FIELD_PEKERJAAN_NAME." ASC";
        if(isset($param['term']) && !empty($param['term'])){
            $where .= "LOWER(".$this->pekerjaan->FIELD_PEKERJAAN_NAME.") LIKE '%".$param['term']."%'";
            $data = $this->pekerjaan->getList(0,0,$where, $order, "");
            foreach($data AS $key => $value){
                $raw = array();
                $raw['id'] = $value->{$this->pekerjaan->FIELD_PRIMARY};
                $raw['text'] = $value->{$this->pekerjaan->FIELD_PEKERJAAN_NAME};
                $result[] = $raw;
            }
        }
        
        $response['results'] = $result;
        echo json_encode($response);
    }

}