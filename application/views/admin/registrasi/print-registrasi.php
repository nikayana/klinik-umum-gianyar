<html>
    <head>
        <title>Form Pendaftaran Pasien</title>
    </head>
    <body>
    <?php 
$checked = "<div style=\"width:20px;height:20px;border: 1px solid #000;content: '';display: block;width: 4px;height: 7px;position:relative;top:4px;left:7px;border: solid #000;border-width: 0 2px 2px 0;transform: rotate(45deg);\"></div>";
$diff = "";
if(isset($data->pasien_tgllahir)){
    $date1 = $data->pasien_tgllahir;
    $date2 = date("Y-m-d");
    
    $diff = abs(strtotime($date2) - strtotime($date1));
    
    $years = floor($diff / (365*60*60*24));
    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

    $diff = $years." tahun ".$months." bulan ".$days." hari";
}

?>
<table border="1" cellpadding="0" cellspacing="0" style="width:100%;font-size:10" width="100%">
    <tbody>
        <tr>
            <th colspan='3' style="padding:5px;" align="left"><?php echo isset($comprof->profile_name) ? $comprof->profile_name : "" ?></th>
            <th colspan="1" align="right" style="padding:5px;">RM.1.0/ADMISI</th>
        </tr>
        <tr>
            <th style="padding:5px;" align="left" width="80"><?php echo isset($comprof->profile_logo) ? "<img src='".site_url("assets/images/profile/".$comprof->profile_logo)."' width='80'>" : "" ?></th>
            <th colspan='3' style="padding:5px;" align="center">PENDAFTARAN PASIEN</th>
        </tr>
        <tr>
            <th style="padding:5px;" align="left" colspan="2">Tanggal Admisi: <?php echo isset($data->registrasi_datecreated) ? date("d F Y", strtotime($data->registrasi_datecreated))  : date("d F Y") ?></th>
            <th style="padding:5px;" align="left">Pukul: <?php echo isset($data->registrasi_datecreated) ? date("H:i:s", strtotime($data->registrasi_datecreated))  : date("H:i:s") ?></th>
            <th style="padding:5px;" align="left">No. RM: <?php echo isset($data->registrasi_norekam) ? $data->registrasi_norekam : "" ?></th>
        </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%;font-size:10" width="100%">
    <tbody>
        <tr>
            <th colspan="3"  style="padding:5px;" align="left">A. IDENTITAS PASIEN</th>
        </tr>
        <tr>
            <td style='padding:5px;' align="left" width="100">Nama</td>
            <td style='padding:5px;' align="left" width="10">:</td>
            <td style='padding:5px;border-bottom:1px solid #000;' align="left"><?php echo isset($data->pasien_name) ? $data->pasien_name : "" ?></td>
        </tr>
        <tr>
            <td style='padding:5px;' align="left" width="200">Tanggal Lahir / Umur</td>
            <td style='padding:5px;' align="left" width="10">:</td>
            <td style='padding:5px;border-bottom:1px solid #000;' align="left"><?php echo isset($data->pasien_tgllahir) ? date("d F Y", strtotime($data->pasien_tgllahir)) ." / ".$diff : "" ?></td>
        </tr>
        <tr>
            <td style='padding:5px;' align="left" width="200">Jenis Kelamin</td>
            <td style='padding:5px;' align="left" width="10">:</td>
            <td style='padding:5px;border-bottom:1px solid #000;' align="left"><?php echo isset($data->pasien_jk) ? $jk[$data->pasien_jk] : "" ?></td>
        </tr>
         
        <tr>
            <td style='padding:5px;' align="left" width="200">Agama</td>
            <td style='padding:5px;' align="left" width="10">:</td>
            <td style='padding:5px;border-bottom:1px solid #000;' align="left"><?php echo isset($data->agama_name) ? $data->agama_name : "" ?></td>
        </tr>
         
                <tr>
                    <td style='padding:5px;' align="left" width="200">Golongan Darah</td>
                    <td style='padding:5px;' align="left" width="10">:</td>
                    <td style='padding:5px;border-bottom:1px solid #000;' align="left"><?php echo isset($data->pasien_gol) ? $data->pasien_gol : "" ?></td>
                </tr>
                <tr>
                    <td style='padding:5px;' align="left" width="200">Pekerjaan</td>
                    <td style='padding:5px;' align="left" width="10">:</td>
                    <td style='padding:5px;border-bottom:1px solid #000;' align="left"><?php echo isset($data->pekerjaan_name) ? $data->pekerjaan_name : "" ?></td>
                </tr>
                <tr>
                    <td style='padding:5px;' align="left" width="200">Alamat KTP</td>
                    <td style='padding:5px;' align="left" width="10">:</td>
                    <td style='padding:5px;border-bottom:1px solid #000;' align="left"><?php echo isset($data->pasien_alamat) ? $data->pasien_alamat : "" ?></td>
                </tr>
                <tr>
                    <td style='padding:5px;' align="left" width="200">Telp / HP</td>
                    <td style='padding:5px;' align="left" width="10">:</td>
                    <td style='padding:5px;border-bottom:1px solid #000;' align="left"><?php echo isset($data->pasien_phone) ? $data->pasien_phone : "" ?></td>
                </tr>
            </tbody>
        </table>
        <hr>
        <table border="0" cellpadding="0" cellspacing="0" style="width:100%;font-size:10" width="100%">
            <tbody>
                <tr>
                    <th colspan="3"  style="padding:5px;" align="left">A. IDENTITAS PENANGGUNG JAWAB PASIEN</th>
                </tr>
                <tr>
                    <td style='padding:5px;' align="left" width="100">Nama</td>
                    <td style='padding:5px;' align="left" width="10">:</td>
                    <td style='padding:5px;border-bottom:1px solid #000;' align="left"><?php echo isset($data->registrasi_name) ? $data->registrasi_name : "" ?></td>
                </tr>
                <tr>
                    <td style='padding:5px;' align="left" width="200">Pekerjaan</td>
                    <td style='padding:5px;' align="left" width="10">:</td>
                    <td style='padding:5px;border-bottom:1px solid #000;' align="left"><?php echo isset($data->registrasi_pekerjaan) ? $data->registrasi_pekerjaan : "" ?></td>
                </tr>
                <tr>
                    <td style='padding:5px;' align="left" width="200">Alamat KTP</td>
                    <td style='padding:5px;' align="left" width="10">:</td>
                    <td style='padding:5px;border-bottom:1px solid #000;' align="left"><?php echo isset($data->registrasi_alamat) ? $data->registrasi_alamat : "" ?></td>
                </tr>
                <tr>
                    <td style='padding:5px;' align="left" width="200">Telp / HP</td>
                    <td style='padding:5px;' align="left" width="10">:</td>
                    <td style='padding:5px;border-bottom:1px solid #000;' align="left"><?php echo isset($data->registrasi_phone) ? $data->registrasi_phone : "" ?></td>
                </tr>
            </tbody>
        </table>
        <br>
        <table border="1" cellpadding="0" cellspacing="0" style="width:100%;font-size:10" width="100%">
            <tbody>
                 
                <tr>
                    <td style='padding:5px;' align="left" colspan="3">
                        <b>Keluhan saat ini:</b>
                        <p><?php echo isset($data->keluhan_saat_ini) ? $data->keluhan_saat_ini : ""?></p>
                    </td>
                </tr>
            </tbody>
        </table>
        <table border="1" cellpadding="0" cellspacing="0" style="width:100%;font-size:10" width="100%">
            <tbody>
                 
                <tr>
                    <td style='padding:5px;' align="left" colspan="3">
                        <b>Jaminan Perawatan:</b>
                        <p><?php echo isset($data->registrasi_jaminan)? $jaminan : "" ?></p>
                    </td>
                </tr>
            </tbody>
        </table>
        <br>
         
        <br>
        <table border="0" cellpadding="0" cellspacing="0" style="width:100%;font-size:10" width="100%">
            <tbody>
                <tr>
                    <td style='padding:5px' width="50%">
                        &nbsp;
                    </td>
                    <td style='padding:5px' width="50%" align="center">
                        Gianyar, <?php echo isset($data->registrasi_datecreated) ? date("d F Y", strtotime($data->registrasi_datecreated))  : date("d F Y") ?><br>
                        Penaggung Jawab
                        <br>
                        <br>
                        <br><br>
                        <br><br>
                        <br>
                        <?php echo isset($data->registrasi_name) ? $data->registrasi_name : "" ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>