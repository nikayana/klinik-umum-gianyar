<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Perawatan extends HC_Controller {
    private $response;
    private $modul;
    private $priv;

	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->response['status'] = 404;
        $this->response['messages'] = "Unknown Error, Please contact your administrator";
        $this->response['datas'] = array();
        $this->load->model("profile/perawatanmodel", "perawatan");
        $this->load->model("profile/pelayananmodel", "pelayanan");

        $this->modul = "profile/perawatan";
        $this->priv = $this->checkpriv($this->modul);

    }

    public function index(){
        $data = array();
        $data['title'] = "Perawatan";
        $data['description'] = "Mengelola data perawatan pada pelayanan rumah sakit";
        $data['status'] = $this->perawatan->STATUS;
        $data['priv'] = $this->priv;

        if(isset($this->priv->{$this->access->FIELD_ACCESS_VIEW}) && $this->priv->{$this->access->FIELD_ACCESS_VIEW} == $this->access->ACCESS){
            $this->theme("admin/perawatan/main-perawatan", $data);
        }else{
            redirect("notfound");
        }
        
    }

    public function datatables(){
        $result = array();
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $aColumns = array("perawatan_id","pelayanan_name","perawatan_name", "perawatan_harga","perawatan_status", "perawatan_datemodified");

        $sLimit = 20;
        $sOffset = 0;
        $sOrder = "";
        $sWhere = "";
        $sGroup = "";
        $sEcho = 0;
        if(isset($param["sEcho"])){
            $sEcho = 0;
        }
        if(isset($param['start']) && $param['start'] > 0){
            $sOffset = $param['start'];
        }
        
        if(isset($param['order'])){
            if(empty($sOrder)){
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }else{
                $sOrder .=$aColumns[$param['order'][0]['column']]." ".$param['order'][0]['dir']."";
            }
        }

        $sWhere = "1=1";
        if(isset($param[$this->perawatan->FIELD_PERAWATAN_NAME]) && !empty($param[$this->perawatan->FIELD_PERAWATAN_NAME])){
            $sWhere .=" AND LOWER(p.".$this->perawatan->FIELD_PERAWATAN_NAME.") LIKE '%".strtolower($param[$this->perawatan->FIELD_PERAWATAN_NAME])."%'";
        }

        if(isset($param[$this->perawatan->FIELD_PELAYANAN_ID])){
            $sWhere .=" AND p.".$this->perawatan->FIELD_PELAYANAN_ID."=".$param[$this->perawatan->FIELD_PELAYANAN_ID]."";
        }

        if(isset($param[$this->perawatan->FIELD_PERAWATAN_STATUS]) && strlen($param[$this->perawatan->FIELD_PERAWATAN_STATUS]) > 0){
            $sWhere .=" AND p.".$this->perawatan->FIELD_PERAWATAN_STATUS."=".$param[$this->perawatan->FIELD_PERAWATAN_STATUS]."";
        }
        $searchWhere = "";
        if(!empty($param['search']['value'])){

            for($i=0;$i<count($aColumns);$i++){
                if(empty($searchWhere)){
                    $searchWhere .=$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }else{
                    $searchWhere .=" OR ".$aColumns[$i]." LIKE '%".$param['search']['value']."%'";
                }

            }

            if(!empty($sWhere)){
                $sWhere .=" AND (".$searchWhere.")";
            }else{
                $sWhere .= "(".$searchWhere.")";
            }

        }
        $sTotal = $this->perawatan->getCountJoin($sWhere);
        $data = $this->perawatan->getListJoin($sOffset,$sLimit,$sWhere,$sOrder,$sGroup);
        $output = array(
            "sEcho" => intval($sEcho),
            "iTotalRecords" => $sTotal,
            "iTotalDisplayRecords" => $sTotal,
            "aaData" => array()
        );

        foreach ($data AS $key => $value){
            $row = array();
            $button = "";

            if(isset($this->priv->{$this->access->FIELD_ACCESS_UPDATE}) && $this->priv->{$this->access->FIELD_ACCESS_UPDATE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->perawatan->FIELD_PRIMARY}."' data-url='". site_url("profile/perawatan/form")."' class='addedit btn btn-warning btn-xs m-t-10' data-toggle='tooltip' data-original-title='Edit' data-placement='top' style='margin-right:5px;' data-modal='#editor' data-title='Ubah Data Perawatan Pada Pelayanan Rumah Sakit'><i class='fas fa-edit'></i></button>";
            }
            if(isset($this->priv->{$this->access->FIELD_ACCESS_DELETE}) && $this->priv->{$this->access->FIELD_ACCESS_DELETE} == $this->access->ACCESS){
                $button .="<button type='button' data-id='".$value->{$this->perawatan->FIELD_PRIMARY}."' data-url='". site_url("profile/perawatan/deleteExc")."' class='delete btn btn-danger btn-xs m-t-10' data-toggle='tooltip' data-original-title='Hapus' data-placement='top' style='margin-right:5px;'><i class='fa fa-trash'></i></button>";
            }

            for($i = 0; $i < count($aColumns); $i++){
                    $reasstype = "";
                    if($aColumns[$i] == $this->perawatan->FIELD_PRIMARY){
                            $row[] = $sOffset+$key+1;
                    }else if($aColumns[$i] == $this->perawatan->FIELD_PERAWATAN_DATEMODIFIED){
                            $row[] = $button;
                    }else if($aColumns[$i] == $this->perawatan->FIELD_PERAWATAN_STATUS){
                        $displaystatus = "danger";
                        if($value->{$aColumns[$i]} == $this->perawatan->ACTIVE_STATUS){
                            $displaystatus = "success";
                        }
                        $row[] = "<div class='label label-table label-".$displaystatus."'>".$this->perawatan->STATUS[$value->{$aColumns[$i]}]."</div>";
                    }else if($aColumns[$i] == $this->perawatan->FIELD_PERAWATAN_HARGA){
                        $row[] = "Rp ".number_format($value->{$this->perawatan->FIELD_PERAWATAN_HARGA},0,",",".");
                    }else{
                            $row[] = $value->{$aColumns[$i]};
                    }


            }

            $row[] = $button;
            $output['aaData'][] = $row;
        }
        echo json_encode($output);
    }

    public function form(){
		$data = array();
		$param = $this->input->post(NULL, TRUE);
		$param = $this->antiinjection->antiinject($param);
		$where = "";
		//GET Data
        $data['data'] = $this->perawatan->fetch($this->perawatan->FIELD_PRIMARY."='".$param['id']."'");
        $data['pelayanan'] = $this->pelayanan->getList(0,0,$this->pelayanan->FIELD_PELAYANAN_STATUS."=".$this->pelayanan->ACTIVE_STATUS,"","");
        $data['status'] = $this->perawatan->STATUS;
        $data['profile'] = $this->getprofile();

		$this->response['html'] = $this->load->view("admin/perawatan/perawatan-form", $data, TRUE);
		$this->response['status'] = 200;
		$this->response['messages'] = '';
        $this->response['datas'] = array();

		echo json_encode($this->response);
    }
    
    public function save(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);

        $filter = $this->checkparam($this->perawatan->getrules(), $param);
        if(count($filter) == 0){
            $data = $this->perawatan->fetch($this->perawatan->FIELD_PRIMARY."='".$param['id']."'");
            $param[$this->perawatan->FIELD_PERAWATAN_DATEMODIFIED] = date("Y-m-d H:i:s");
            if(isset($data->{$this->perawatan->FIELD_PRIMARY})){
                $this->updateExc($param);
            }else{
                $param[$this->perawatan->FIELD_PERAWATAN_DATECREATED] = date("Y-m-d H:i:s");
                $this->insertExc($param);
            }
        }else{
            $this->response = $filter;
        }
        
        echo json_encode($this->response);
    }

    public function updateExc($param){
        $id = $param['id'];

        unset($param['id']);
        $result = $this->perawatan->update($param, $this->perawatan->FIELD_PRIMARY."='".$id."'");

        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data perawatan pada pelayanan rumah sakit berhasil diperbaharui';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function insertExc($param){

        unset($param['id']);
        $result = $this->perawatan->insert($param);
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data perawatan pada pelayanan rumah sakit berhasil diperbaharui';
        }else{
            $this->response['status'] = 500;
            $this->response['messages'] = 'Terdapat kesalahan saat menyimpan data, silahkan ulangi kembali';
        }
    }

    public function deleteExc(){
        $param = $this->input->post(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $result = $this->perawatan->delete($this->perawatan->FIELD_PRIMARY."='".$param['id']."'");
        if($result){
            $this->response['status'] = 200;
            $this->response['messages'] = 'Data perawatan pada pelayanan rumah sakit berhasil dihapus';
        }else{
            $this->response['status'] = 404;
            $this->response['messages'] = 'Terdapat kesalahan saat menghapus data, silahkan ulangi kembali';
        }

        echo json_encode($this->response);
    }

    public function selectdata(){
        $response = array();
        $result = array();
        $raw = array();
        $raw['id'] = "";
        $raw['text'] = "Semua Perawatan Pelayanan Rumah Sakit";
        $result[] = $raw;
        $param = $this->input->get(NULL, TRUE);
        $param = $this->antiinjection->antiinject($param);
        $where = "";
        $order = $this->perawatan->FIELD_PERAWATAN_NAME." ASC";
        if(isset($param['term']) && !empty($param['term'])){
            $where .= "LOWER(".$this->perawatan->FIELD_PERAWATAN_NAME.") LIKE '%".$param['term']."%'";
            $data = $this->perawatan->getList(0,0,$where, $order, "");
            foreach($data AS $key => $value){
                $raw = array();
                $raw['id'] = $value->{$this->perawatan->FIELD_PRIMARY};
                $raw['text'] = $value->{$this->perawatan->FIELD_PERAWATAN_NAME};
                $result[] = $raw;
            }
        }
        
        $response['results'] = $result;
        echo json_encode($response);
    }

}