<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">
                    <b>Laporan Kunjungan</b>
                </h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard") ?>"><i class='fa fa-home'></i> Beranda</a></li>
                        <li class="breadcrumb-item active">Laporan Kunjungan</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        
        <!-- ============================================================== -->
        <!-- Sales Chart and browser state-->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-lg-12">
                <div class='card'>
                    <div class="card-body">
                        <h4 class="card-title">Form Pencarian</h4>
                        <form id="report" class="form-material" data-url="<?php echo site_url("report/report/displayreportkunjungan") ?>">
                            <div class='row'>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="tgl_start">Dari Tanggal</label>
                                        <input type="text" name="tgl_start" class="datepickers form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="tgl_end">Sampai Tanggal</label>
                                        <input type="text" name="tgl_end" class="datepickers form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="reportbtn"><br><br></label>
                                    <button name="reportbtn" type="submit" class="btn btn-success" id="reportbtn"><i class='fa fa-search'></i> Cari</button>
                                    <button name="printbtn" type="button" class="btn btn-default" id="printbtn" data-url="<?php echo site_url("report/report/printreportkunjungan") ?>"><i class='fa fa-print'></i> Print</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Column -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body" id="displayreport">
                    
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        
        <!-- ============================================================== -->
        <!-- Comment - chats -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- End Comment - chats -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</div>