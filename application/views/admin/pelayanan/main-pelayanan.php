<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">
                    <b>Pelayanan Rumah Sakit</b>
                </h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard") ?>"><i class='fa fa-home'></i> Beranda</a></li>
                        <li class="breadcrumb-item active">Pelayanan</li>
                    </ol>
                    <?php
                        if(isset($priv->access_add) && $priv->access_add == 1){
                            ?>
                                <button type="button" class="btn btn-success addedit d-none d-lg-block m-l-15" data-modal="#editor" data-url="<?php echo site_url("profile/pelayanan/form") ?>" data-id="" data-title="Tambah Data Pelayanan Rumah Sakit"><i class='fa fa-plus-circle'></i> Tambah Data</button>
                            <?php
                        }
                    ?>
                    <button type="button" class="btn btn-info d-none d-lg-block m-l-15" id="searchformbtn"><i class='fa fa-search'></i> Pencarian</button>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        
        <!-- ============================================================== -->
        <!-- Sales Chart and browser state-->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-lg-12">
                <div class='card' id="searchform" style="display:none;">
                    <div class="card-body">
                        <h4 class="card-title">Form Pencarian</h4>
                        <form id="search" class="form-material">
                            <div class='row'>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="pelayanan_name">Nama Pelayanan</label>
                                        <input type="text" name="pelayanan_name" class="form-control form-control-line">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="pelayanan_status">Status</label>
                                        <select name="pelayanan_status" class="selectmain form-control form-control-line custom-select " style="width:100%">
                                            <option value="">Semua Status</option>
                                            <?php 
                                                if(isset($status)){
                                                    foreach($status AS $key => $value){
                                                        ?>
                                                        <option value='<?php echo $key ?>'><?php echo $value ?></option>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="searchbtn"> &nbsp; </label>
                                    <button name="searchbtn" type="submit" class="btn btn-block btn-success" id="searchbtn"><i class='fa fa-search'></i> Cari</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Column -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"><?php echo isset($title) ? $title : "" ?></h4>
                        <h6 class="card-subtitle"> <?php echo isset($description) ? $description : "" ?> </h6>
                        <div class="table-responsive">
                            <table id="tables" class="table display table-bordered table-striped no-wrap" data-url="<?php echo site_url("profile/pelayanan/datatables") ?>">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Pelayanan</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        
        <!-- ============================================================== -->
        <!-- Comment - chats -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- End Comment - chats -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</div>
<div class="modal fade" id="editor" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	   <div class="modal-dialog modal-lg" role="document">
		     <div class="modal-content">
              <form id="formdata" data-url="<?php echo site_url("profile/pelayanan/save") ?>" data-modal="#editor">
			        <div class="modal-header">
			            <h5 class="modal-title">Modal title</h5>
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
		                </button>
                    </div>
	                <div class="modal-body">
				            
        			</div>
        			<div class="modal-footer">
                        
                        <div class="col-md-12 mb-3">
                            <div class="alert" id="notif" style="display:none;"></div>
                        </div>
        				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Tutup</button>
        				<button type="submit" class="btn btn-success" id="savebtn"><i class='fa fa-check'></i> Simpan</button>
        			</div>
              </form>
          </div>
     </div>
</div>