
<html>
    <head>
        <title>Form Screening Pasien</title>
    </head>
    <body>
    <?php
    $date1 = $data->pasien_tgllahir;
    $date2 = date("Y-m-d");
    
    $diff = abs(strtotime($date2) - strtotime($date1));
    
    $years = floor($diff / (365*60*60*24));
    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

    $diff = $years." tahun ".$months." bulan ".$days." hari";

    $transport = "";
    if(isset($kendaraan)){
        foreach($kendaraan AS $key => $value){
            if(empty($transport)){
                $transport .=$value;
            }else{
                $transport .=" / ".$value;
            }
        }
    }
?>
<table cellpadding="0" cellspacing="0" border="1">
    <thead>
        <tr>
            <th colspan="2" style='padding:5px;'><?php echo isset($profile->profile_name) ? $profile->profile_name : "" ?></th>
            <th style='padding:5px;'><b>RM. 2.14</th>
        </tr>
        <tr>
            <th style='padding:5px;'><img src="<?php echo isset($profile->profile_logo) ? site_url("assets/images/profile/".$profile->profile_logo) : "" ?>" width="100"></th>
            <th style='padding:5px;'><h3>FORM SCREENING LUAR RUMAH SAKIT</h3></th>
            <th style='padding:5px;'>
                <table cellpadding="0" cellspacing="0" border="0">
                    <tbody>
                        <tr>
                            <td style='padding:5px;' align="left">Tanggal</td>
                            <td style='padding:5px;'>:</td>
                            <td style='padding:5px;' align="left"><?php echo isset($data->kunjungan_date) ? date("d F Y", strtotime($data->kunjungan_date)) : "" ?></td>
                        </tr>
                        <tr>
                            <td style='padding:5px;' align="left">Jam</td>
                            <td style='padding:5px;'>:</td>
                            <td style="border-bottom:1px solid #000;padding:5px;" align="left"><?php echo isset($data->kunjungan_time) ? date("H:i:s", strtotime($data->kunjungan_time)) : "" ?></td>
                        </tr>
                    </tbody>
                </table>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2">
                <table cellpadding="0" cellspacing="0" border="0">
                    <tbody>
                        <tr>
                            <td style='padding:5px;'>Kunjungan RSUD</td>
                            <td style='padding:5px;'>:</td>
                            <td style='padding:5px;'><?php echo isset($data->kunjungan_pasien) ? $pasientype[$data->kunjungan_pasien] : "" ?></td>
                        </tr>
                        <tr>
                            <td style="padding:5px;">No. RM</td>
                            <td style="padding:5px;">:</td>
                            <td style="padding:5px;"><?php echo isset($data->registrasi_norekam) ? $data->registrasi_norekam : "" ?></td>
                        </tr>
                        <tr>
                            <td style="padding:5px;">Nama Lengkap</td>
                            <td style="padding:5px;">:</td>
                            <td style="padding:5px;"><?php echo isset($data->pasien_name) ? $data->pasien_name : "" ?></td>
                        </tr>
                        <tr>
                            <td style="padding:5px;">TTL / Umur</td>
                            <td style="padding:5px;">:</td>
                            <td style="padding:5px;"><?php echo isset($data->pasien_tgllahir) ? date("d F Y", strtotime($data->pasien_tgllahir))." / ".$diff : "" ?></td>
                        </tr>
                        <tr>
                            <td style="padding:5px;">Jenis Kelamin</td>
                            <td style="padding:5px;">:</td>
                            <td style="padding:5px;"><?php echo isset($data->pasien_jk) ? $jk[$data->pasien_jk] : "" ?></td>
                        </tr>
                        <tr>
                            <td style="padding:5px;">Alamat</td>
                            <td style="padding:5px;">:</td>
                            <td style="padding:5px;"><?php echo isset($data->pasien_alamat) ? $data->pasien_alamat : "" ?></td>
                        </tr>
                        <tr>
                            <td style="padding:5px;">No. HP</td>
                            <td style="padding:5px;">:</td>
                            <td style="padding:5px;"><?php echo isset($data->pasien_phone) ? $data->pasien_phone : "" ?></td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td style="border-left:1px solid #000;">
                <table cellpadding="0" cellspacing="0" border="0">
                    <tbody>
                        <tr>
                            <td colspan="2" valign="top" style="padding:5px;">ATM yang dimiliki:</td>
                        </tr>
                        <tr>
                            <?php 
                                if(isset($atm)){
                                    foreach($atm AS $key => $value){
                                        $checked = "";
                                        if(isset($data->atm) && $data->atm == $key){
                                            $checked = "checked='checked'";
                                        }
                                        if($key != 2){
                                            ?>
                                            <td style="padding:5px;"><input type="checkbox" <?php echo $checked ?>> <?php echo $value ?></td>
                                            <?php
                                        }
                                    }
                                }
                            ?> 
                        </tr>
                        <tr>
                            <td colspan="2" style="padding:5px;"><input type="checkbox" <?php echo isset($dat->atm) && $data->atm == 2 ? "checked='checked'" : "" ?>>Lainnya: <?php isset($data->atm_lainnya) && !empty($data->atm_lainnya) ? $data->atm_lainnya : "____________" ?></td>
                        </tr>
                        <tr>
                            <td colspan="2" style='padding:5px;;'>No Rek.
                            <br>
                            <?php echo isset($data->no_rel) ? $data->no_rel : "" ?>
                            <br>
                            <hr></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding:5px;">Ketersediaan jaringan internet</td>
                        </tr>
                        <tr>
                            <?php  
                                if(isset($internet)){
                                    foreach($internet AS $key => $value){
                                        $checked = "";
                                        if(isset($data->ketersediaan_internet) && $data->ketersediaan_internet == $key){
                                            $checked = "checked='checked'";
                                        }
                                        ?>
                                        <td style="padding:5px;"><input type="checkbox" <?php echo $checked ?>> Ada</td>
                                        <?php
                                    }
                                }
                            ?>
                            
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="padding:5px;border-top:1px solid #000;">
                Anamnesa kebutuhan pasien:<br>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tbody>
                        <tr>
                            <td style='padding:5px;'><?php echo isset($data->amnesa_keluhan) ? $data->amnesa_keluhan : "<br><br><br><br>" ?></td>
                        </tr>
                    </tbody>
                </table><br>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="padding:5px;border-top:1px solid #000;">
                Riwayat Penyakit:<br>
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tbody>
                        <tr>
                            <td style='padding:5px;'><?php echo isset($data->riwayat_penyakit) ? $data->riwayat_penyakit : "<br><br><br><br>" ?></td>
                        </tr>
                    </tbody>
                </table><br>
            </td>
        </tr>
        <tr>
                    <td colspan="3" style="padding:5px;border-top:1px solid #000;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tbody>
                                <tr>
                                    <td style="padding:5px;" width="150">Riwayat Alergi:</td>
                                    <td style="padding:5px;" width="100"><input type="checkbox" <?php echo isset($data->riwayat_alergi_status) && $data->riwayat_alergi_status == 0  ? "checked='checked'" : "" ?>> Tidak Ada</td>
                                    <td  style="padding:5px;" width="100"><input type="checkbox" <?php echo isset($data->riwayat_alergi_status) && $data->riwayat_alergi_status == 1  ? "checked='checked'" : "" ?>> Ada</td>
                                    <td style='border-bottom:1px solid #000;padding:5px;'><?php echo isset($data->riwayat_alergi) ? $data->riwayat_alergi : "" ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding:5px;border-top:1px solid #000;">
                        Obat-obatan yang dikonsumsi:<br><br>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tbody>
                                <tr>
                                    <td style='padding:5px;'><?php echo isset($data->obat_konsumsi) ? $data->obat_konsumsi : "<br><br><br><br>" ?></td>
                                </tr>
                            </tbody>
                        </table><br>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding:5px;border-top:1px solid #000;">
                        Layanan yang diinginkan
                        <br><br>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tbody>
                                <?php  
                                    if(isset($pelayanan)){
                                        foreach($pelayanan AS $key => $value){
                                            ?>
                                            <tr>
                                                <td style='padding:5px;'>
                                                    <div style='font-weight:bold;'><input type="checkbox" <?php echo $value['booked'] > 0 ? "checked='checked'" : "" ?>><?php echo $value['name'] ?></br></div>
                                                    <table  cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tbody>
                                                            <?php
                                                                $content = "";
                                                                $done = 0;
                                                                foreach($value['perawatan'] AS $keys => $values){
                                                                    $checked =  $values['booked'] > 0 ? "checked='checked'" : "";
                                                                    if($keys < (count($value['perawatan'])-1)){
                                                                        if(($keys%2) == 0){
                                                                            if(!empty($content)){
                                                                                $content .="</td></tr>";
                                                                            }
                                                                            $content .="<tr><td width='200'><input type='checkbox' ".$checked.">".$values['name']."</td>";
                                                                        }else{
                                                                            $done+=1;
                                                                            $content .="<td width='200'><input type='checkbox' ".$checked.">".$values['name']."</td>";
                                                                        }
                                                                    }else if($keys == 0){
                                                                        $content .="<tr><td width='200'><input type='checkbox' ".$checked.">".$values['name']."</td>";
                                                                    }

                                                                    if(!empty($content) && $keys == (count($value['perawatan'])-1)){
                                                                        $content .="</td></tr>";
                                                                    }
                                                                    
                                                                    
                                                                    ?>
                                                                        
                                                                    <?php
                                                                }

                                                                echo $content;
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                ?>
                                
                            </tbody>
                        </table><br>
                    </td>

                </tr>
                <tr>
                    <td colspan="3" style="padding:5px;border-top:1px solid #000;">
                        Assesment:
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tbody>
                                <tr>
                                    <td style='padding:5px;'><?php echo isset($data->obat_konsumsi) ? $data->obat_konsumsi : "<br><br><br><br>" ?></td>
                                </tr>
                            </tbody>
                        </table><br>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style='border:1px solid #000;'>
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <td style='padding:5px;border-right:1px solid #000' width="33%" rowspan="2">
                                        Planning Treatment<br><br>
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td><?php echo isset($data->planing_treatment) ? $data->planing_treatment : "" ?></td>
                                                </tr>
                                            </tbody>
                                        </table><br>
                                    </td>
                                    <td style='padding:5px;border-right:1px solid #000;' rowspan="2" width="33%">
                                        Petugas<br><br>
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td><?php echo isset($data->petugas) ? $data->petugas : "" ?></td>
                                                </tr>
                                            </tbody>
                                        </table><br>
                                    </td>
                                    <td style='padding:5px;border-bottom:1px solid #000'>
                                        Kendaraan<br><br>
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td><?php echo isset($data->kendaraan) ? $kendaraan[$data->kendaraan] : $transport ?></td>
                                                </tr>
                                            </tbody>
                                        </table><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='padding:5px;'>
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td>Jarak Tempuh <?php echo isset($data->jarak_tempuh) && $data->jarak_tempuh > 0 ? number_format($data->jarak_tempuh,0,",",".") : "............." ?> Km</td>
                                                </tr>
                                            </tbody>
                                        </table><br>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style='border:1px solid #000;'>
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <td style='padding:5px;border-right:1px solid #000' width="33%" rowspan="2">
                                        Perkiraan biaya pelayanan<br><br>
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td><?php echo isset($data->perkiraan_biaya) && $data->perkiraan_biaya > 0 ? "Rp. ".number_format($data->perkiraan_biaya,0,",",".") : "" ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td style="padding:5px;border-right:1px solid #000;" rowspan="2" width="33%">
                                        Bahan yang diperlukan<br><br>
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td><?php echo isset($data->bahan_diperlukan) ? $data->bahan_diperlukan : "" ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td style="padding:5px;border-bottom:1px solid #000;">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td>Waktu tempuh <?php echo isset($data->waktu_tempuh) && $data->waktu_tempuh > 0 ? number_format($data->waktu_tempuh,0,",",".") : "............." ?> menit</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:5px;">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td>Kebutuhan BBM <?php echo isset($data->bbm) && $data->bbm > 0 ? number_format($data->bbm,0,",",".") : "............." ?> Liter</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <td style='padding:5px;' width="33%" rowspan="3" align="center">
                                        Petugas Validasi<br><br><br><br>___________________
                                    </td>
                                    <td style='padding:5px;border-right:1px solid #000;' width="33%" rowspan="3" align="center">
                                        Petugas Screening<br><br><br><br>___________________
                                    </td>
                                    <td style="padding:5px;border-bottom:1px solid #000;">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td width="150">Jam Berangkat</td>
                                                    <td> : <?php echo isset($data->jam_berangkat) ? $data->jam_berangkat : "" ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:5px;border-bottom:1px solid #000;">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td width="150">Jam Tiba Lokasi</td>
                                                    <td> : <?php echo isset($data->jam_tiba) ? $data->jam_tiba : "" ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:5px;border-bottom:1px solid #000;">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td width="150">Jam Pulang</td>
                                                    <td> : <?php echo isset($data->jam_tiba) ? $data->jam_tiba : "" ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tr>
            </tbody>
        </table>
        * Berikan tanda checklist (&#10003;) pada kolom yang sesuai
    </body>
</html>