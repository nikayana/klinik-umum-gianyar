<input type="hidden" name="id" value="<?php echo isset($data->kunjungan_pelayanan_id) ? $data->kunjungan_id : 0 ?>">
<input type="hidden" name="kunjungan_id" value="<?php echo isset($data->kunjungan_id) ? $data->kunjungan_id : $kunjungan_id ?>">
<input type="hidden" name="kunjungan_pelayanan_modifiedby" value="<?php echo isset($profile->user_id) ? $profile->user_id : "" ?>">
<div class="form-material">
    <div class="row">
        <div class="col-md-12 b-r"> 
            <div class="form-group">
                <label>Perawatan <font color='red'>*</font></label>
                <select class='form-control form-control-line selects' style='width:100%;' name="perawatan_id" required="true">
                    <option value=""> Pilih Perawatan </option>
                    <?php
                        if(isset($perawatan)){
                            foreach($perawatan AS $key => $value){
                                $selected = "";
                                if(isset($data->perawatan_id) && $data->perawatan_id == $value->perawatan_id){
                                    $selected = "selected='selected'";
                                }

                                ?>
                                    <option value="<?php echo $value->perawatan_id ?>" <?php echo $selected ?>><?php echo "".$value->perawatan_name." (Rp ".number_format($value->perawatan_harga,0,",",".").")" ?></option>
                                <?php
                            }
                        }
                    ?>
                    <option value="0" <?php echo isset($data->perawatan_id) && $data->perawatan_id == 0 ? "selected='selected'" : "" ?>>Lainnya</option>
                </select>
            </div>
        </div>
        <div class="col-md-12 b-r">
            <div class="form-group">
                <label>Biaya Perawtan <font color='red'>*</font></label>
                <input type="number" name="pelayanan_harga" class="form-control form-control-line" value="<?php echo isset($data->pelayanan_harga) ? $data->pelayanan_harga : "" ?>" placeholder='Isikan biaya kunjungan' required="true"> 
            </div>
        </div>
        <div class="col-md-12 b-r">
            <div class="form-group">
                <label>Keterangan <font color='red'>*</font></label>
                <input type="text" name="pelayanan_lainnya" class="form-control form-control-line" value="<?php echo isset($data->pelayanan_lainnya) ? $data->pelayanan_lainnya : "" ?>" placeholder='Isikan keterangan pelayanan' required="true"> 
            </div>
        </div>
    </div>
</div>
