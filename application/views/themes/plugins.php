<script src="<?php echo site_url("assets/node_modules/jquery/jquery-3.2.1.min.js") ?>"></script>
<!-- Bootstrap popper Core JavaScript -->
<script src="<?php echo site_url("assets/node_modules/popper/popper.min.js") ?>"></script>
<script src="<?php echo site_url("assets/node_modules/bootstrap/dist/js/bootstrap.min.js") ?>"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="<?php echo site_url("assets/dist/js/perfect-scrollbar.jquery.min.js") ?>"></script>
<!-- Datatables Javascript -->
<script src="<?php echo site_url("assets/node_modules/datatables.net/js/jquery.dataTables.min.js") ?>"></script>
<script src="<?php echo site_url("assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js") ?>"></script>
<!-- Switcher Bootstrap -->
<script src="<?php echo site_urL("assets/node_modules/bootstrap-switch/bootstrap-switch.min.js") ?>"></script>
<!--Wave Effects -->
<script src="<?php echo site_url("assets/dist/js/waves.js") ?>"></script>
<!--Menu sidebar -->
<script src="<?php echo site_url("assets/dist/js/sidebarmenu.js") ?>"></script>
<!--Select2 Javascript -->
<script src="<?php echo site_url("assets/node_modules/select2/dist/js/select2.full.min.js") ?>" type="text/javascript"></script>
<!-- Toast Javascript -->
<script src="<?php echo site_url("assets/node_modules/toast-master/js/jquery.toast.js") ?>"></script>
<!-- Sweet Alert Javascript -->
<script src="<?php echo site_url("assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js") ?>"></script>
<!-- Croppie Javascript -->
<script src="<?php echo site_url("assets/node_modules/croppie/croppie.js") ?>"></script>
<!-- Datepicker Javascript -->
<script src="<?php echo site_url("assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js") ?>"></script>
<!-- Clockpicker Javascript -->
<script src="<?php echo site_url("assets/node_modules/clockpicker/dist/jquery-clockpicker.min.js") ?>"></script>
<!-- Date Paginator -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="<?php echo site_url("assets/node_modules/date-paginator/bootstrap-datepaginator.min.js") ?>"></script>
<!--Custom JavaScript -->
<script src="<?php echo site_url("assets/dist/js/custom.min.js") ?>"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->
<!--Sky Icons JavaScript -->
<script src="<?php echo site_url("assets/node_modules/skycons/skycons.js") ?>"></script>
<!-- APP -->
<script src="<?php echo site_url("assets/dist/js/app/errorhandler.js") ?>"></script>
<script src="<?php echo site_url("assets/dist/js/app/app.js") ?>"></script>
<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);

    $('.select2').select2();
})();
</script>