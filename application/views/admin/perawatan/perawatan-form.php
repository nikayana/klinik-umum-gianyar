<input type="hidden" name="id" value="<?php echo isset($data->perawatan_id) ? $data->perawatan_id : 0 ?>">
<input type="hidden" name="perawatan_modifiedby" value="<?php echo isset($profile->user_id) ? $profile->user_id : "" ?>">
<div class="form-material">
    <div class="row">
        <div class="col-md-12 b-r"> 
            <div class="form-group">
                <label>Status <font color='red'>*</font></label>
                <select class='form-control form-control-line selects' name="pelayanan_id" required="true" style="width:100%">
                    <option value="">Pilih Pelayanan</option>
                    <?php
                        if(isset($pelayanan)){
                            foreach($pelayanan AS $key => $value){
                                $selected = "";
                                if(isset($data->pelayanan_id) && $data->pelayanan_id == $value->pelayanan_id){
                                    $selected = "selected='selected'";
                                }
                                ?>
                                <option value='<?php echo $value->pelayanan_id ?>' <?php echo $selected ?>><?php echo $value->pelayanan_name ?></option>
                                <?php
                                
                            }
                        }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label>Nama Perawatan <font color='red'>*</font></label>
                <input type="text" name="perawatan_name" class="form-control form-control-line" value="<?php echo isset($data->perawatan_name) ? $data->perawatan_name : "" ?>" placeholder='Isikan nama perawtan pelayanan rumah sakit' required="true"> 
            </div>
            <div class="form-group">
                <label>Status <font color='red'>*</font></label>
                <select class='form-control form-control-line' name="perawatan_status" required="true">
                    <option value=""> Pilih Status Perawatan </option>
                    <?php
                        if(isset($status)){
                            foreach($status AS $key => $value){
                                $selected = "";
                                if(isset($data->perawatan_status) && $data->perawatan_status == $key){
                                    $selected = "selected='selected'";
                                }
                                ?>
                                <option value='<?php echo $key ?>' <?php echo $selected ?>><?php echo $value ?></option>
                                <?php
                                
                            }
                        }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label>Biaya Perawatan <font color='red'>*</font></label>
                <input type="number" name="perawatan_harga" class="form-control form-control-line" value="<?php echo isset($data->perawatan_harga) ? $data->perawatan_harga : "" ?>" placeholder='Isikan harga perawtan' required="true"> 
            </div>
        </div>
    </div>
</div>
